jQuery(document).ready(function($) {

    // Добавляем маску для поля с номера телефона
    $('#phone').mask('+7 (999) 999-99-99');

    // Проверяет отмечен ли чекбокс согласия
    // с обработкой персональных данных
    $('#check').on('click', function() {
        if ($("#check").prop("checked")) {
            $('#button').attr('disabled', false);
        } else {
            $('#button').attr('disabled', true);
        }
    });
    /*
    // Отправляет данные из формы на сервер и получает ответ
    $('#contactForm').on('submit', function(event) {
        
        event.preventDefault();

        var form = $('#contactForm'),
            button = $('#button'),
            answer = $('#answer'),
            loader = $('#loader');

        $.ajax({
            url: 'handler.php',
            type: 'POST',
            data: form.serialize(),
            beforeSend: function() {
                answer.empty();
                button.attr('disabled', true).css('margin-bottom', '20px');
                loader.fadeIn();
            },
            success: function(result) {
                loader.fadeOut(300, function() {
                    answer.text(result);
                });
                form.find('.form-control').val(' ');
                button.attr('disabled', false);
            },
            error: function() {
                loader.fadeOut(300, function() {
                    answer.text('Произошла ошибка! Попробуйте позже.');
                });
                button.attr('disabled', false);
            }
        });

    }); 
    */
    $('body').on('submit', 'form', function(event){
    var $form = $(this);
    event.preventDefault();
    var data = $form.serialize();
    $.ajax({
        url: '/ajax/send.php',
        data: data,
        method: 'POST',
        beforeSend: function () {
            $form.find('.submit-btn').addClass('process');
            $form.find('.submit-btn').text('Идет отправка');
            $form.find('.submit-btn').val('Идет отправка');
        },
        success: function (xhr) {
            dataLayer.push({'event': 'cheap_call'});
            setTimeout(function () {
                $form.find('.submit-btn').removeClass('process').addClass('success');
                $form.find('.submit-btn').text('Заявка отправлена!');
                $form.find('.submit-btn').val('Заявка отправлена!');
                $form.find('.submit-btn').attr('disabled');
                setTimeout(function () {
                    $form.find('input[type="text"]').val('');
                    $form.find('input[type="email"]').val('');
                    $form.find('input[type="phone"]').val('');
                    $form.find('.submit-btn').removeClass('process').removeClass('success');
                    $form.find('.submit-btn').text('Отправить');
                    $form.find('.submit-btn').val('Отправить');
                }, 4500);
            }, 2000);
        },
        error: function (xhr) {
            $form.find('.submit-btn').css({background: '#aa4400', color: '#fff'});
            $form.find('.submit-btn').removeClass('process').removeClass('success');
            $form.find('.submit-btn').text('Форма не отправилась ((');
            console.log(xhr);
        }
    })
    return false;
    
    });
    

});