<div class="col-m-20 col-md-3 col-m-pull-60 col-sm-6 col-xs-6 unpad">
    <div class="slider-cat">
        <div class="slider-cat__box">
            <a href="/by-installments">
                <img class="slider-cat-box__img" src="https://formulamebeli.com/upload/medialibrary/62b/62beec0405900f32ef006b7d6649b5c2.jpg" alt="">
            </a>
        </div>
        <div class="slider-cat__box">
            <a href="https://franshiza.formulamebeli.com">
                <img class="slider-cat-box__img" src="https://formulamebeli.com/upload/medialibrary/193/193a3bbf7dfb686459db99a5eca5eb51.jpg" alt="">
            </a>
        </div>
    </div>
</div>
<div class="col-m-20 col-md-3 col-sm-6 col-xs-6 unpad">
    <div class="slider-cat">
        <div class="slider-cat__box">
            <a href="/novosel">
                <img class="slider-cat-box__img" src="https://formulamebeli.com/upload/medialibrary/9e0/9e0ee4691b08dc61701c8e9ed55b818a.jpg" alt="">
            </a>
        </div>
        <div class="slider-cat__box">
            <a href="/podarok-za-otzyv">
                <img class="slider-cat-box__img" src="https://formulamebeli.com/upload/medialibrary/6e4/6e46045714d29b1ec46930d5d2599f29.jpg" alt="">
            </a>
        </div>
    </div>
</div>