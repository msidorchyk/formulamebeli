<div class="credit__modal" style="display: none;" id="modal__init">
    <div class="credit__modal-img-wrap">
        <img alt="Покупай со Сбербанком" src="<?php echo SITE_TEMPLATE_PATH ?>/images/credit/logo_white.png" class="credit__modal-img">
    </div>
    <div class="credit__modal-text">
        <p class="credit__modal-text-main">
            Этот товар может быть оформлен в кредит при сумме покупки от <b>3000</b> рублей*.
            <br>
          Добавьте товары в корзину и выберите пункт оплаты «Кредит от Сбербанка» 
        </p>
        <p class="credit__modal-text-additional">
            * Услуга доступна для обладателей дебетовых карт Сбербанка.
        </p>
    </div>
    <div class="credit__modal-link-wrap">
        <a class="credit__modal-link btn btn-lg btn-default" href="https://www.pokupay.ru/credit_terms" target="_blank">Условия кредита</a>
    </div>
</div><br>