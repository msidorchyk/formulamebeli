<section class="whybuy-block <?=$style?>">
    <h2 style="text-align: center;">Почему покупают в «Формула Мебели»</h2>
    <ul class="whybuy">
        <li class="wb_icon1"><span>Лучшее ценовое предложение в Пермском крае</span></li>
        <li class="wb_icon2"><span>Регулярные акции и скидки, проведение маркетинговых мероприятий</span></li>
        <li class="wb_icon3"><span>Предоставление выгодной рассрочки без процентов и переплат</span></li>
        <li class="wb_icon4"><span>Собственный автопарк, штатные водители, грузчики, сборщики</span></li>
        <li class="wb_icon5"><span>Гарантия на мебель до 3 лет</span></li>
        <li class="wb_icon6"><span>Регулярный контроль качества обслуживания, высокие стандарты продаж</span></li>
    </ul>
</section>