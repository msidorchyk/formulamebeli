<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
\Bitrix\Main\Loader::includeModule('iblock');

$IBLOCK_ID = 39;
$arInfo = CCatalogSKU::GetInfoByProductIBlock($IBLOCK_ID);

$ciBlockElement = new CIBlockElement;
$select = Array("ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_CATALOG_EL");
$filter = Array("IBLOCK_ID"=>41, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$elements = $ciBlockElement::GetList(Array(), $filter, false, false, $select);
$goods = [];

while($element = $elements->GetNextElement())
{
    $fields = $element->GetFields();
    $current_element = $ciBlockElement::GetByID($fields['PROPERTY_CATALOG_EL_VALUE']);
    if ($current_good = $current_element->GetNext()) {
        $goods[$current_good['ID']] = ['GOOD'=>$current_good];
    }
    if (is_array($arInfo)) {
        $offers = CIBlockElement::GetList(array(array("PRICE" => "ASC")),array('IBLOCK_ID' => $arInfo['IBLOCK_ID'], 'PROPERTY_'.$arInfo['SKU_PROPERTY_ID'] => $fields['PROPERTY_CATALOG_EL_VALUE']));
        if($offer = $offers->GetNext()) {
            $price = GetCatalogProductPrice($offer["ID"], 3);;
            $goods[$current_good['ID']]['OFFER'] = $offer;
            $goods[$current_good['ID']]['PRICE'] = $price;
            $goods[$current_good['ID']]['OFFER_QUANTITY'] = CCatalogProduct::GetByID($offer["ID"]);
        }
    }


}
?>
<div style="position: relative;">
    <h5 style="margin: 40px 0 10px;"><span style="border-bottom: 2px solid #ddd;">С этим товаром покупают</span></h5>
    <div class="flexslider loading_state shadow border custom_flex top_right" data-plugin-options='{"animation": "slide", "animationSpeed": 600, "directionNav": true, "controlNav" :false, "animationLoop": true, "slideshow": false, "controlsContainer": ".tabs_slider_navigation.BUY_WITH_IT_nav", "counts": [4,3,3,2,1]}'>
        <ul class="tabs_slider BUY_WITH_IT_slides slides catalog_block">
            <?foreach ($goods as $good):?>
                <li class="catalog_item visible catalog_item visible">
                    <div class="inner_wrap">
                        <div class="image_wrapper_block">
                            <a href="<?=$good['GOOD']['DETAIL_PAGE_URL'];?>" class="thumb shine">
                                <div class="like_icons">
                                    <div class="wish_item_button">
                                        <span title="Отложить" class="wish_item to" data-item="<?=$good['OFFER']['ID']?>"><i></i></span>
                                        <span title="В отложенных" class="wish_item in added" style="display: none;" data-item="<?=$good['OFFER']['ID'];?>"><i></i></span>
                                    </div>
                                    <div class="compare_item_button">
                                        <span title="Сравнить" class="compare_item to" data-iblock="<?=$good['OFFER']['IBLOCK_ID'];?>" data-item="<?=$good['OFFER']['ID'];?>"><i></i></span>
                                        <span title="В сравнении" class="compare_item in added" style="display: none;" data-iblock="<?=$good['OFFER']['IBLOCK_ID'];?>" data-item="<?=$good['OFFER']['ID'];?>"><i></i></span>
                                    </div>
                                </div>
                                <?if($good['GOOD']['PREVIEW_PICTURE']):?>
                                    <?$image = CFile::GetPath($good['GOOD']['PREVIEW_PICTURE'] )?>
                                    <img src="<?echo $image;?>"/>
                                <?else:?>
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/no_photo_medium.png"/>
                                <?endif;?>
                                <div class="fast_view_block"
                                     data-event="jqm"
                                     data-param-form_id="fast_view"
                                     data-param-iblock_id="<?=$good['OFFER']['IBLOCK_ID'];?>"
                                     data-param-id="<?=$good['OFFER']['ID'];?>"
                                     data-param-item_href="<?=$good['GOOD']['DETAIL_PAGE_URL'];?>" data-name="fast_view">Быстрый просмотр</div>
                            </a>
                        </div>
                        <div class="item_info">
                            <div class="item-title">
                                <a href="<?=$good['GOOD']['DETAIL_PAGE_URL'];?>" class="dark_link">
                                    <span><?=$good['GOOD']['NAME'];?></span>
                                </a>
                            </div>
                            <div class="sa_block">
                                <div class="item-stock">
                                    <?if($good['OFFER_QUANTITY']['QUANTITY']):?>
                                        <span class="icon stock"></span>
                                        <span class="value">Много</span>
                                    <?else:?>
                                        <span class="icon order"></span>
                                        <span class="value">Нет в наличии</span>
                                    <?endif;?>
                                </div>
                            </div>
                            <div class="cost prices clearfix">
                                <div class="price">
                                    от
                                    <span class="values_wrapper">
                                        <?=CurrencyFormat($good['PRICE']['PRICE'], $good['PRICE']["CURRENCY"])?>
                                    </span>
                                    <span class="price_measure">/шт</span>
                                </div>
                            </div>
                        </div>
                        <div class="footer_button" style="">
                            <a class="btn btn-default basket read_more"
                               rel="nofollow"
                               href="<?=$good['GOOD']['DETAIL_PAGE_URL'];?>"
                               data-item="<?=$good['OFFER']['ID'];?>">Подробнее</a>
                        </div>
                    </div>
                </li>
            <?endforeach;?>
        </ul>
    </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('.tabs li[data-code="BUY_WITH_IT"]').remove();
  })
</script>
