<section class="popup js-text-director-popup text-director-popup">
    <div class="form text-director-popup-body">
        <div class="text-director-popup-form">
	        <?$APPLICATION->IncludeComponent(
		        "bitrix:form.result.new",
		        "popup",
		        Array(
			        "CACHE_TIME" => "3600",
			        "CACHE_TYPE" => "A",
			        "CHAIN_ITEM_LINK" => "",
			        "CHAIN_ITEM_TEXT" => "",
			        "COMPOSITE_FRAME_MODE" => "A",
			        "COMPOSITE_FRAME_TYPE" => "AUTO",
			        "EDIT_URL" => "result_edit.php",
			        "IGNORE_CUSTOM_TEMPLATE" => "N",
			        "LIST_URL" => "result_list.php",
			        "SEF_MODE" => "N",
			        "SUCCESS_URL" => "/thanks.php",
			        "USE_EXTENDED_ERRORS" => "N",
			        "VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
			        "WEB_FORM_ID" => "13"
		        )
	        );?>
        </div>
    </div>
</section>
