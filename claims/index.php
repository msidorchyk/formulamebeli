<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Составление рекламации после покупки мебели в Формула мебели");
$APPLICATION->SetTitle("Составить рекламацию");
?>
<?
use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();
$form_id = $request->getQuery("form_id");
if (empty($form_id)) {
	$form_id = 'CLAIMS_KORPUSNAYA';
}
?>
<script>
	$(document).ready(function () {
		$('#CLAIMS_FORM_SELECT').change(function () {
			window.location.href = "?form_id="+$(this).val();
		});
	});
</script>
<section id="claim-form">
	<div class="maxwidth-theme">
		<h3>1. Выберите вид мебели:</h3>
		<form method="get" action="">
			<select id="CLAIMS_FORM_SELECT" name="form_id">
				<option <?=($form_id=="CLAIMS_KORPUSNAYA"?'selected ':'')?>value="CLAIMS_KORPUSNAYA">Рекламация. Форма для корпусной мебели</option>
				<option <?=($form_id=="CLAIMS_MYAGKAYA_MATRAS"?'selected ':'')?>value="CLAIMS_MYAGKAYA_MATRAS">Рекламация. Форма для мягкой мебели и матрасов</option>
			</select>
		</form>
	</div>
	<br>
	<? if ($form_id == 'CLAIMS_KORPUSNAYA' || $form_id == 'CLAIMS_MYAGKAYA_MATRAS') : ?>
		<?$APPLICATION->IncludeComponent(
			"bitrix:form.result.new",
			"inline",
			Array(
				"WEB_FORM_ID" => $form_id,
				"IGNORE_CUSTOM_TEMPLATE" => "N",
				"USE_EXTENDED_ERRORS" => "N",
				"SEF_MODE" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600000",
				"LIST_URL" => "",
				"EDIT_URL" => "",
				"SUCCESS_URL" => "",
				"CHAIN_ITEM_TEXT" => "",
				"CHAIN_ITEM_LINK" => "",
				"VARIABLE_ALIASES" => Array("WEB_FORM_ID" => "WEB_FORM_ID", "RESULT_ID" => "RESULT_ID"),
				"AJAX_MODE" => "Y",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"HIDE_SUCCESS" => "N",
			)
		);?>
		<?
		/*$APPLICATION->IncludeComponent(
			"bitrix:form",
			"claims",
			Array(
				"AJAX_MODE" => "Y",
				"SEF_MODE" => "N",
				"WEB_FORM_ID" => $form_id,
				"START_PAGE" => "new",
				"SHOW_LIST_PAGE" => "N",
				"SHOW_EDIT_PAGE" => "N",
				"SHOW_VIEW_PAGE" => "N",
				"SUCCESS_URL" => "",
				"SHOW_ANSWER_VALUE" => "Y",
				"SHOW_ADDITIONAL" => "N",
				"SHOW_STATUS" => "N",
				"EDIT_ADDITIONAL" => "N",
				"EDIT_STATUS" => "N",
				"HIDE_SUCCESS" => "Y",
				"NOT_SHOW_FILTER" => "",
				"NOT_SHOW_TABLE" => "",
				"CHAIN_ITEM_TEXT" => "",
				"CHAIN_ITEM_LINK" => "",
				"IGNORE_CUSTOM_TEMPLATE" => "N",
				"USE_EXTENDED_ERRORS" => "Y",
				"CACHE_GROUPS" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "3600000",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
			)
		);*/
		?>
	<? endif; ?>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
