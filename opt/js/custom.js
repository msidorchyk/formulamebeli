(function ($) {

  "use strict";

    // PRE LOADER
    $(window).load(function(){
      $('.preloader').fadeOut(1000); // set duration in brackets    
    });


    // MENU
    $('.navbar-collapse a').on('click',function(){
      $(".navbar-collapse").collapse('hide');
    });

    $(window).scroll(function() {
      if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
          } else {
            $(".navbar-fixed-top").removeClass("top-nav-collapse");
          }
    });
      $('body').on('submit', 'form', function(event){
      var $form = $(this);
      event.preventDefault();
      var data = $form.serialize();
      $.ajax({
          url: '/ajax/send.php',
          data: data,
          method: 'POST',
          beforeSend: function () {
              $form.find('.submit-btn').addClass('process');
              $form.find('.submit-btn').text('Идет отправка');
              $form.find('.submit-btn').val('Идет отправка');
          },
          success: function (xhr) {
              dataLayer.push({'event': 'form'});
              setTimeout(function () {
                  console.log(data);
                  $form.find('.submit-btn').removeClass('process').addClass('success');
                  $form.find('.submit-btn').text('Заявка отправлена!');
                  $form.find('.submit-btn').val('Заявка отправлена!');
                  $form.find('.submit-btn').attr('disabled');
                  setTimeout(function () {
                    $form.find('input[type="text"]').val('');
                    $form.find('input[type="email"]').val('');
                    $form.find('input[type="phone"]').val('');
                      $form.find('.submit-btn').removeClass('process').removeClass('success');
                      $form.find('.submit-btn').text('Отправить');
                      $form.find('.submit-btn').val('Отправить');
                  }, 4500);
              }, 2000);
          },
          error: function (xhr) {
              $form.find('.submit-btn').css({background: '#aa4400', color: '#fff'});
              $form.find('.submit-btn').removeClass('process').removeClass('success');
              $form.find('.submit-btn').text('Форма не отправилась ((');
              console.log(xhr);
          }
      })
      return false;
    })

    // HOME SLIDER & COURSES & CLIENTS
    $('.home-slider').owlCarousel({
      animateOut: 'fadeOut',
      items:1,
      loop:true,
      dots:false,
      autoplayHoverPause: false,
      autoplay: true,
      smartSpeed: 1000,
    })

    $('.owl-courses').owlCarousel({
      animateOut: 'fadeOut',
      loop: true,
      autoplayHoverPause: false,
      autoplay: true,
      smartSpeed: 1000,
      dots: false,
      nav:true,
      navText: [
          '<i class="fa fa-angle-left"></i>',
          '<i class="fa fa-angle-right"></i>'
      ],
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
        },
        1000: {
          items: 3,
        }
      }
    });

    $('.owl-client').owlCarousel({
      animateOut: 'fadeOut',
      loop: true,
      autoplayHoverPause: false,
      autoplay: true,
      smartSpeed: 1000,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
        },
        1000: {
          items: 3,
        }
      }
    });


    // SMOOTHSCROLL
    $(function() {
      $('.custom-navbar a, #home a').on('click', function(event) {
        var $anchor = $(this);
          $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 49
          }, 1000);
            event.preventDefault();
      });
    });  

})(jQuery);
