<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "О компании — мебельный интернет-магазин «Формула мебели»");
$APPLICATION->SetTitle("О компании");
CModule::IncludeModule("fileman");
CMedialib::Init();
$certificates_collection = CMedialibItem::GetList(array('arCollections' => array("0" => 3)));
?><div class="container static-page tt">
	<div class="container static-page js-cert" style="opacity: 0;">
	</div>
	<p>
		 Что такое «Формула Мебели»? Надежность. Качество. Красота. Универсальность. Невысокие цены. Гарантия. Быстрая доставка. Подъем и сборка. Сумма этих условий равна удачной покупке.
	</p>
	<p>
	</p>
	<p>
		 Это формула того, как сделать мебель доступной каждому. С «Формула Мебели» можно обставить квартиру по доступной цене, но без потери качества, быстро и с хорошим сервисом.
	</p>
 <span style="color: #000000;"> </span>
	<p>
		 Наша цель – дать возможность людям покупать хорошую мебель по минимальным ценам. Мы хотим, чтобы в любой ситуации каждый мог позволить себе купить новый диван, прихожую, кухню и все, что понадобится для создания комфорта в его квартире. Для этого не нужно собирать деньги, можно купить сейчас, а при необходимости оформить рассрочку без переплат.
	</p>
	<p>
		 Вот уже 10 лет мы работаем над этим, растем и совершенствуемся. Мы создали большой прочный воздушный шар из лоскутов счастья наших покупателей. На этом шаре мы приносим возможность приобретать хорошую мебель по доступным ценам в новые места, в новые города, в новые районы, чтобы все люди смогли воспользоваться нашим предложением.
	</p>
	<p>
	</p>
	<p>
		 На сегодняшний день уже 34 салона в 16 городах Пермского края работают для вас. Мы не собираемся останавливаться на достигнутом, потому что людям нравится наша мебель и то, как мы ее продаем.
	</p>
	<div class="about_icon_container">
		<div class="about_icon_title">
			 Почему нас выбирают?
		</div>
		<div class="about_icon_column_container clear">
			<div class="about_icon_column">
				<div class="about_icon_image">
 <img width="64" alt="Доступные цены" src="/upload/medialibrary/9f4/9f4ec85a14064543bbcf877ae1c4cd54.png" height="64" title="Доступные цены">
				</div>
				<div class="about_icon_text">
 <b>Доступные цены</b><br>
 <br>
					 Закупаем мебель в больших&nbsp;<br>
					 количествах, за счет чего<br>
					 имеем лучшие цены
				</div>
			</div>
			<div class="about_icon_column">
				<div class="about_icon_image">
 <img width="64" alt="Широкий ассортимент" src="/upload/medialibrary/da1/da1f2212240c61941f93c83f89c108ff.png" height="64" title="Широкий ассортимент">
				</div>
				<div class="about_icon_text">
 <b>Широкий ассортимент</b><br>
 <br>
					 Работаем <br>
					 напрямую с 43 лучшими <br>
					российскими&nbsp;фабриками
				</div>
			</div>
			<div class="about_icon_column about_icon_column_last">
				<div class="about_icon_image">
 <img width="64" alt="Собственные склады" src="/upload/medialibrary/2de/2de831beaf5f50a3c4a4d23fc010b495.png" height="64" title="Собственные склады">
				</div>
				<div class="about_icon_text">
 <b>Собственные склады</b><br>
 <br>
					 Склады в&nbsp;Перми и&nbsp;Соликамске <br>
					 с постоянно пополняемым<br>
					 запасом продукции<br>
 <br>
 <br>
				</div>
			</div>
		</div>
		<div class="about_icon_column_container clear">
			<div class="about_icon_column">
				<div class="about_icon_image">
 <img width="64" alt="Сертификаты качества" src="/upload/medialibrary/4c9/4c90f60b361a0c39e0485e760555dcba.png" height="64" title="Сертификаты качества">
				</div>
				<div class="about_icon_text">
 <b>Сертификаты качества</b><br>
 <br>
					 Все наши поставщики имеют<br>
					 сертификаты качества на<br>
					 выпускаемую продукцию<br>
 <br>
 <br>
				</div>
			</div>
			<div class="about_icon_column">
				<div class="about_icon_image">
 <img width="64" alt="Гарантийное обслуживание" src="/upload/medialibrary/51f/51f91f2fc2ca1fd7987447c2fabdcb98.png" height="64" title="Гарантийное обслуживание">
				</div>
				<div class="about_icon_text">
 <b>Гарантийное обслуживание</b><br>
 <br>
					 Предоставляем гарантию<br>
					 на продаваемую продукцию<br>
 <br>
 <br>
				</div>
			</div>
			<div class="about_icon_column about_icon_column_last">
				<div class="about_icon_image">
 <img width="64" alt="17 лет на рынке" src="/upload/medialibrary/f11/f118f7e1677c58ef7be74e56373e260e.png" height="64" title="17 лет на рынке">
				</div>
				<div class="about_icon_text">
					<b>10 лет на рынке</b><br>
 <br>
					Являемся крупнейшей<br>
					 мебельной компанией<br>
					 Пермского края<br>
 <br>
 <br>
				</div>
			</div>
		</div>
		<div class="about_icon_column_container clear">
			<div class="about_icon_column">
				<div class="about_icon_image">
 <img width="64" alt="Беспроцентная рассрочка" src="/upload/medialibrary/348/348c31f18e0c88564ff79eb68dcf26a9.png" height="64" title="Беспроцентная рассрочка">
				</div>
				<div class="about_icon_text">
 <b>Беспроцентная рассрочка</b><br>
 <br>
					 Оформим беспроцентную<br>
					 рассрочку без банков и <br>
					без переплат<br>
 <br>
 <br>
				</div>
			</div>
			<div class="about_icon_column">
				<div class="about_icon_image">
 <img width="64" src="/upload/medialibrary/e22/e225e806abe746e55e700535ad7011e5.png" height="64" title="Акции" alt="Акции">
				</div>
				<div class="about_icon_text">
 <b>Акции</b><br>
 <br>
					 Регулярно&nbsp;проводим<br>
					 акции и распродажи<br>
				</div>
			</div>
			<div class="about_icon_column about_icon_column_last">
				<div class="about_icon_image">
 <img width="64" alt="Своя сервисная служба" src="/upload/medialibrary/662/662ae60a5ddeccc2094307c735fbca80.png" height="64" title="Своя сервисная служба">
				</div>
				<div class="about_icon_text">
 <b>Сервисная служба</b><br>
 <br>
					 Собственный автопарк, <br>
					 профессиональные сборщики <br>
					 и грузчики в штате<br>
 <br>
 <br>
				</div>
			</div>
		</div>
	</div>
 <br>
 <br>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>