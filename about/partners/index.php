<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сотрудничество");
?><div class="container static-page">
	 «Формула Мебели»&nbsp;– большая и активно развивающаяся компания на мебельном рынке Пермского края. Наши магазины присутствуют в <a href="/contact-information">16 городах Пермского края</a>.&nbsp;Мы&nbsp;сотрудничаем&nbsp;с десятками поставщиков и производителей мебели. Приглашаем и вас к сотрудничеству для реализации мебели через нашу торговую сеть.<br>
	<h2>Рекламным агентствам</h2>
	<p>
		 Если у вас масса идей, высокий уровень квалификации или вы являетесь представителем дизайнерской компании, мы всегда рады рассмотреть ваши предложения о сотрудничестве.
	</p>
	<p>
		 Вы являетесь агентством по продвижению других компаний, сфера деятельности которых пересекается с нашей? Наш сайт к вашим услугам. Мы готовы обсудить варианты представленности ваших компаний на наших страницах.
	</p>
	<p>
		 Если у вас есть какие-либо другие предложения, мы готовы их рассмотреть.
	</p>
	<p>
 <b>Александра Россихина&nbsp;&nbsp;</b>
	</p>
	<p>
		 Электронная почта:&nbsp;<a href="mailto:v.vadim@formulamebeli.com">m.marketing@formulamebeli.com</a>
	</p>
 <br>
	<h2>Поставщикам</h2>
	<p>
		 Основой для принятия решения о закупке мебели и сопутствующих товаров служит необходимое качество материалов, конкурентная цена и предлагаемый поставщиком сервис. Мы всегда готовы рассмотреть альтернативные предложения на поставку мебели. Закупку товара, согласование условий сотрудничества для нашей&nbsp;компании осуществляет отдел закупок.
	</p>
	<p>
 <b>Екатерина Федотова</b>
	</p>
	<p>
		 Телефон:&nbsp; +7-919-700-13-29
	</p>
	<p>
		 Электронная почта:&nbsp; <a href="mailto:fedotova_ea@formulamebeli.com">fedotova_ea@formulamebeli.com</a>
	</p>
 <br>
	<h2>Аренда помещений</h2>
	<p>
		 Компания&nbsp;«Формула Мебели»&nbsp;&nbsp;готова рассмотреть предложения по аренде площадей для размещения магазинов. Предлагаемые или рассматриваемые объекты должны соответствовать <a href="/upload/rent.pdf">требованиям</a>.
	</p>
	<p>
		 Электронная почта: <a href="mailto:arenda@formulamebeli.com">arenda@formulamebeli.com</a>
	</p>
 <br>
	<h2>Корпоративные продажи</h2>
	<p>
		 Если вашей организации нужна офисная или гостиничная мебель, мебель для кабинета руководителя, а также готовые интерьерные решения для вашего офиса, мы будем рады вам помочь. Специалисты отдела корпоративных продаж помогут подобрать наиболее оптимальный вариант, исходя из ваших пожеланий и потребностей. <a href="https://formulamebeli.com/opt/">Подробнее...</a>
	</p>
	<p>
 <b>Алёна Гришина&nbsp;</b>
	</p>
	<p>
		 Телефон: +7-922-242-01-63
	</p>
	<p>
		 Электронная почта: <a href="mailto:opt.perm@formulamebeli.com">opt.perm@formulamebeli.com</a>
	</p>
 <br>
	<h2>Партнерство в доставке</h2>
	<p>
		 Приглашаем к сотрудничеству фирмы и индивидуальных предпринимателей Пермского края, оказывающих услуги по доставке мебели.
	</p>
	<p>
 <b>Павел Медведев&nbsp;</b>
	</p>
	<p>
		 Телефон: +7-982-481-01-63
	</p>
	<p>
		 Электронная почта: <a href="mailto:service@formulamebeli.com">service@formulamebeli.com</a>
	</p>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>