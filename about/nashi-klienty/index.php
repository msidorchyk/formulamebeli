<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Наши клиенты");
?><div class="container static-page">
	<p>
		 Наши клиенты — это не только многочисленные жители Пермского края и соседних регионов, но и предприятия, школы, больницы и другие учреждения края. Салоны «Формула Мебели» есть уже в 16 городах Пермского края.
	</p>
	<p>
		 Нас выбирают&nbsp;не только потому, что мы даем лучшие цены на широкий ассортимент мебели и осуществляем бесплатную доставку на следующий день. У нас есть собственная сервисная служба — профессиональные водители, грузчики, сборщики. Гарантия на купленную у нас мебель составляет от 18 месяцев до 25 лет.
	</p>
	<p>
		 Мы благодарны нашим клиентам и считаем их нашими партнерами.
	</p>
	<p>
		 Присоединяйтесь!
	</p>
	<div style="text-align: center;">
		<p>
 <img width="200" alt="uralkalii.png" src="/upload/medialibrary/9e7/9e7341d2d5c320eb9496928ba13d5ff6.png" height="133" title="uralkalii.png">&nbsp; &nbsp; &nbsp;<img width="200" alt="sol-magn-zavod.png" src="/upload/medialibrary/be1/be16efd3e6516a04e3c20316cd3ca1b1.png" height="143" title="sol-magn-zavod.png">&nbsp; &nbsp; &nbsp;<img width="200" alt="uralhim.png" src="/upload/medialibrary/113/113ed289d57bd322a56286a91e73f3df.png" height="151" title="uralhim.png">&nbsp; &nbsp; &nbsp;<img width="150" alt="goznak.png" src="/upload/medialibrary/1c4/1c4d6c21625659629d67233d87f9c7a0.png" height="147" title="goznak.png"><br>
		</p>
		<p>
 <br>
		</p>
		<p>
 <img width="200" alt="solbumprom.png" src="/upload/medialibrary/824/824400a98d2d48fe98c4271517c63597.png" height="43" title="solbumprom.png">&nbsp; &nbsp; &nbsp;<img width="200" alt="avisma.png" src="/upload/medialibrary/347/347cf3c4aa837cdcbac287bcca84edd6.png" height="102" title="avisma.png">&nbsp; &nbsp; &nbsp;<img width="125" alt="sibur.png" src="/upload/medialibrary/09a/09ae8f5b21d779aabb6df7aecbd579e3.png" height="131" title="sibur.png">&nbsp; &nbsp; &nbsp;<img width="200" alt="metafrax.png" src="/upload/medialibrary/0f5/0f52aab999cbc2de1b67e0b3ae51d0bc.png" height="60" title="metafrax.png"><br>
		</p>
		<p>
 <br>
		</p>
		<p>
 <img width="150" alt="telec.png" src="/upload/medialibrary/40a/40a97d3f220339acc3aee870e0a63d36.png" height="150" title="telec.png">&nbsp; &nbsp; &nbsp;<img width="200" alt="molochnyi.png" src="/upload/medialibrary/356/356eeffe25ba4252aadd70d8d029b2d5.png" height="202" title="molochnyi.png">&nbsp; &nbsp; &nbsp;<img width="200" alt="kungurskii.png" src="/upload/medialibrary/554/554138a6af98885ccc26b68338948a28.png" height="130" title="kungurskii.png">&nbsp; &nbsp; &nbsp;<img width="200" alt="chmz.png" src="/upload/medialibrary/f63/f631b5e6f27db6139b3c307c2350f970.png" height="200" title="chmz.png"><br>
		</p>
		<p>
 <br>
		</p>
		<p>
 <br>
		</p>
		<p>
 <img width="200" alt="kama.png" src="/upload/medialibrary/cef/cef4734700144c73d46ac9e740586b4c.png" height="67" title="kama.png">&nbsp; &nbsp; &nbsp;<img width="200" alt="motoviliha.png" src="/upload/medialibrary/7f5/7f554b9240b2f032937dacb183a91dd2.png" height="79" title="motoviliha.png">&nbsp; &nbsp; &nbsp;<img width="200" alt="gres.png" src="/upload/medialibrary/541/541be8f3af7a681140f407fddb329fae.png" height="64" title="gres.png">&nbsp; &nbsp; &nbsp;<img width="200" alt="solgb.png" src="/upload/medialibrary/e7c/e7c2e9e79bc3a2684f076ad093ab6e5a.png" height="102" title="solgb.png"><br>
		</p>
		<p>
 <br>
		</p>
		<p>
 <br>
		</p>
		<p>
 <img width="400" alt="gimnaziya.jpg" src="/upload/medialibrary/e1c/e1c73fec962c4cfba67f59abd8f090af.jpg" height="64" title="gimnaziya.jpg">&nbsp; &nbsp; &nbsp;<img width="400" alt="detsad.png" src="/upload/medialibrary/f5b/f5bf174a2e5f806e964d44ec1fa71e9f.png" height="96" title="detsad.png"><br>
		</p>
	</div>
 <br>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>