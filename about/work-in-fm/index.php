<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Работа у нас");
?><a href="https://docs.google.com/forms/d/e/1FAIpQLSf_hldRMaGpS-uYsB41lJGQaFlqQhASENGlg4gZLNdCEtGWsQ/viewform" target="_blank">
<p style=" padding: 10px 20px; text-align: right;">
 <b><span style="font-size: 13pt; color:#393185"><u>Заполнить анкету на вакансию →</u></span></b>
</p>
 </a>
<div class="container static-page">
	<p>
	</p>
	<div>
		<p>
			 Сеть мебельных салонов компании «Формула Мебели» является одной из самый крупных в Пермском крае. Наши 34 магазина присутствуют в 16 городах, и мы продолжаем расширяться, поэтому находимся в поисках новых ярких кадров.
		</p>
		<p>
			 Компания «Формула Мебели» открыта для целеустремлённых, энергичных и ответственных людей. В наши ряды мы приглашаем профессионалов и специалистов, готовых работать в команде и твёрдо идти к вершинам успеха. Работа у нас – это возможность карьерного и&nbsp;профессионального роста&nbsp;каждого работника. Наша компания создаёт все необходимые условия для плодотворной и интересной работы. Мы предоставляет всем сотрудникам равные права и карьерные возможности.
		</p>
	</div>
	<p>
 <b><span style="color: #000000;">Мы гарантируем</span></b><span style="color: #000000;"> </span>
	</p>
 <span style="color: #000000;"> </span>
	<p>
 <span style="color: #000000;"> </span>
	</p>
	<ul>
		<li>
		<p style="color: #000000;">
			 Реальную возможность карьерного роста
		</p>
 </li>
		<li>
		<p>
 <span style="color: #000000;">Бесплатное обучение, направленное на профессиональное развитие</span>
		</p>
 </li>
		<li>
		<p>
 <span style="color: #000000;">Достойную заработную плату</span>
		</p>
 </li>
		<li>
		<p>
 <span style="color: #000000;">Возможность влиять на свой уровень дохода</span>
		</p>
 </li>
		<li>
		<p>
 <span style="color: #000000;">Выбор места работы с учетом вашего места жительства</span>
		</p>
 </li>
	</ul>
	<p>
 <b><span style="color: #000000;">Мы приглашаем:</span></b>
	</p>
	<ul>
		<li>
		<p>
 <span style="color: #000000;">Директоров магазина</span>
		</p>
 </li>
		<li>
		<p>
 <span style="color: #000000;">Продавцов-консультантов</span>
		</p>
 </li>
		<li>
		<p>
 <span style="color: #000000;">Кладовщиков</span>
		</p>
 </li>
		<li>
		<p>
 <span style="color: #000000;">Грузчиков</span>
		</p>
 </li>
		<li>
		<p>
 <span style="color: #000000;">Водителей-экспедиторов</span>
		</p>
 </li>
		<li>
		<p>
 <span style="color: #000000;">Сборщиков мебели</span>
		</p>
 </li>
	</ul>
	<p>
 <span style="color: #000000;"> </span>
	</p>
	<p>
 <span style="color: #000000;"> По вопросам трудоустройства обращайтесь к Старостиной Светлане Викторовне<br>
 </span>
	</p>
	<p>
 <span style="color: #000000;">Телефон: +7 (982) 481-03-53</span>
	</p>
	<p>
 <span style="color: #000000;"> Электронная почта:</span>&nbsp;<a href="mailto:personal@formulamebeli.com">personal@formulamebeli.com</a>
	</p>
	 Мы обязательно рассмотрим ваше резюме в течение 14 дней. Отсутствие ответа в течение двух недель означает, что, к сожалению, на сегодняшний день у нас нет подходящих вакансий. При этом ваше резюме будет сохранено в нашей информационной системе. Если подходящая вакансия появится в будущем, мы предложим ее вам для рассмотрения.
	<p>
	</p>
	<div class="b-workcv">
 <a class="b-workcv__link" href="https://docs.google.com/forms/d/e/1FAIpQLSf_hldRMaGpS-uYsB41lJGQaFlqQhASENGlg4gZLNdCEtGWsQ/viewform" target="_blank">Заполнить анкету →</a>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>