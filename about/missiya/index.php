<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Наша миссия");
?>
 <div class="container static-page">
     <div class="b-page__wrapper">
         <h3 class="b-mission__p">
             Мы хотим быть лучшими на мебельном рынке Урала и Поволжья.
         </h3>
         <h3 class="b-mission__p">
             Мы хотим быть лучшими по качеству предоставляемых услуг нашим клиентам и быстроте реагирования на их запросы.
         </h3>
         <h3 class="b-mission__p">
             Мы благодарны нашим клиентам и считаем их нашими партнерами.
         </h3>
     </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>