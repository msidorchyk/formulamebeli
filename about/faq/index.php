<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Часто задаваемые вопросы");

$faq_sections_filter = Array('IBLOCK_ID'=>43, 'GLOBAL_ACTIVE'=>'Y');
$faq_sections = CIBlockSection::GetList(array('SORT_BY' => 'desc'), $faq_sections_filter);
?>
 <div class="container static-page">
 <?while ($section = $faq_sections->GetNext()) :?>
    <section class="faq js-shelf">
        <h3 class="h3"><?=$section['NAME'];?></h3>
        <ul class="faq-list">
        <?
            $faq_elements = CIBlockElement::GetList(array(), array('IBLOCK_ID'=>43, 'SECTION_ID'=>$section['ID'],'GLOBAL_ACTIVE'=>'Y'), false, false, array());
            while($element = $faq_elements->GetNextElement()):
                $fields = $element->GetFields(); ?>
            <li class="faq-list__item">
                <?=$fields['NAME']?>
                <blockquote>
                    <?=$fields['PREVIEW_TEXT'];?>
                </blockquote>
            </li>
            <?endwhile;?>
        </ul>
    </section>
 <?endwhile;?>
 </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>