<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Отзывы о мебельном салоне Формула мебели.");
$APPLICATION->SetPageProperty("title", "Мебель отзывы | Отзывы о мебельном магазине Формула мебели");
$APPLICATION->SetTitle("Отзывы");
?><p>
	 Мы будем очень рады и благодарны, если вы оставите свой отзыв о компании&nbsp;«Формула Мебели»! Чтобы это сделать, вам&nbsp;нужно выбрать кнопку «оставить отзыв», в открывшемся окне&nbsp;написать свое мнение о товаре, сервисе, впечатление о компании,&nbsp;указать имя или авторизоваться через любую социальную сеть, а затем нажать на кнопку «опубликовать». Спасибо!
</p>
 <br>
<div id="mc-review">
</div>
 <script type="text/javascript">
  cackle_widget = window.cackle_widget || [];
  cackle_widget.push({widget: 'Review', id: 42394});
  (function() {
    var mc = document.createElement('script');
    mc.type = 'text/javascript';
    mc.async = true;
    mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mc, s.nextSibling);
  })();
</script> <a id="mc-link" href="http://cackle.me">Отзывы для сайта <b style="color:#4FA3DA">Cackl</b><b style="color:#F65077">e</b></a> <br>
<p>
 <strong>15.05.2017 Евгения, Пермь</strong>
</p>
<p>
	 Добрый вечер! Хочется поблагодарить мальчиков сервисной службы Анатолия и Сергея. Приехали вовремя, позвонили заранее, вежливое общение, на все мои вопросы получила информацию. Большое им спасибо!!!
</p>
 <br>
<p>
 <strong>07.04.2017 Оксана</strong>
</p>
<p>
	 Здравствуйте Ирина! Спасибо Вам большое за помощь в выборе дивана! Нам очень понравился диван, доставили всю мебель во время и осмотрели все в порядке. Ещё раз Вам огромное спасибо за понимание таких капризных клиентов как мы!!! Очень рада, что есть такой интернет магазин и такие менеджеры как Вы Ирина!!!от Оксаны.
</p>
 <br>
<p>
 <strong>30.03.2017 Юлия, г. Соликамск</strong>
</p>
<p>
	 Доброго времени суток!!! Хотели бы выразить благодарность менеджеру Татьяне, работающей в гипермаркете по адресу ул. Всеобуча 176в. Приобрели товар, оказался бракованный, менеджер очень грамотно и оперативно разрешил нашу проблему, брак заменили на качественный товар в кратчайшие сроки - Огромнейшее Спасибо, профессионал своего дела!!! Отдельное спасибо хотелось бы выразить грузчикам, которые осуществляют доставку товара, очень вежливые, пунктуальные и аккуратные работники!!!! Будем и дальше приобретать товар в сети магазинов "Формула мебели"
</p>
 <br>
<p>
 <strong>21.03.2017 Артем, г. Пермь</strong>
</p>
<p>
	 Привезли сегодня Шкаф Купе Мираж11. Очень доволен! Хорошие грузчики! Спасибо!
</p>
 <br>
<p>
 <strong>03.02.2017 Ширинкина Л.В.</strong>
</p>
<p>
	 Спасибо сотрудникам за чуткое отношение к заказчику.
</p>
 <br>
<p>
 <strong>27.01.2017 Митрофанов Сергей Михайлович, г. Пермь</strong>
</p>
<p>
	 Выражаю благодарность сотрудникам сервисной службы за своевременную доставку и уважительное отношение к клиентам.
</p>
 <br>
<p>
 <strong>13.01.2017 Ольга, г. Пермь</strong>
</p>
<p>
	 Спасибо ребятам за доставку! Очень вежливые, исполнительные! Доставили в срок!
</p>
 <br>
<p>
 <strong>09.11.2016 Тамара Юрьевна</strong>
</p>
<p>
	 Урааа! Спасибо вам, а в особенности Александру Сергеевичу за его профессионализм. Вопрос решен очень оперативно
</p>
 <br>
<p>
 <strong>01.11.2016 Елена, г. Александровск</strong>
</p>
<p>
	 Хочу сказать большое спасибо Седеговой Ирине Владимировне продавцу интернет магазина. Я заказала диван она подобрала мне расцветку. привезли во время. все понравилось. Еще раз большое спасибо.
</p>
 <br>
<p>
 <strong>03.08.2016 Лариса, г. Кунгур</strong>
</p>
<p>
	 Отличный магазин сама бы хотела там работать.
</p>
 <br>
<p>
 <strong>01.04.2016 Анастасия, г. Краснокамск</strong>
</p>
<p>
	 Доброе утро! Хочу поблагодарить сотрудников Формулы мебели в краснокамске, а именно Валентину и Дмитрия, за то что отнеслись серьезно к моей проблеме и решили ее за быстрое время. Очень довольна их обслуживанием! Спасибо! Придем всей семьей еще раз!
</p>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>