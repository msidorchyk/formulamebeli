<?php

namespace Solar;

class Agent
{
    static public function doReindex()
    {
        \CModule::IncludeModule('search');
        set_time_limit(0);
        $progress = array();
        $max_execution_time = 10000;

        while (is_array($progress)) {
            $progress = \CSearch::ReIndexAll(true, $max_execution_time, $progress);
        }

        return '\\'.__METHOD__.'();';
    }
}
