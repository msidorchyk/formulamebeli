<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?$this->setFrameMode(true);?>
<link rel="stylesheet" type="text/css" href="/bitrix/templates/aspro_next/css/slick.css">
<link rel="stylesheet" type="text/css" href="/bitrix/templates/aspro_next/css/slick-theme.css">
<script type="text/javascript" src="/bitrix/templates/aspro_next/js/slick.min.js"></script>

<?if($arResult['ITEMS']):?>
	<script>
		$(document).ready(function() {
			$('#promo-slider').slick({
				autoplay: true,
				autoplaySpeed: 5500,
				lazyLoad: 'progressive',
				slidesToShow: 2,
				slidesToScroll: 2,
				responsive: [
					{
					  breakpoint: 480,
					  settings: {
						slidesToShow: 1,
						adaptiveHeight: true,
						arrows: false,
						dots: true,
					  }
					}
				]
			});
		});
	</script>
	<div class="slider" id="promo-slider">
		<?foreach($arResult['ITEMS'] as $i => $arItem):?>
			<div class="slide"><img src="<?=CFile::GetFileArray($arItem['PROPERTIES']['PROMO_IMAGE']['VALUE'])['SRC']?>" alt="MDN"></div>
			</a>
		<?endforeach;?>
	</div>
<?endif;?>