$(document).ready(function(){
	$("form.subscribe-form").validate({
		rules:{ "EMAIL": {email: true} },
		submitHandler: function( form ){
			console.log('subscribed');
			mindbox("async", {
				operation: "FooterSub",
				data: {
					customer: {

						email: $('.subscribe-input[name="EMAIL"]').val(),

						subscriptions: [
							{
								brand: "formulamebeli",
								pointOfContact: "Email",

							},

						]
					}
				},
			});
		}
	});
})