<?
use Bitrix\Main\Loader,
	Bitrix\Main\ModuleManager;
?>
<?CNext::AddMeta(
	array(
		'og:description' => $arElement['PREVIEW_TEXT'],
		'og:image' => (($arElement['PREVIEW_PICTURE'] || $arElement['DETAIL_PICTURE']) ? CFile::GetPath(($arElement['PREVIEW_PICTURE'] ? $arElement['PREVIEW_PICTURE'] : $arElement['DETAIL_PICTURE'])) : false),
	)
);?>
<?$sViewElementTemplate = ($arParams["ELEMENT_TYPE_VIEW"] == "FROM_MODULE" ? $arTheme["CATALOG_PAGE_DETAIL"]["VALUE"] : $arParams["ELEMENT_TYPE_VIEW"]);?>
<?$hide_left_block = ($arTheme["LEFT_BLOCK_CATALOG_DETAIL"]["VALUE"] == "Y" ? "N" : "Y");
$arWidePage = array("element_3", "element_4", "element_5");

//set offer view type
$typeTmpDetail = 0;
if($arSection['UF_ELEMENT_DETAIL'])
	$typeTmpDetail = $arSection['UF_ELEMENT_DETAIL'];
else
{
	if($arSection["DEPTH_LEVEL"] > 2)
	{
		$sectionParent = CNextCache::CIBlockSection_GetList(array('CACHE' => array("MULTI" =>"N", "TAG" => CNextCache::GetIBlockCacheTag($arParams["IBLOCK_ID"]))), array('GLOBAL_ACTIVE' => 'Y', "ID" => $arSection["IBLOCK_SECTION_ID"], "IBLOCK_ID" => $arParams["IBLOCK_ID"]), false, array("ID", "IBLOCK_ID", "NAME", "UF_ELEMENT_DETAIL"));
		if($sectionParent['UF_ELEMENT_DETAIL'] && !$typeTmpDetail)
			$typeTmpDetail = $sectionParent['UF_ELEMENT_DETAIL'];

		if(!$typeTmpDetail)
		{
			$sectionRoot = CNextCache::CIBlockSection_GetList(array('CACHE' => array("MULTI" =>"N", "TAG" => CNextCache::GetIBlockCacheTag($arParams["IBLOCK_ID"]))), array('GLOBAL_ACTIVE' => 'Y', "<=LEFT_BORDER" => $arSection["LEFT_MARGIN"], ">=RIGHT_BORDER" => $arSection["RIGHT_MARGIN"], "DEPTH_LEVEL" => 1, "IBLOCK_ID" => $arParams["IBLOCK_ID"]), false, array("ID", "IBLOCK_ID", "NAME", "UF_ELEMENT_DETAIL"));
			if($sectionRoot['UF_ELEMENT_DETAIL'] && !$typeTmpDetail)
				$typeTmpDetail = $sectionRoot['UF_ELEMENT_DETAIL'];
		}
	}
	else
	{
		$sectionRoot = CNextCache::CIBlockSection_GetList(array('CACHE' => array("MULTI" =>"N", "TAG" => CNextCache::GetIBlockCacheTag($arParams["IBLOCK_ID"]))), array('GLOBAL_ACTIVE' => 'Y', "<=LEFT_BORDER" => $arSection["LEFT_MARGIN"], ">=RIGHT_BORDER" => $arSection["RIGHT_MARGIN"], "DEPTH_LEVEL" => 1, "IBLOCK_ID" => $arParams["IBLOCK_ID"]), false, array("ID", "IBLOCK_ID", "NAME", "UF_ELEMENT_DETAIL"));
		if($sectionRoot['UF_ELEMENT_DETAIL'] && !$typeTmpDetail)
			$typeTmpDetail = $sectionRoot['UF_ELEMENT_DETAIL'];
	}
}
if($typeTmpDetail)
{
	$rsTypes = CUserFieldEnum::GetList(array(), array("ID" => $typeTmpDetail));
	if($arType = $rsTypes->GetNext())
		$typeDetail = $arType['XML_ID'];
	if($typeDetail)
		$sViewElementTemplate = $typeDetail;
}

if(in_array($sViewElementTemplate, $arWidePage))
	$hide_left_block = "Y";
?>
<?$APPLICATION->SetPageProperty("HIDE_LEFT_BLOCK", $hide_left_block)?>
<?if($arParams["USE_SHARE"] == "Y" && $arElement && !in_array($sViewElementTemplate, $arWidePage)):?>
	<?$this->SetViewTarget('product_share');?>
	<div class="line_block share top <?=($arParams['USE_RSS'] == 'Y' ? 'rss-block' : '');?>">
		<?$APPLICATION->IncludeFile(SITE_DIR."include/share_buttons.php", Array(), Array("MODE" => "html", "NAME" => GetMessage('CT_BCE_CATALOG_SOC_BUTTON')));?>
	</div>
	<?$this->EndViewTarget();?>
<?endif;?>
<?$isWideBlock = (isset($arParams["DIR_PARAMS"]["HIDE_LEFT_BLOCK"]) ? $arParams["DIR_PARAMS"]["HIDE_LEFT_BLOCK"] : "");?>
<?if($arParams['AJAX_MODE'] == 'Y' && strpos($_SERVER['REQUEST_URI'], 'bxajaxid') !== false):?>
	<script type="text/javascript">
		setStatusButton();
	</script>
<?endif;?>
<div class="catalog_detail detail<?=($isWideBlock == "Y" ? " fixed_wrapper" : "");?> <?=$sViewElementTemplate;?>" itemscope itemtype="http://schema.org/Product">
	<?@include_once('page_blocks/'.$sViewElementTemplate.'.php');?>
</div>

<?CNext::checkBreadcrumbsChain($arParams, $arSection, $arElement);?>
<div class="clearfix"></div>

<?$arAllValues=$arSimilar=$arAccessories=array();
$arShowTabs = array("element_1", "element_2");
if(!in_array($sViewElementTemplate, $arWidePage)):
/*similar goods*/
$arExpValues=CNextCache::CIBlockElement_GetProperty($arParams["IBLOCK_ID"], $ElementID, array("CACHE" => array("TAG" =>CNextCache::GetIBlockCacheTag($arParams["IBLOCK_ID"]))), array("CODE" => "EXPANDABLES"));
if($arExpValues){
	$arAllValues["EXPANDABLES"]=$arExpValues;
}
/*accessories goods*/
$arAccessories=CNextCache::CIBlockElement_GetProperty($arParams["IBLOCK_ID"], $ElementID, array("CACHE" => array("TAG" =>CNextCache::GetIBlockCacheTag($arParams["IBLOCK_ID"]))), array("CODE" => "ASSOCIATED"));
if($arAccessories){
	$arAllValues["ASSOCIATED"]=$arAccessories;
}
?>

<?if($arAccessories || $arExpValues || (ModuleManager::isModuleInstalled("sale") && (!isset($arParams['USE_BIG_DATA']) || $arParams['USE_BIG_DATA'] != 'N'))){?>
	<?$bViewBlock = ($arParams["VIEW_BLOCK_TYPE"] == "Y");?>
	<?
	$arTab=array();
	if($arExpValues){
		$arTab["EXPANDABLES"]=($arParams["DETAIL_EXPANDABLES_TITLE"] ? $arParams["DETAIL_EXPANDABLES_TITLE"] : GetMessage("EXPANDABLES_TITLE"));
	}
	if($arAccessories){
		$arTab["ASSOCIATED"]=( $arParams["DETAIL_ASSOCIATED_TITLE"] ? $arParams["DETAIL_ASSOCIATED_TITLE"] : GetMessage("ASSOCIATED_TITLE"));
	}
	/* Start Big Data */
	if(ModuleManager::isModuleInstalled("sale") && (!isset($arParams['USE_BIG_DATA']) || $arParams['USE_BIG_DATA'] != 'N'))
		$arTab["RECOMENDATION"]=GetMessage("RECOMENDATION_TITLE");
	?>
	<?if($isWideBlock == "Y"):?>
		<div class="row">
			<div class="col-md-9">
	<?endif;?>


	<?if($bViewBlock):?>
		<div class="bottom_slider specials tab_slider_wrapp block_v">
            <?if($arTab):?>
				<?foreach($arTab as $code=>$title):?>
					<div class="wraps">
						<hr>
						<h4><?=$title;?></h4>
						<ul class="slider_navigation top custom_flex border">
							<li class="tabs_slider_navigation <?=$code?>_nav cur" data-code="<?=$code?>"></li>
						</ul>
                        <ul class="tabs_content">
                            <li class="tab <?=$code?>_wrapp cur" data-code="<?=$code?>">
								<?if($code=="RECOMENDATION"){?>
									<?
									$GLOBALS["CATALOG_CURRENT_ELEMENT_ID"] = $ElementID;
									?>
									<?$APPLICATION->IncludeComponent("bitrix:catalog.bigdata.products", CNext::checkVersionExt(), array(
										"USE_REGION" => ($arRegion ? "Y" : "N"),
										"STORES" => $arParams['STORES'],
										"LINE_ELEMENT_COUNT" => 5,
										"TEMPLATE_THEME" => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
										"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
										"BASKET_URL" => $arParams["BASKET_URL"],
										"ACTION_VARIABLE" => (!empty($arParams["ACTION_VARIABLE"]) ? $arParams["ACTION_VARIABLE"] : "action")."_cbdp",
										"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
										"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
										"SHOW_MEASURE_WITH_RATIO" => $arParams["SHOW_MEASURE_WITH_RATIO"],
										"ADD_PROPERTIES_TO_BASKET" => "N",
										"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
										"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
										"SHOW_OLD_PRICE" => $arParams['SHOW_OLD_PRICE'],
										"SHOW_DISCOUNT_PERCENT" => $arParams['SHOW_DISCOUNT_PERCENT'],
										"PRICE_CODE" => $arParams['PRICE_CODE'],
										"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
										"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
										"PRODUCT_SUBSCRIPTION" => $arParams['PRODUCT_SUBSCRIPTION'],
										"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
										"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
										"SHOW_NAME" => "Y",
										"SHOW_IMAGE" => "Y",
										"SHOW_MEASURE" => $arParams["SHOW_MEASURE"],
										"SHOW_RATING" => $arParams["SHOW_RATING"],
										"MESS_BTN_BUY" => $arParams['MESS_BTN_BUY'],
										"MESS_BTN_DETAIL" => $arParams['MESS_BTN_DETAIL'],
										"MESS_BTN_SUBSCRIBE" => $arParams['MESS_BTN_SUBSCRIBE'],
										"MESS_NOT_AVAILABLE" => $arParams['MESS_NOT_AVAILABLE'],
										"PAGE_ELEMENT_COUNT" => $disply_elements,
										"SHOW_FROM_SECTION" => "N",
										"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
										"IBLOCK_ID" => $arParams["IBLOCK_ID"],
										"SALE_STIKER" => $arParams["SALE_STIKER"],
										"STIKERS_PROP" => $arParams["STIKERS_PROP"],
										"DEPTH" => "2",
										"CACHE_TYPE" => $arParams["CACHE_TYPE"],
										"CACHE_TIME" => $arParams["CACHE_TIME"],
										"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
										"SHOW_PRODUCTS_".$arParams["IBLOCK_ID"] => "Y",
										"ADDITIONAL_PICT_PROP_".$arParams["IBLOCK_ID"] => $arParams['ADD_PICT_PROP'],
										"LABEL_PROP_".$arParams["IBLOCK_ID"] => "-",
										"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
										'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
										"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
										"CURRENCY_ID" => $arParams["CURRENCY_ID"],
										"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
										"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
										"SECTION_ELEMENT_ID" => $arResult["VARIABLES"]["SECTION_ID"],
										"SECTION_ELEMENT_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
										"ID" => $ElementID,
										"PROPERTY_CODE_".$arParams["IBLOCK_ID"] => $arParams["LIST_PROPERTY_CODE"],
										"CART_PROPERTIES_".$arParams["IBLOCK_ID"] => $arParams["PRODUCT_PROPERTIES"],
										"RCM_TYPE" => (isset($arParams['BIG_DATA_RCM_TYPE']) ? $arParams['BIG_DATA_RCM_TYPE'] : ''),
										"DISPLAY_WISH_BUTTONS" => $arParams["DISPLAY_WISH_BUTTONS"],
										"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
										"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
										),
										false,
										array("HIDE_ICONS" => "Y", "ACTIVE_COMPONENT" => "Y")
									);
									?>
								<?}else{?>
									<div class="flexslider loading_state shadow border custom_flex top_right" data-plugin-options='{"animation": "slide", "animationSpeed": 600, "directionNav": true, "controlNav" :false, "animationLoop": true, "slideshow": false, "controlsContainer": ".tabs_slider_navigation.<?=$code?>_nav", "counts": [4,3,3,2,1]}'>
										<ul class="tabs_slider <?=$code?>_slides slides">
											<?$GLOBALS['arrFilter'.$code] = array( "ID" => $arAllValues[$code] );?>
											<?$APPLICATION->IncludeComponent(
												"bitrix:catalog.top",
												"main",
												array(
													"USE_REGION" => ($arRegion ? "Y" : "N"),
													"STORES" => $arParams['STORES'],
													"TITLE_BLOCK" => $arParams["SECTION_TOP_BLOCK_TITLE"],
													"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
													"IBLOCK_ID" => $arParams["IBLOCK_ID"],
													"SALE_STIKER" => $arParams["SALE_STIKER"],
													"STIKERS_PROP" => $arParams["STIKERS_PROP"],
													"SHOW_RATING" => $arParams["SHOW_RATING"],
													"FILTER_NAME" => 'arrFilter'.$code,
													"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
													"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
													"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
													"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
													"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
													"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
													"BASKET_URL" => $arParams["BASKET_URL"],
													"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
													"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
													"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
													"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
													"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
													"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
													"DISPLAY_WISH_BUTTONS" => $arParams["DISPLAY_WISH_BUTTONS"],
													"ELEMENT_COUNT" => $disply_elements,
													"SHOW_MEASURE_WITH_RATIO" => $arParams["SHOW_MEASURE_WITH_RATIO"],
													"SHOW_MEASURE" => $arParams["SHOW_MEASURE"],
													"LINE_ELEMENT_COUNT" => $arParams["TOP_LINE_ELEMENT_COUNT"],
													"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
													"PRICE_CODE" => $arParams['PRICE_CODE'],
													"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
													"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
													"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
													"PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"],
													"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
													"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
													"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
													"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
													"CACHE_TYPE" => $arParams["CACHE_TYPE"],
													"CACHE_TIME" => $arParams["CACHE_TIME"],
													"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
													"CACHE_FILTER" => $arParams["CACHE_FILTER"],
													"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
													"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
													"OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],
													"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
													"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
													"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
													"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
													"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
													'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
													'CURRENCY_ID' => $arParams['CURRENCY_ID'],
													'HIDE_NOT_AVAILABLE' => $arParams['HIDE_NOT_AVAILABLE'],
													'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
													'VIEW_MODE' => (isset($arParams['TOP_VIEW_MODE']) ? $arParams['TOP_VIEW_MODE'] : ''),
													'ROTATE_TIMER' => (isset($arParams['TOP_ROTATE_TIMER']) ? $arParams['TOP_ROTATE_TIMER'] : ''),
													'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
													'LABEL_PROP' => $arParams['LABEL_PROP'],
													'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
													'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

													'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
													'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
													'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
													'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
													'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
													'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
													'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
													'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
													'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
													'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
													'ADD_TO_BASKET_ACTION' => $basketAction,
													'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
													'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
												),
												false, array("HIDE_ICONS"=>"Y")
											);?>
										</ul>
									</div>
								<?}?>
							</li>
						</ul>
					</div>
				<?endforeach;?>
			<?endif;?>
		</div>
	<?else:?>
        <!--With that good buy block START-->
        <?//$APPLICATION->IncludeFile(SITE_DIR."include/parts/with_good_buy.php", Array(), Array("MODE"=>"php"));?>
        <?//$APPLICATION->IncludeFile(SITE_DIR."include/parts/with_good_buy_recommends.php", Array(), Array("MODE"=>"php"));?>
        <?$APPLICATION->IncludeFile(SITE_DIR."include/parts/with_good_buy_v2.php", Array("CURENT_ID"=>$arElement["ID"]), Array("MODE"=>"php"));?>

        <!--With that good buy block END-->
	<div class="bottom_slider specials tab_slider_wrapp">
		<div class="top_blocks">

			<ul class="tabs">
				<?$i=1;
				foreach($arTab as $code=>$title):?>
					<li data-code="<?=$code?>" <?=($code=="RECOMENDATION" ? "style='display:none;'" : "" );?> <?=($i==1 ? "class='cur'" : "")?>><span><?=$title;?></span></li>
					<?$i++;?>
				<?endforeach;?>
				<li class="stretch"></li>
			</ul>
			<ul class="slider_navigation top custom_flex border">
				<?$i=1;
				foreach($arTab as $code=>$title):?>
					<li class="tabs_slider_navigation <?=$code?>_nav <?=($i==1 ? "cur" : "")?>" data-code="<?=$code?>"></li>
					<?$i++;?>
				<?endforeach;?>
			</ul>
		</div>
		<?$disply_elements=($arParams["DISPLAY_ELEMENT_SLIDER"] ? $arParams["DISPLAY_ELEMENT_SLIDER"] : 10);?>
		<ul class="tabs_content">
			<?foreach($arTab as $code=>$title){?>
				<li class="tab <?=$code?>_wrapp" data-code="<?=$code?>">

					<?if($code=="RECOMENDATION"){?>
						<?
						$GLOBALS["CATALOG_CURRENT_ELEMENT_ID"] = $ElementID;
						?>
						<?$APPLICATION->IncludeComponent("bitrix:catalog.bigdata.products", CNext::checkVersionExt(), array(
							"USE_REGION" => ($arRegion ? "Y" : "N"),
							"STORES" => $arParams['STORES'],
							"LINE_ELEMENT_COUNT" => 5,
							"TEMPLATE_THEME" => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
							"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
							"BASKET_URL" => $arParams["BASKET_URL"],
							"ACTION_VARIABLE" => (!empty($arParams["ACTION_VARIABLE"]) ? $arParams["ACTION_VARIABLE"] : "action")."_cbdp",
							"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
							"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
							"SHOW_MEASURE_WITH_RATIO" => $arParams["SHOW_MEASURE_WITH_RATIO"],
							"ADD_PROPERTIES_TO_BASKET" => "N",
							"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
							"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
							"SHOW_OLD_PRICE" => $arParams['SHOW_OLD_PRICE'],
							"SHOW_DISCOUNT_PERCENT" => $arParams['SHOW_DISCOUNT_PERCENT'],
							"PRICE_CODE" => $arParams['PRICE_CODE'],
							"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
							"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
							"PRODUCT_SUBSCRIPTION" => $arParams['PRODUCT_SUBSCRIPTION'],
							"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
							"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
							"SHOW_NAME" => "Y",
							"SHOW_IMAGE" => "Y",
							"SHOW_MEASURE" => $arParams["SHOW_MEASURE"],
							"SHOW_RATING" => $arParams["SHOW_RATING"],
							"MESS_BTN_BUY" => $arParams['MESS_BTN_BUY'],
							"MESS_BTN_DETAIL" => $arParams['MESS_BTN_DETAIL'],
							"MESS_BTN_SUBSCRIBE" => $arParams['MESS_BTN_SUBSCRIBE'],
							"MESS_NOT_AVAILABLE" => $arParams['MESS_NOT_AVAILABLE'],
							"PAGE_ELEMENT_COUNT" => $disply_elements,
							"SHOW_FROM_SECTION" => "N",
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"SALE_STIKER" => $arParams["SALE_STIKER"],
							"STIKERS_PROP" => $arParams["STIKERS_PROP"],
							"DEPTH" => "2",
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
							"SHOW_PRODUCTS_".$arParams["IBLOCK_ID"] => "Y",
							"ADDITIONAL_PICT_PROP_".$arParams["IBLOCK_ID"] => $arParams['ADD_PICT_PROP'],
							"LABEL_PROP_".$arParams["IBLOCK_ID"] => "-",
							"HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
							'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
							"CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
							"CURRENCY_ID" => $arParams["CURRENCY_ID"],
							"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
							"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
							"SECTION_ELEMENT_ID" => $arResult["VARIABLES"]["SECTION_ID"],
							"SECTION_ELEMENT_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
							"ID" => $ElementID,
							"PROPERTY_CODE_".$arParams["IBLOCK_ID"] => $arParams["LIST_PROPERTY_CODE"],
							"CART_PROPERTIES_".$arParams["IBLOCK_ID"] => $arParams["PRODUCT_PROPERTIES"],
							"RCM_TYPE" => (isset($arParams['BIG_DATA_RCM_TYPE']) ? $arParams['BIG_DATA_RCM_TYPE'] : ''),
							"DISPLAY_WISH_BUTTONS" => $arParams["DISPLAY_WISH_BUTTONS"],
							"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
							"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
							),
							false,
							array("HIDE_ICONS" => "Y", "ACTIVE_COMPONENT" => "Y")
						);
						?>
					<?}else{?>
						<div class="flexslider loading_state shadow border custom_flex top_right" data-plugin-options='{"animation": "slide", "animationSpeed": 600, "directionNav": true, "controlNav" :false, "animationLoop": true, "slideshow": false, "controlsContainer": ".tabs_slider_navigation.<?=$code?>_nav", "counts": [4,3,3,2,1]}'>
						<ul class="tabs_slider <?=$code?>_slides slides">
							<?$GLOBALS['arrFilter'.$code] = array( "ID" => $arAllValues[$code] );?>
							<?$APPLICATION->IncludeComponent(
								"bitrix:catalog.top",
								"main",
								array(
									"USE_REGION" => ($arRegion ? "Y" : "N"),
									"STORES" => $arParams['STORES'],
									"TITLE_BLOCK" => $arParams["SECTION_TOP_BLOCK_TITLE"],
									"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
									"IBLOCK_ID" => $arParams["IBLOCK_ID"],
									"SALE_STIKER" => $arParams["SALE_STIKER"],
									"STIKERS_PROP" => $arParams["STIKERS_PROP"],
									"SHOW_RATING" => $arParams["SHOW_RATING"],
									"FILTER_NAME" => 'arrFilter'.$code,
									"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
									"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
									"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
									"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
									"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
									"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
									"BASKET_URL" => $arParams["BASKET_URL"],
									"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
									"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
									"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
									"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
									"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
									"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
									"DISPLAY_WISH_BUTTONS" => $arParams["DISPLAY_WISH_BUTTONS"],
									"ELEMENT_COUNT" => $disply_elements,
									"SHOW_MEASURE_WITH_RATIO" => $arParams["SHOW_MEASURE_WITH_RATIO"],
									"SHOW_MEASURE" => $arParams["SHOW_MEASURE"],
									"LINE_ELEMENT_COUNT" => $arParams["TOP_LINE_ELEMENT_COUNT"],
									"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
									"PRICE_CODE" => $arParams['PRICE_CODE'],
									"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
									"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
									"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
									"PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"],
									"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
									"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
									"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
									"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
									"CACHE_TYPE" => $arParams["CACHE_TYPE"],
									"CACHE_TIME" => $arParams["CACHE_TIME"],
									"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
									"CACHE_FILTER" => $arParams["CACHE_FILTER"],
									"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
									"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
									"OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],
									"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
									"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
									"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
									"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
									"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
									'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
									'CURRENCY_ID' => $arParams['CURRENCY_ID'],
									'HIDE_NOT_AVAILABLE' => $arParams['HIDE_NOT_AVAILABLE'],
									'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
									'VIEW_MODE' => (isset($arParams['TOP_VIEW_MODE']) ? $arParams['TOP_VIEW_MODE'] : ''),
									'ROTATE_TIMER' => (isset($arParams['TOP_ROTATE_TIMER']) ? $arParams['TOP_ROTATE_TIMER'] : ''),
									'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
									'LABEL_PROP' => $arParams['LABEL_PROP'],
									'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
									'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

									'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
									'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
									'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
									'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
									'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
									'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
									'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
									'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
									'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
									'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
									'ADD_TO_BASKET_ACTION' => $basketAction,
									'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
									'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
								),
								false, array("HIDE_ICONS"=>"Y")
							);?>
						</ul>
						</div>
					<?}?>
				</li>
			<?}?>
		</ul>
	</div>
	<?endif;?>
	<?if($isWideBlock == "Y"):?>
			</div>
		</div>
	<?endif;?>
<?}?>
<?/*fix title after ajax form start*/
endif;
$arAdditionalData = $arNavParams = array();

$postfix = '';
global $arSite;
if(\Bitrix\Main\Config\Option::get("aspro.next", "HIDE_SITE_NAME_TITLE", "N")=="N")
	$postfix = ' - '.$arSite['SITE_NAME'];

$arAdditionalData['TITLE'] = htmlspecialcharsback($APPLICATION->GetTitle());
$arAdditionalData['WINDOW_TITLE'] = htmlspecialcharsback($APPLICATION->GetTitle('title').$postfix);

// dirty hack: try to get breadcrumb call params
for ($i = 0, $cnt = count($APPLICATION->buffer_content_type); $i < $cnt; $i++){
	if ($APPLICATION->buffer_content_type[$i]['F'][1] == 'GetNavChain'){
		$arNavParams = $APPLICATION->buffer_content_type[$i]['P'];
	}
}
if ($arNavParams){
	$arAdditionalData['NAV_CHAIN'] = $APPLICATION->GetNavChain($arNavParams[0], $arNavParams[1], $arNavParams[2], $arNavParams[3], $arNavParams[4]);
}
?>
<script type="text/javascript">
	if(!$('.js_seo_title').length)
		$('<span class="js_seo_title" style="display:none;"></span>').appendTo($('body'));
	BX.addCustomEvent(window, "onAjaxSuccess", function(e){
		var arAjaxPageData = <?=CUtil::PhpToJSObject($arAdditionalData, true, true, true);?>;

		//set title from offers
		if(typeof ItemObj == 'object' && Object.keys(ItemObj).length)
		{
			if('TITLE' in ItemObj && ItemObj.TITLE)
			{
				arAjaxPageData.TITLE = ItemObj.TITLE;
				arAjaxPageData.WINDOW_TITLE = ItemObj.WINDOW_TITLE;
			}
		}

		if (arAjaxPageData.TITLE)
			$('h1').html(arAjaxPageData.TITLE);
		if (arAjaxPageData.WINDOW_TITLE || arAjaxPageData.TITLE)
		{
			$('.js_seo_title').html(arAjaxPageData.WINDOW_TITLE || arAjaxPageData.TITLE); //seo fix for spec symbol
			BX.ajax.UpdateWindowTitle($('.js_seo_title').html());
		}

		if (arAjaxPageData.NAV_CHAIN)
			BX.ajax.UpdatePageNavChain(arAjaxPageData.NAV_CHAIN);
		$('.catalog_detail input[data-sid="PRODUCT_NAME"]').attr('value', $('h1').html());
	});
</script>
<?/*fix title after ajax form end*/?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/js/jquery.history.js');?>
<!--Block you may like it START-->
<section style="width: 100%; margin: -20px 0 0; overflow: hidden; padding-left: 2px; padding-top: 1px;">
    <?php
    echo '<h5 style="margin: 0 0 10px;font-size: 16px;color: #222;"><span style="border-bottom: 2px solid #ddd;">Вам также может понравиться</span></h5>';
    $APPLICATION->IncludeComponent("bitrix:catalog.bigdata.products", "fm_recommendations", Array(
	"IBLOCK_TYPE" => "1c_catalog",	// Тип инфоблока
		"IBLOCK_ID" => "39",	// Инфоблок
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CACHE_TIME" => "36000",	// Время кеширования (сек.)
		"COMPONENT_TEMPLATE" => "main_new",
		"RCM_TYPE" => "any",	// Тип рекомендации
		"ELEMENT_ID" => "",
		"ELEMENT_CODE" => "",
		"ELEMENTS_LIMIT" => "10",
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"ID" => "",	// Параметр ID продукта (для товарных рекомендаций)
		"SHOW_FROM_SECTION" => "Y",	// Показывать товары из раздела
		"SECTION_ID" => "",	// ID раздела
		"SECTION_CODE" => "",	// Код раздела
		"SECTION_ELEMENT_ID" => $arElement['ID'],	// ID элемента, для которого будет выбран раздел
		"SECTION_ELEMENT_CODE" => "",	// Символьный код элемента, для которого будет выбран раздел
		"DEPTH" => "2",	// Максимальная отображаемая глубина разделов
		"HIDE_NOT_AVAILABLE" => "N",	// Не отображать товары, которых нет на складах
		"SHOW_DISCOUNT_PERCENT" => "Y",	// Показывать процент скидки
		"PRODUCT_SUBSCRIPTION" => "N",	// Разрешить оповещения для отсутствующих товаров
		"SHOW_NAME" => "Y",	// Показывать название
		"SHOW_IMAGE" => "Y",	// Показывать изображение
		"MESS_BTN_BUY" => "Купить",	// Текст кнопки "Купить"
		"MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"
		"MESS_BTN_SUBSCRIBE" => "Подписаться",	// Текст кнопки "Уведомить о поступлении"
		"PAGE_ELEMENT_COUNT" => "30",	// Количество элементов на странице
		"LINE_ELEMENT_COUNT" => "3",	// Количество элементов, выводимых в одной строке
		"TEMPLATE_THEME" => "blue",	// Цветовая тема
		"DETAIL_URL" => "",	// URL, ведущий на страницу с содержимым элемента раздела
		"COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
		"COMPOSITE_FRAME_TYPE" => "AUTO",	// Содержимое компонента
		"SHOW_OLD_PRICE" => "Y",	// Показывать старую цену
		"PRICE_CODE" => array(	// Тип цены
			0 => "BASE",
			1 => "Интернет-продажи(Трифонов)",
		),
		"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества
		"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену
		"CONVERT_CURRENCY" => "Y",	// Показывать цены в одной валюте
		"BASKET_URL" => "/personal/basket.php",	// URL, ведущий на страницу с корзиной покупателя
		"ACTION_VARIABLE" => "action_cbdp",	// Название переменной, в которой передается действие
		"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
		"ADD_PROPERTIES_TO_BASKET" => "Y",	// Добавлять в корзину свойства товаров и предложений
		"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
		"PARTIAL_PRODUCT_PROPERTIES" => "N",	// Разрешить частично заполненные свойства
		"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара
		"SHOW_PRODUCTS_17" => "Y",	// Показывать товары каталога
		"PROPERTY_CODE_17" => array(	// Свойства для отображения
			0 => "MINIMUM_PRICE",
			1 => "MAXIMUM_PRICE",
			2 => "CML2_ATTRIBUTES",
			3 => "MINIMUM_PRICE,MAXIMUM_PRICE,,",
			4 => "",
		),
		"CART_PROPERTIES_17" => array(	// Свойства для добавления в корзину
			0 => "",
			1 => ",",
			2 => "",
		),
		"ADDITIONAL_PICT_PROP_17" => "MORE_PHOTO",	// Дополнительная картинка
		"LABEL_PROP_17" => "-",	// Свойство меток товара
		"PROPERTY_CODE_20" => array(	// Свойства для отображения
			0 => "CML2_LINK",
			1 => "",
		),
		"CART_PROPERTIES_20" => array(	// Свойства для добавления в корзину
			0 => "SIZES",
			1 => "COLOR_REF",
			2 => "",
		),
		"ADDITIONAL_PICT_PROP_20" => "MORE_PHOTO",	// Дополнительная картинка
		"OFFER_TREE_PROPS_20" => "",	// Свойства для отбора предложений
		"SHOW_PRODUCTS_39" => "Y",	// Показывать товары каталога
		"PROPERTY_CODE_39" => array(	// Свойства для отображения
			0 => "STARAYA_TSENA_",
			1 => "CML2_ATTRIBUTES",
			2 => ",",
			3 => "",
		),
		"CART_PROPERTIES_39" => array(	// Свойства для добавления в корзину
			0 => "",
			1 => ",",
			2 => "",
		),
		"ADDITIONAL_PICT_PROP_39" => "MORE_PHOTO",	// Дополнительная картинка
		"LABEL_PROP_39" => "-",	// Свойство меток товара
		"PROPERTY_CODE_40" => array(	// Свойства для отображения
			0 => "STARAYA_TSENA_",
			1 => "CML2_ATTRIBUTES",
			2 => "",
		),
		"CART_PROPERTIES_40" => array(	// Свойства для добавления в корзину
			0 => "",
			1 => "",
		),
		"ADDITIONAL_PICT_PROP_40" => "MORE_PHOTO",	// Дополнительная картинка
		"OFFER_TREE_PROPS_40" => "",	// Свойства для отбора предложений
		"CURRENCY_ID" => "RUB",	// Валюта, в которую будут сконвертированы цены
	),
	false
);
    ?>
</section>
<!--Block you may like it END-->
<!--Stores map START-->
<div style="clear: both;"></div>
<div class="contacts-v5 detail-contacts">
    <?$APPLICATION->IncludeComponent(
	"bitrix:news", 
	"detail_map", 
	array(
		"IBLOCK_TYPE" => "aspro_next_content",
		"IBLOCK_ID" => "60",
		"NEWS_COUNT" => "20",
		"USE_SEARCH" => "N",
		"USE_RSS" => "Y",
		"USE_RATING" => "N",
		"USE_CATEGORIES" => "N",
		"USE_FILTER" => "Y",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"CHECK_DATES" => "Y",
		"SEF_MODE" => "Y",
		"SEF_FOLDER" => "/contacts/",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"SET_TITLE" => "Y",
		"SET_STATUS_404" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"USE_PERMISSIONS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"LIST_ACTIVE_DATE_FORMAT" => "j F Y",
		"LIST_FIELD_CODE" => array(
			0 => "NAME",
			1 => "PREVIEW_TEXT",
			2 => "PREVIEW_PICTURE",
			3 => "DATE_ACTIVE_FROM",
			4 => "",
		),
		"LIST_PROPERTY_CODE" => array(
			0 => "",
			1 => "PERIOD",
			2 => "REDIRECT",
			3 => "",
		),
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"DISPLAY_NAME" => "N",
		"META_KEYWORDS" => "-",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"DETAIL_ACTIVE_DATE_FORMAT" => "j F Y",
		"DETAIL_FIELD_CODE" => array(
			0 => "PREVIEW_TEXT",
			1 => "DETAIL_TEXT",
			2 => "DETAIL_PICTURE",
			3 => "DATE_ACTIVE_FROM",
			4 => "",
		),
		"DETAIL_PROPERTY_CODE" => array(
			0 => "",
			1 => "FORM_QUESTION",
			2 => "FORM_ORDER",
			3 => "PHOTOPOS",
			4 => "LINK_GOODS",
			5 => "LINK_SERVICES",
			6 => "LINK_STUDY",
			7 => "VIDEO",
			8 => "PHOTOS",
			9 => "DOCUMENTS",
			10 => "",
		),
		"DETAIL_DISPLAY_TOP_PAGER" => "N",
		"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
		"DETAIL_PAGER_TITLE" => "Страница",
		"DETAIL_PAGER_TEMPLATE" => "",
		"DETAIL_PAGER_SHOW_ALL" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"IMAGE_POSITION" => "left",
		"USE_SHARE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"USE_REVIEW" => "N",
		"ADD_ELEMENT_CHAIN" => "Y",
		"SHOW_DETAIL_LINK" => "Y",
		"S_ASK_QUESTION" => "",
		"S_ORDER_SERVISE" => "",
		"T_GALLERY" => "",
		"T_DOCS" => "",
		"T_GOODS" => "",
		"T_SERVICES" => "",
		"T_STUDY" => "",
		"COMPONENT_TEMPLATE" => "detail_map",
		"SET_LAST_MODIFIED" => "N",
		"T_VIDEO" => "",
		"DETAIL_SET_CANONICAL_URL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"NUM_NEWS" => "20",
		"NUM_DAYS" => "30",
		"YANDEX" => "N",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"SECTIONS_TYPE_VIEW" => "sections_1",
		"SECTION_TYPE_VIEW" => "section_1",
		"SECTION_ELEMENTS_TYPE_VIEW" => "list_elements_2",
		"ELEMENT_TYPE_VIEW" => "element_1",
		"S_ORDER_SERVICE" => "",
		"T_PROJECTS" => "",
		"T_REVIEWS" => "",
		"T_STAFF" => "",
		"IMAGE_CATALOG_POSITION" => "left",
		"SHOW_SECTION_PREVIEW_DESCRIPTION" => "Y",
		"SHOW_SECTION_DESCRIPTION" => "Y",
		"LINE_ELEMENT_COUNT" => "20",
		"LINE_ELEMENT_COUNT_LIST" => "3",
		"SHOW_CHILD_SECTIONS" => "N",
		"GALLERY_TYPE" => "small",
		"INCLUDE_SUBSECTIONS" => "Y",
		"FORM_ID_ORDER_SERVISE" => "",
		"T_NEXT_LINK" => "",
		"T_PREV_LINK" => "",
		"SHOW_NEXT_ELEMENT" => "N",
		"IMAGE_WIDE" => "N",
		"SHOW_FILTER_DATE" => "Y",
		"FILTER_NAME" => "arFilterNews",
		"FILTER_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"DETAIL_STRICT_SECTION_CHECK" => "N",
		"VIEW_TYPE" => "list",
		"SHOW_TABS" => "Y",
		"SHOW_ASK_QUESTION_BLOCK" => "Y",
		"STRICT_SECTION_CHECK" => "N",
		"SHOW_TOP_MAP" => "N",
		"SEF_URL_TEMPLATES" => array(
			"news" => "",
			"section" => "",
			"detail" => "stores/#ELEMENT_ID#/",
			"rss" => "rss/",
			"rss_section" => "#SECTION_ID#/rss/",
		)
	),
	false
);?>
</div>
<!--Stores map END-->
<!--Why buy block START-->
<?$APPLICATION->IncludeFile(SITE_DIR."include/parts/why_buy.php",
    Array('style'=>'whybuy-block--good'),
    Array("MODE"=>"php")
);?>
<!--Why buy block END-->


