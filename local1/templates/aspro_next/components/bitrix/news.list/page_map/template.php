<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?$this->setFrameMode(true);?>

	<?if($arResult['SECTIONS']):?>
				<div class="col-md-6">
					<div class="detail-map-city-list">
						<?foreach($arResult['SECTIONS'] as $si => $arSection):?>
							<?$bHasSection = (isset($arSection['SECTION']) && $arSection['SECTION'])?>
							<?if($bHasSection):?>
								<?// edit/add/delete buttons for edit mode
								$arSectionButtons = CIBlock::GetPanelButtons($arSection['SECTION']['IBLOCK_ID'], 0, $arSection['SECTION']['ID'], array('SESSID' => false, 'CATALOG' => true));
								$this->AddEditAction($arSection['SECTION']['ID'], $arSectionButtons['edit']['edit_section']['ACTION_URL'], CIBlock::GetArrayByID($arSection['SECTION']['IBLOCK_ID'], 'SECTION_EDIT'));
								$this->AddDeleteAction($arSection['SECTION']['ID'], $arSectionButtons['edit']['delete_section']['ACTION_URL'], CIBlock::GetArrayByID($arSection['SECTION']['IBLOCK_ID'], 'SECTION_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));?>
								<div class="detail-map-city-list__header" id="<?=$this->GetEditAreaId($arSection['SECTION']['ID'])?>">
									<h5><?=$arSection['SECTION']['NAME'];?></h5>
								</div>
							<?endif;?>
							<?foreach($arSection['ITEMS'] as $i => $arItem):?>
								<?
								// edit/add/delete buttons for edit mode
								$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
								$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
								// use detail link?
								$bDetailLink = $arParams['SHOW_DETAIL_LINK'] != 'N' && (!strlen($arItem['DETAIL_TEXT']) ? ($arParams['HIDE_LINK_WHEN_NO_DETAIL'] !== 'Y' && $arParams['HIDE_LINK_WHEN_NO_DETAIL'] != 1) : true);
								// preview picture
								$bImage = strlen($arItem['FIELDS']['PREVIEW_PICTURE']['SRC']);
								$imageSrc = ($bImage ? $arItem['FIELDS']['PREVIEW_PICTURE']['SRC'] : false);
								$imageDetailSrc = ($bImage ? $arItem['FIELDS']['DETAIL_PICTURE']['SRC'] : false);
								$address = ($arItem['PROPERTIES']['ADDRESS']['VALUE'] ? ", ".$arItem['PROPERTIES']['ADDRESS']['VALUE'] : "");
								?>

								<div class="detail-map-city-list__item" id="<?=$this->GetEditAreaId($arItem['ID'])?>">
									<div <?=(($arResult['ITEMS_HAS_IMG'] && !$imageSrc) ? 'colspan=2' : '');?>>
										<div class="title">
											<a  href="<?=$arItem["DETAIL_PAGE_URL"];?>" class="dark_link btn-inline rounded">
                                                <?=$arItem['NAME'];?><?=$address;?>
                                                <i class="fa fa-angle-right"></i>
                                            </a>
										</div>
										<?if($arItem['PROPERTIES']['METRO']['VALUE']):?>
											<?foreach($arItem['PROPERTIES']['METRO']['VALUE'] as $metro):?>
												<div class="metro">
													<i></i><?=$metro;?>
												</div>
											<?endforeach;?>
										<?endif;?>
										<?if($arItem['PROPERTIES']['SCHEDULE']['VALUE']):?>
											<div class="muted">
												<span class="icon-text schedule grey s25"><i class="fa fa-clock-o"></i> <span class="text"><?=$arItem['PROPERTIES']['SCHEDULE']['~VALUE']['TEXT'];?></span></span>
											</div>
										<?endif;?>
                                        <div class="phone">
                                            <?if($arItem['PROPERTIES']['PHONE']['VALUE'])
                                            {
                                                foreach($arItem['PROPERTIES']['PHONE']['VALUE'] as $phone):?>
                                                    <a href="tel:+<?=str_replace(array(' ', ',', '-', '(', ')'), '', $phone);?>" class="black"><?=$phone;?></a>
                                                <?endforeach;
                                            }?>
                                        </div>
									</div>
								</div>
							<?endforeach;?>
						<?endforeach;?>
					</div>
				</div>
	<?endif;?>