<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if ($arParam['SHOW_ACCOUNT_PAGE'] === 'N')
{
	LocalRedirect($arParams['SEF_FOLDER']);
}

use Bitrix\Main\Localization\Loc;
use Bitrix\Sale\OrderDiscountManager;

global $USER;

$APPLICATION->SetTitle(Loc::getMessage("SPS_TITLE_ACCOUNT"));
// $APPLICATION->AddChainItem(Loc::getMessage("SPS_CHAIN_MAIN"), $arResult['SEF_FOLDER']);
$APPLICATION->AddChainItem(Loc::getMessage("SPS_CHAIN_ACCOUNT"));

$DISCOUNTS = [5000=> 300, 20000=>3000, 60000 => 10000, 100000 => 15000];
$arPrice = 0;
$inc = 0;
$discount_result = 0;
$equality = [];
$arFilter = Array(
    "USER_ID" => $USER->GetID(),
    ">=DATE_INSERT" => date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), mktime(0, 0, 0, date("n"), 1, date("Y")))
);

$db_sales = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter);
while ($ar_sales = $db_sales->Fetch()) {
    $arPrice += intval($ar_sales['PRICE']);
}
foreach ($DISCOUNTS as $discount_key=>$discount_value) {
        $equality[] = $arPrice >= $discount_key;
}
for ($i = 0; $i < count($equality); $i++) {
    if (!$equality[$i]) {
        $inc = $i-1;
        break;
    }
}

$j = -1;
foreach ($DISCOUNTS as $key=>$value) {
    $j++;
    if ($inc === $j) {
        $discount_result = $value;
    }
}
?>
<div class="personal_wrapper">
	<div class="inner_border">
        <section class="discount-block">
            <h3 class="sale-personal-section-account-sub-header">
                Персональная накопительная скидка
            </h3>
            <div class="sale-personal-account-wallet-container">
                <div class="sale-personal-account-wallet-title">
                    Скидка на единицу товара к <?
                    echo date("d.m.Y");
                    ?>
                </div>
                <div class="sale-personal-account-wallet-list-container">
                    <div class="sale-personal-account-wallet-list">
                        <div class="sale-personal-account-wallet-list-item">
                            <p class="sale-personal-account-wallet-sum">
                                <?=CurrencyFormat($discount_result, 'RUB');?>
                            </p>
                            <div class="sale-personal-account-wallet-currency">
                                <div class="sale-personal-account-wallet-currency-item">RUB</div>
                                <div class="sale-personal-account-wallet-currency-item">Рубль</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
		<?if ($arParam['SHOW_ACCOUNT_COMPONENT'] !== 'N')
		{
			$APPLICATION->IncludeComponent(
				"bitrix:sale.personal.account",
				"",
				Array(
					"SET_TITLE" => "N"
				),
				$component
			);
		}
		?>
		<h3 class="sale-personal-section-account-sub-header">
			<?=Loc::getMessage("SPS_BUY_MONEY")?>
		</h3>

		<?
		if ($arParam['SHOW_ACCOUNT_PAY_COMPONENT'] !== 'N')
		{
			$APPLICATION->IncludeComponent(
				"bitrix:sale.account.pay",
				"",
				Array(
					"COMPONENT_TEMPLATE" => ".default",
					"REFRESHED_COMPONENT_MODE" => "Y",
					"ELIMINATED_PAY_SYSTEMS" => $arParams['ACCOUNT_PAYMENT_ELIMINATED_PAY_SYSTEMS'],
					"PATH_TO_BASKET" => $arParams['PATH_TO_BASKET'],
					"PATH_TO_PAYMENT" => $arParams['PATH_TO_PAYMENT'],
					"PERSON_TYPE" => $arParams['ACCOUNT_PAYMENT_PERSON_TYPE'],
					"REDIRECT_TO_CURRENT_PAGE" => "N",
					"SELL_AMOUNT" => $arParams['ACCOUNT_PAYMENT_SELL_TOTAL'],
					"SELL_CURRENCY" => $arParams['ACCOUNT_PAYMENT_SELL_CURRENCY'],
					"SELL_SHOW_FIXED_VALUES" => $arParams['ACCOUNT_PAYMENT_SELL_SHOW_FIXED_VALUES'],
					"SELL_SHOW_RESULT_SUM" =>  $arParams['ACCOUNT_PAYMENT_SELL_SHOW_RESULT_SUM'],
					"SELL_TOTAL" => $arParams['ACCOUNT_PAYMENT_SELL_TOTAL'],
					"SELL_USER_INPUT" => $arParams['ACCOUNT_PAYMENT_SELL_USER_INPUT'],
					"SELL_VALUES_FROM_VAR" => "N",
					"SELL_VAR_PRICE_VALUE" => "",
					"SET_TITLE" => "N",
				),
				$component
			);
		}
		?>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.btn').addClass('button')
		})
	</script>
</div>
