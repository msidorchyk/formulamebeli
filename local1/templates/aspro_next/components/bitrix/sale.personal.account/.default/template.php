<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
function cl_print_r ($var, $label = '') {
	$str = json_encode(print_r ($var, true));
	echo "<script>console.group('".$label."');console.log('".$str."');console.groupEnd();</script>";
}
global $USER;
$userBy = "id";
$userOrder = "desc";

$out = CUser::GetList($userBy, $userOrder, array('ID' => $USER->GetID()));
if ($arUser = $out->Fetch()):
	if ($userEmail) $userEmail = $arUser["EMAIL"];
	if ($userPhone) $userPhone = $arUser["PERSONAL_PHONE"];
	$userID = $arUser["ID"];
endif;

$postData = '<operation><customer><email>'.$userEmail.'</email><mobilePhone>'.$userPhone.'</mobilePhone><ids><clientWebsiteId>'.$userID.'</clientWebsiteId></ids></customer></operation>';

$ch = curl_init("https://api.mindbox.ru/v3/operations/sync?endpointId=OfflineFm&operation=GetCustomerInfo");
curl_setopt_array($ch, array(
	CURLOPT_POST => TRUE,
	CURLOPT_RETURNTRANSFER => TRUE,
	CURLOPT_HTTPHEADER => array(
		'Authorization: Mindbox secretKey="1EqLiyAsJGFkp426qzRh"',
		'Accept: application/xml',
		'X-Customer-IP: '.$_SERVER['REMOTE_ADDR'],
		'Content-Type: application/xml; charset=utf-8'
	),
	CURLOPT_POSTFIELDS => $postData
));
$response = curl_exec($ch);
$parsedResponse = (array) simplexml_load_string($response);
cl_print_r($parsedResponse);
?>
<div class="sale-personal-account-wallet-container">
	<div class="sale-personal-account-wallet-title">
		<?=Bitrix\Main\Localization\Loc::getMessage('SPA_BILL_AT')?>
		<?=$arResult["DATE"];?>
	</div>
	<div class="sale-personal-account-wallet-list-container">
		<div class="sale-personal-account-wallet-list">
			<?
			foreach($arResult["ACCOUNT_LIST"] as $accountValue)
			{
				?>
				<div class="sale-personal-account-wallet-list-item">
					<div class="sale-personal-account-wallet-sum"><?=$accountValue['SUM']?></div>
					<div class="sale-personal-account-wallet-currency">
						<div class="sale-personal-account-wallet-currency-item"><?=$accountValue['CURRENCY']?></div>
						<div class="sale-personal-account-wallet-currency-item"><?=$accountValue["CURRENCY_FULL_NAME"]?></div>
					</div>
				</div>
				<?
			}
			?>
		</div>
	</div>
</div>
	<? $out = CUser::GetList($userBy, $userOrder, array('ID' => $USER->GetID())); ?>
	<? if ($arUser = $out->Fetch()): ?>
		<? if ($userPhone) $userPhone = $arUser["PERSONAL_PHONE"]; ?>
		<div class="sale-personal-account-wallet-container">
			<div class="sale-personal-account-wallet-title">
				СОСТОЯНИЕ БОНУСНОГО СЧЕТА НА
				<?=$arResult["DATE"];?>
			</div>
			<div class="sale-personal-account-wallet-list-container">
				<div class="sale-personal-account-wallet-list">
					<div class="sale-personal-account-wallet-list-item">
						<div class="sale-personal-account-wallet-sum"><?=$parsedResponse['balances']->balance->available?> Б</div>
						<div class="sale-personal-account-wallet-currency">
							<div class="sale-personal-account-wallet-currency-item">Б</div>
							<div class="sale-personal-account-wallet-currency-item">Баллы</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<? else: ?>
		<div class="sale-personal-account-wallet-container">
			<div class="sale-personal-account-wallet-title">
				Чтобы получать бонусные баллы, вы должны указать свой номер телефона в разделе <a href="/personal/private/">личных данных</a>
			</div>
		</div>
<? endif; ?>