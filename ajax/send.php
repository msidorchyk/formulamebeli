<?php
/**
 * Created by Vladimir Krasnoselskikh.
 * Date: 02.08.17
 */

/////////////////////////////////////////////////////////

$emailTo = 'shop@formulamebeli.com';

/////////////////////////////////////////////////////////

// Использую сессию, чтобы предотвратить флудинг:

session_name('quickFeedback');
session_start();

// Если последняя форма была отправлена меньше 10 секунд назад,
// или пользователь уже послал 10 сообщений за последний час

//if( $_SESSION['lastSubmit'] && ( time() - $_SESSION['lastSubmit'] < 10 || $_SESSION['submitsLastHour'][date('d-m-Y-H')] > 10 )){
//    die('<div class="error"> Пожалуста, подождите несколько минут, прежде чем отправить сообщение снова.</div>');
//}

$_SESSION['lastSubmit'] = time();
$_SESSION['submitsLastHour'][date('d-m-Y-H')]++;

//Проверка наличия ТЕКСТА сообщения
if(trim($_POST['name']) != '') {
    $form_name=trim($_POST['name']);
} else $form_name = 'Не указано';

if(trim($_POST['phone']) != '') {
    $form_phone=trim($_POST['phone']);
} else $form_phone = 'Не указан';

if(trim($_POST['email']) != '') {
    $form_id=trim($_POST['email']);
} else $form_id = 'Не указана';

if(trim($_POST['ref']) != '') {
    $form_ref=trim($_POST['ref']);
} else $form_ref = 'Не указана';


//Если ошибок нет, отправить email
if(!isset($hasError)) {        
    $body = "Страница: $form_ref\r\nИмя: $form_name\r\nТелефон: $form_phone \r\nПочта: $form_id";
    $subject = "$form_ref";
    $headers = "From: shop@formulamebeli.com\r\nReply-To: shop@formulamebeli.com\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $result = @mail($emailTo, $subject, $body, $headers);
    echo "Ваша заявка принята!";
} else {
    header("HTTP/1.0 503 Service Unavailable");
    echo "Что то пошло не так. Попробуйте заполнить заявку позже";
    print_r($_POST);
}