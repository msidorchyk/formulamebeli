<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$cities = CIBlockElement::GetList(['SORT'=>'ASC'], ['IBLOCK_ID'=>2]);
$cities_array = [];

while($cities_object = $cities->GetNextElement()){
    $fields = $cities_object->GetFields();
    $cities_array[] = $fields['NAME'];
}
sort($cities_array);
echo json_encode(['cities'=>$cities_array]);

