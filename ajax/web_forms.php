<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("form");
use Bitrix\Main\Mail\Event;

$request = $_POST;

function getFiles($files) {
    $count = 4;
    $array = [];
    for ($i = 1; $i < $count; $i++) {
        if ($files['file_'.$i]) {
            $file = $files['file_'.$i];
            $file["old_file"] = "";
            $file["del"] = "N";
            $file["MODULE_ID"] = "";
            $file_id = CFile::SaveFile($file, '/sended-files/');
            array_push($array, CFile::GetPath($file_id));
        }
    }
    return $array;
}

function sendForm ($form_id ,$fields, $callback_message, $files, $hasFiles, $sender_array)
{
    $RESULT_ID = (new CFormResult)->Add($form_id, $fields, 'N');
        if ($hasFiles) {
            if ($RESULT_ID) {
                //TODO fix this shit
                $image_urls = getFiles($files);

                Event::send([
                    "EVENT_NAME" => "FORM_FILLING_WRITE_TO_US",
                    "LID" => "s1",
                    "C_FIELDS" => $sender_array,
                    "FILE" => $image_urls
                ]);
                echo json_encode(['callback' => $callback_message]);
            } else {
                global $strError;
                echo json_encode(['callback' => $strError]);
            }
        } else {
            if ($RESULT_ID) {
                (new CFormResult)->Mail($RESULT_ID);
                echo json_encode(['callback'=> $callback_message]);
            } else {
                global $strError;
                echo json_encode(['callback'=> $strError]);
            }
        }
}
switch ($request['type']) {
    case 'installment':
        $price = $request['good-price'];
        preg_match_all('!\d+!', $price, $matches);
        $price_formatted = intval(implode($matches[0]));
        $counted_price = ($price_formatted * $request['good-count']) / 1000;
        $thousands = (intval(explode('.', $counted_price)[0]));
        $hundreds = (intval(explode('.', number_format($counted_price, 3, '.', ''))[1]));
        $price_string = $thousands.' '.$hundreds.' руб.';

        sendForm(
            9,
            ['form_text_37' => $request['name'],
            'form_text_67'=>$request['birthday'],
            'form_text_39'=>$request['phone'],
            'form_text_40'=>$request['good-name'],
            'form_text_41'=>$request['good-count'],
            'form_text_42'=>$request['good-price'],
            'form_text_43'=>$price_string,
            'form_text_68'=>$request['passport_series'],
            'form_text_69'=>$request['passport_number'],
            'form_text_70'=>$request['passport_receive'],
            'form_text_71'=>$request['passport_date'],
            'form_text_72'=>$request['city']],
            'Спасибо! Ваша заявка будет одобрена в течении 50 минут, после чего наш менеджер свяжется с Вами.',
            false,
            false,
            false
            );

        break;
    case 'write-us':
        sendForm(
            10,
            [
                'form_text_44'=>$request['city'],
                'form_text_45'=>$request['theme'],
                'form_text_46'=>$request['name'],
                'form_text_47'=>$request['shop'],
                'form_text_48'=>$request['order-number'],
                'form_text_49'=>$request['phone'],
                'form_text_50'=>$request['email'],
                'form_text_51'=>$request['comment'],
            ],
            'Спасибо! Ваша заявка будет одобрена в течении 50 минут, после чего наш менеджер свяжется с Вами.',
            $_FILES,
            true,
            [
                'CITY'=>$request['city'],
                'MESSAGE_THEME'=>$request['theme'],
                'NAME'=>$request['name'],
                'SHOP'=>$request['shop'],
                'ORDER_NUMBER'=>$request['order-number'],
                'PHONE'=>$request['phone'],
                'EMAIL'=>$request['email'],
                'COMMENT'=>$request['comment']
            ]
        );
        break;
    case 'installment_cart':
        $goods = json_decode($request['good_data']);
        $goods_html = '';

        foreach ($goods as $good) {
            $goods_html .= '<p>Наименование: '.$good->name.', <span> Количество: '.$good->count.'</span><p>';
        }
        // Clear Basket
        $basket_result = CSaleBasket::GetList(array(), array(
            'FUSER_ID' => CSaleBasket::GetBasketUserID(),
            'LID' => SITE_ID,
            'ORDER_ID' => 'null',
            'DELAY' => 'N',
            'CAN_BUY' => 'Y'));
        while ($basket_row = $basket_result->fetch()) {
            (new CSaleBasket)->Delete($basket_row['ID']);
        }
        // Sending a web-form form
        sendForm(
            13,
            ['form_text_58' => $request['name'],
             'form_text_60'=> $request['phone'],
             'form_text_73'=> $request['birthday'],
             'form_text_74'=> $request['passport_series'],
             'form_text_75'=> $request['passport_number'],
             'form_text_76'=> $request['passport_receive'],
             'form_text_77'=> $request['passport_date'],
             'form_text_78'=> $request['city'],
             'form_text_61'=> $goods_html,
             'form_text_62'=> $request['total_price']],
            'Спасибо! Ваша заявка будет одобрена в течении 50 минут, после чего наш менеджер свяжется с Вами.',
            false,
            false,
            false
        );
        break;
}