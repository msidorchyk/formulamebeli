<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
header("Content-type: text/html; charset=UTF-8");

$ajaxData = json_decode(urldecode($_GET['data']));

$arrFilterPropName = array();
$arrFilterPropValue = array();

array_push($arrFilterPropName, 'ACTIVE');
array_push($arrFilterPropValue, 'Y');

foreach ($ajaxData as $i => $value) {
    if($ajaxData[$i]->question == 'for_who') {
		$forWho = ($ajaxData[$i]->answer == 'children') ? '2131' : '2132';
	}
	if($ajaxData[$i]->question == 'reason') {
		if($ajaxData[$i]->answer == 'alergy') {
			array_push($arrFilterPropName, 'PROPERTY_GIPOALLERGENNYY_VALUE');
			array_push($arrFilterPropValue, array('Y'));
		}
	}
	if($ajaxData[$i]->question == 'reason') {
		if($ajaxData[$i]->answer == 'pain') {
			array_push($arrFilterPropName, 'PROPERTY_ZHESTKOST');
			array_push($arrFilterPropValue, '108426');
		}
	}
	if($ajaxData[$i]->question == 'age') {
		if($ajaxData[$i]->answer == 'young') {
			array_push($arrFilterPropName, 'PROPERTY_VOZRAST');
			array_push($arrFilterPropValue, '108419');
		}
		if($ajaxData[$i]->answer == 'middle_grow') {
			array_push($arrFilterPropName, 'PROPERTY_VOZRAST');
			array_push($arrFilterPropValue, '108420');
		}
		if($ajaxData[$i]->answer == 'old') {
			array_push($arrFilterPropName, 'PROPERTY_VOZRAST');
			array_push($arrFilterPropValue, '108421');
		}
	}
	if($ajaxData[$i]->question == 'age_children' || $ajaxData[$i]->question == 'count') {
		$mattressAssignment = $ajaxData[$i]->answer;
	}
	if($ajaxData[$i]->question == 'weight') {
		if($ajaxData[$i]->answer == 'less_than_70') {
			array_push($arrFilterPropName, 'PROPERTY_MAX_VES_NA_MESTO');
			array_push($arrFilterPropValue, '108422');
		}
		if($ajaxData[$i]->answer == 'from_70_to_100') {
			array_push($arrFilterPropName, 'PROPERTY_MAX_VES_NA_MESTO');
			array_push($arrFilterPropValue, '108423');
		}
		if($ajaxData[$i]->answer == 'from_100_to_140') {
			array_push($arrFilterPropName, 'PROPERTY_MAX_VES_NA_MESTO');
			array_push($arrFilterPropValue, '108424');
		}
		if($ajaxData[$i]->answer == 'more_than_140') {
			array_push($arrFilterPropName, 'PROPERTY_MAX_VES_NA_MESTO');
			array_push($arrFilterPropValue, '108425');
		}
	}
	if($ajaxData[$i]->question == 'weight') {
		if($ajaxData[$i]->answer == '70') {
			array_push($arrFilterPropName, 'PROPERTY_MAX_VES_NA_MESTO');
			array_push($arrFilterPropValue, '108422');
		}
		if($ajaxData[$i]->answer == '100') {
			array_push($arrFilterPropName, 'PROPERTY_MAX_VES_NA_MESTO');
			array_push($arrFilterPropValue, '108423');
		}
		if($ajaxData[$i]->answer == '140') {
			array_push($arrFilterPropName, 'PROPERTY_MAX_VES_NA_MESTO');
			array_push($arrFilterPropValue, '108424');
		}
		if($ajaxData[$i]->answer == '200') {
			array_push($arrFilterPropName, 'PROPERTY_MAX_VES_NA_MESTO');
			array_push($arrFilterPropValue, '108425');
		}
	}
	if($ajaxData[$i]->question == 'weight_second') {
		if($ajaxData[$i-1]->answer == 'none' || $ajaxData[$i]->answer > $ajaxData[$i-1]->answer) {
			if($ajaxData[$i]->answer == '70') {
				array_push($arrFilterPropName, 'PROPERTY_MAX_VES_NA_MESTO');
				array_push($arrFilterPropValue, '108422');
			}
			if($ajaxData[$i]->answer == '100') {
				array_push($arrFilterPropName, 'PROPERTY_MAX_VES_NA_MESTO');
				array_push($arrFilterPropValue, '108423');
			}
			if($ajaxData[$i]->answer == '140') {
				array_push($arrFilterPropName, 'PROPERTY_MAX_VES_NA_MESTO');
				array_push($arrFilterPropValue, '108424');
			}
			if($ajaxData[$i]->answer == '200') {
				array_push($arrFilterPropName, 'PROPERTY_MAX_VES_NA_MESTO');
				array_push($arrFilterPropValue, '108425');
			}
		}
	}
	if($ajaxData[$i]->question == 'rigidity') {
		if($ajaxData[$i]->answer == 'middle') {
			array_push($arrFilterPropName, 'PROPERTY_ZHESTKOST');
			array_push($arrFilterPropValue, '108426');
		}
		if($ajaxData[$i]->answer == 'hard') {
			array_push($arrFilterPropName, 'PROPERTY_ZHESTKOST');
			array_push($arrFilterPropValue, '108427');
		}
		if($ajaxData[$i]->answer == 'soft') {
			array_push($arrFilterPropName, 'PROPERTY_ZHESTKOST');
			array_push($arrFilterPropValue, '108428');
		}
	}
	if($ajaxData[$i]->question == 'segment') {
		if($ajaxData[$i]->answer == 'standart') {
			array_push($arrFilterPropName, 'PROPERTY_TSENOVOY_SEGMENT');
			array_push($arrFilterPropValue, '108429');
		}
		if($ajaxData[$i]->answer == 'business') {
			array_push($arrFilterPropName, 'PROPERTY_TSENOVOY_SEGMENT');
			array_push($arrFilterPropValue, '108430');
		}
		if($ajaxData[$i]->answer == 'premium') {
			array_push($arrFilterPropName, 'PROPERTY_TSENOVOY_SEGMENT');
			array_push($arrFilterPropValue, '108431');
		}
	}
}

$arrFilterProp = array_combine(array_values($arrFilterPropName), array_values($arrFilterPropValue));

$APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	"catalog_block_configurator", 
	array(
		"IBLOCK_TYPE" => "1c_catalog",
		"IBLOCK_ID" => "39",
		"SECTION_ID" => $forWho,
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "RAND",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "",
		"ELEMENT_SORT_ORDER2" => "",
		"FILTER_NAME" => "arrFilterProp",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "Y",
		"HIDE_NOT_AVAILABLE" => "Y",
		"PAGE_ELEMENT_COUNT" => "8",
		"LINE_ELEMENT_COUNT" => "4",
		"PROPERTY_CODE" => array(
			0 => "STARAYA_TSENA_",
			1 => "",
		),
		"OFFERS_LIMIT" => "",
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"BASKET_URL" => "/basket/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "N",
		"CACHE_FILTER" => "Y",
		"META_KEYWORDS" => "-",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"DISPLAY_COMPARE" => "Y",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "N",
		"PRICE_CODE" => array(
			0 => "Интернет-продажи(Трифонов)",
			1 => "Цены для сайта",
			2 => "BASE",
			3 => "Интернет-продажи (ИП Михайлов)",
		),
		"USE_PRICE_COUNT" => "Y",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_PROPERTIES" => array(
		),
		"USE_PRODUCT_QUANTITY" => "N",
		"CONVERT_CURRENCY" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"DISCOUNT_PRICE_CODE" => "",
		"AJAX_OPTION_ADDITIONAL" => "",
		"SHOW_ADD_FAVORITES" => "Y",
		"SECTION_NAME_FILTER" => "",
		"SECTION_SLIDER_FILTER" => "21",
		"COMPONENT_TEMPLATE" => "actions",
		"OFFERS_FIELD_CODE" => array(
			0 => "ID",
			1 => "",
		),
		"OFFERS_PROPERTY_CODE" => array(
			0 => "NAZNACHENIE_MATRASA",
			1 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER2" => "desc",
		"SHOW_MEASURE" => "Y",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"DISPLAY_WISH_BUTTONS" => "N",
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"SHOW_OLD_PRICE" => "Y",
		"SHOW_RATING" => "Y",
		"SALE_STIKER" => "SALE_TEXT",
		"SHOW_DISCOUNT_TIME" => "Y",
		"STORES" => array(
			0 => "43",
			1 => "44",
			2 => "45",
			3 => "46",
			4 => "47",
			5 => "48",
			6 => "49",
			7 => "50",
			8 => "51",
			9 => "52",
			10 => "53",
			11 => "54",
			12 => "55",
			13 => "56",
			14 => "57",
			15 => "58",
			16 => "59",
			17 => "60",
			18 => "61",
			19 => "62",
			20 => "63",
			21 => "64",
			22 => "65",
			23 => "66",
			24 => "67",
			25 => "68",
			26 => "69",
			27 => "70",
			28 => "71",
			29 => "72",
			30 => "73",
			31 => "74",
			32 => "75",
			33 => "76",
			34 => "77",
			35 => "78",
			36 => "79",
			37 => "80",
			38 => "81",
			39 => "82",
			40 => "83",
			41 => "84",
			42 => "2",
			43 => "",
		),
		"STIKERS_PROP" => "HIT",
		"SHOW_DISCOUNT_PERCENT_NUMBER" => "Y",
		"SHOW_MEASURE_WITH_RATIO" => "N",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"NAZNACHENIE_MATRASA" => $mattressAssignment,
	),
	false, array("HIDE_ICONS"=>"Y")
);
?>