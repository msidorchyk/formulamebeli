<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "В нашем интернет-магазине есть возможность подобрать матрас по индивидуальным характеристикам");
$APPLICATION->SetPageProperty("title", "Подобрать матрас по индивидуальным характеристикам в Формула мебели");
$APPLICATION->SetTitle("Подбор матраса");
$APPLICATION->AddHeadString('<link href="styles.css";  type="text/css" rel="stylesheet" />',true);
$APPLICATION->AddHeadString('<script src="script.js"></script>',true);
?>

<ul id="conf_menu" class="flex-row jc-c ai-c fxw-w"></ul>
<div id="conf_container" class="flex-column">
	<div id="for_who" class="flex-row jc-c ai-c fxw-w conf-row">
		<div class="header">Для кого матрас?</div>
		<div id="adult" class="item">
			<img src="icons/1-1.svg" />
			<p>Взрослый</p>
		</div>
		<div id="children" class="item">
			<img src="icons/1-2.svg" />
			<p>Ребенок</p>
		</div>
		<div class="skip-question">
			<p>Пропустить вопрос&nbsp;&gt;</p>
		</div>
	</div>
	<div id="reason" class="flex-row ai-c jc-c fxw-w conf-row">
		<div class="header">Причина покупки нового матраса?</div>
		<div id="new" class="item">
			<img src="icons/2-0.png" />
			<p>Первый матрас</p>
		</div>
		<div id="broken" class="item">
			<img src="icons/2-1.svg" />
			<p>Старый износился</p>
		</div>
		<div id="alergy" class="item">
			<img src="icons/2-2.svg" />
			<p>Аллергия</p>
		</div>
		<div id="pain" class="item">
			<img src="icons/2-3.svg" />
			<p>Болит спина после сна</p>
		</div>
		<div class="skip-question">
			<p>Пропустить вопрос&nbsp;&gt;</p>
		</div>
	</div>
	<div id="age" class="flex-row ai-c jc-c fxw-w conf-row">
		<div class="header">Укажите свой возраст</div>
		<div id="young" class="item">
			<img src="icons/3-1.svg" />
			<p>До 25 лет</p>
		</div>
		<div id="middle_grow" class="item">
			<img src="icons/3-2.svg" />
			<p>От 25 до 50 лет</p>
		</div>
		<div id="old" class="item">
			<img src="icons/3-3.svg" />
			<p>Больше 50 лет</p>
		</div>
		<div class="skip-question">
			<p>Пропустить вопрос&nbsp;&gt;</p>
		</div>
	</div>
	<div id="age_children" class="flex-row ai-c jc-c fxw-w conf-row">
		<div class="header">Укажите возраст ребенка</div>
		<div id="from_0_to_3" class="item">
			<img src="icons/12-1.svg" />
			<p>От 0 до 3 лет</p>
		</div>
		<div id="from_3_to_7" class="item">
			<img src="icons/12-2.svg" />
			<p>От 3 до 7 лет</p>
		</div>
		<div id="more_than_7" class="item">
			<img src="icons/12-3.svg" />
			<p>От 7 лет</p>
		</div>
	</div>
	<div id="count" class="flex-row jc-c ai-c fxw-w conf-row">
		<div class="header">Сколько человек будет спать на матрасе?</div>
		<div id="single" class="item">
			<img src="icons/4-1.svg" />
			<p>Один</p>
		</div>
		<div id="duo" class="item">
			<img src="icons/4-2.svg" />
			<p>Двое</p>
		</div>
		<div class="skip-question">
			<p>Пропустить вопрос&nbsp;&gt;</p>
		</div>
	</div>
	<div id="weight" class="flex-row ai-c jc-c fxw-w conf-row">
		<div class="header">Укажите вес первого партнера</div>
		<div id="less_than_70" data-id="70" class="item">
			<img src="icons/5-1.svg" />
			<p>До 70 кг</p>
		</div>
		<div id="from_70_to_100" data-id="100" class="item">
			<img src="icons/5-2.svg" />
			<p>70-100 кг</p>
		</div>
		<div id="from_100_to_140" data-id="140" class="item">
			<img src="icons/5-3.svg" />
			<p>100-140 кг</p>
		</div>
		<div id="more_than_140" data-id="200"class="item">
			<img src="icons/5-4.svg" />
			<p>От 140 кг</p>
		</div>
		<div class="skip-question">
			<p>Пропустить вопрос&nbsp;&gt;</p>
		</div>
	</div>
	<div id="weight_second" class="flex-row ai-c jc-c fxw-w conf-row">
		<div class="header">Укажите вес второго партнера</div>
		<div id="less_than_70" data-id="70" class="item">
			<img src="icons/5-1.svg" />
			<p>До 70 кг</p>
		</div>
		<div id="from_70_to_100" data-id="100" class="item">
			<img src="icons/5-2.svg" />
			<p>70-100 кг</p>
		</div>
		<div id="from_100_to_140" data-id="140" class="item">
			<img src="icons/5-3.svg" />
			<p>100-140 кг</p>
		</div>
		<div id="more_than_140" data-id="200" class="item">
			<img src="icons/5-4.svg" />
			<p>От 140 кг</p>
		</div>
		<div class="skip-question">
			<p>Пропустить вопрос&nbsp;&gt;</p>
		</div>
	</div>
	<div id="rigidity" class="flex-row ai-c jc-c fxw-w conf-row">
		<div class="header">Предпочтения по жесткости у первого партнера</div>
		<div id="middle" class="item">
			<img src="icons/6-1.svg" />
			<p>Средний</p>
		</div>
		<div id="hard" class="item">
			<img src="icons/6-2.svg" />
			<p>Жесткий</p>
		</div>
		<div id="soft" class="item">
			<img src="icons/6-3.svg" />
			<p>Мягкий</p>
		</div>
		<div class="skip-question">
			<p>Пропустить вопрос&nbsp;&gt;</p>
		</div>
	</div>
	<div id="rigidity_second" class="flex-row ai-c jc-c fxw-w conf-row">
		<div class="header">Предпочтения по жесткости у второго партнера</div>
		<div id="middle" class="item">
			<img src="icons/6-1.svg" />
			<p>Средний</p>
		</div>
		<div id="hard" class="item">
			<img src="icons/6-2.svg" />
			<p>Жесткий</p>
		</div>
		<div id="soft" class="item">
			<img src="icons/6-3.svg" />
			<p>Мягкий</p>
		</div>
		<div class="skip-question">
			<p>Пропустить вопрос&nbsp;&gt;</p>
		</div>
	</div>
	<!--div id="segment" class="flex-row ai-c jc-c fxw-w conf-row">
		<div class="header">Матрас какого сегмента вы хотите?</div>
		<div id="standart" class="item">
			<img src="icons/7-1.svg" />
			<p>Стандарт</p>
		</div>
		<div id="business" class="item">
			<img src="icons/7-2.svg" />
			<p>Бизнес</p>
		</div>
		<div id="premium" class="item">
			<img src="icons/7-3.svg" />
			<p>Премиум</p>
		</div>
		<div id="nevermind" class="item">
			<p style="line-height: 120px;">Не важно</p>
		</div>
	</div-->
	<div id="pain_area" class="flex-row ai-c jc-c fxw-w conf-row">
		<div class="header">Какая область спины болит?</div>
		<div id="scoliosis" class="item">
			<img src="icons/10-2.svg" />
			<p>Сколиоз (для подростков)</p>
		</div>
		<div id="wholeback" class="item">
			<img src="icons/10-3.svg" />
			<p>Вся спина</p>
		</div>
		<div id="topback" class="item">
			<img src="icons/10-4.svg" />
			<p>Верх спины</p>
		</div>
		<div id="herniaback" class="item">
			<img src="icons/10-5.svg" />
			<p>Есть грыжа спины</p>
		</div>
		<div id="bottomback" class="item">
			<img src="icons/10-6.svg" />
			<p>Низ спины</p>
		</div>
	</div>
	<div id="pain_area_second" class="flex-row ai-c jc-c fxw-w conf-row">
		<div class="header">Какая область спины болит у второго партнера?</div>
		<div id="nopain" class="item">
			<img src="icons/11-1.svg" />
			<p>Не болит</p>
		</div>
		<div id="scoliosis" class="item">
			<img src="icons/10-2.svg" />
			<p>Сколиоз (для подростков)</p>
		</div>
		<div id="wholeback" class="item">
			<img src="icons/10-3.svg" />
			<p>Вся спина</p>
		</div>
		<div id="topback" class="item">
			<img src="icons/10-4.svg" />
			<p>Верх спины</p>
		</div>
		<div id="herniaback" class="item">
			<img src="icons/10-5.svg" />
			<p>Есть грыжа спины</p>
		</div>
		<div id="bottomback" class="item">
			<img src="icons/10-6.svg" />
			<p>Низ спины</p>
		</div>
		<div class="skip-question">
			<p>Пропустить вопрос&nbsp;&gt;</p>
		</div>
	</div>
	<div id="finish" class="flex-row ai-c jc-c fxw-w conf-row">
		<div class="header">Поздравляем! <br>
Мы подобрали для вас идеальные варианты</div>
		<a class="again" href='#'>Подобрать снова</a>
	</div>
</div>
<div id="mattress_items" class="flex-column ai-c"></div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>