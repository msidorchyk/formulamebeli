$(document).ready(function() {
	var stacks = {
		for_who: {adult: 'reason', children: 'age_children', block_id: 'for_who', stage_name: 'Для кого'},
		age: {young: 'count', middle_grow: 'count', old: 'count', block_id: 'age', stage_name: 'Возраст'},
		age_children: {from_0_to_3: 'finish', from_3_to_7: 'finish', more_than_7: 'finish', block_id: 'age_children', stage_name: 'Возраст'},
		reason: {new: 'age', broken: 'age', alergy: 'finish', pain: 'count_pain', block_id: 'reason', stage_name: 'Причина'},
		count: {single: 'weight', duo: 'weight_duo', block_id: 'count', stage_name: 'Сколько человек'},
		count_pain: {single: 'pain_area', duo: 'pain_area_duo', block_id: 'count', stage_name: 'Сколько человек'},
		weight: {less_than_70: 'rigidity', from_70_to_100: 'rigidity', from_100_to_140: 'rigidity', more_than_140: 'rigidity', block_id: 'weight', stage_name: 'Вес'},
		weight_duo: {less_than_70: 'weight_second', from_70_to_100: 'weight_second', from_100_to_140: 'weight_second', more_than_140: 'weight_second', block_id: 'weight', stage_name: 'Вес'},
		weight_second: {less_than_70: 'rigidity_duo', from_70_to_100: 'rigidity_duo', from_100_to_140: 'rigidity_duo', more_than_140: 'rigidity_duo', block_id: 'weight_second', stage_name: 'Вес партнера'},
		weight_pain: {less_than_70: 'finish', from_70_to_100: 'finish', from_100_to_140: 'finish', more_than_140: 'finish', block_id: 'weight', stage_name: 'Вес'},
		weight_pain_duo: {less_than_70: 'weight_pain_second', from_70_to_100: 'weight_pain_second', from_100_to_140: 'weight_pain_second', more_than_140: 'weight_pain_second', block_id: 'weight', stage_name: 'Вес'},
		weight_pain_second: {less_than_70: 'finish', from_70_to_100: 'finish', from_100_to_140: 'finish', more_than_140: 'finish', block_id: 'weight_second', stage_name: 'Вес партнера'},
		rigidity: {middle: 'finish', hard: 'finish', soft: 'finish', block_id: 'rigidity', stage_name: 'Жесткость'},
		rigidity_duo: {middle: 'rigidity_second', hard: 'rigidity_second', soft: 'rigidity_second', block_id: 'rigidity', stage_name: 'Жесткость'},
		rigidity_second: {middle: 'finish', hard: 'finish', soft: 'finish', block_id: 'rigidity_second', stage_name: 'Жесткость для партнера'},
		//segment: {standart: 'finish', business: 'finish', premium: 'finish', nevermind: 'finish', block_id: 'segment', stage_name: 'Цена'},
		pain_area: {scoliosis: 'weight_pain', wholeback: 'weight_pain', topback: 'weight_pain', herniaback: 'weight_pain', bottomback: 'weight_pain', block_id: 'pain_area', stage_name: 'Область спины'},
		pain_area_duo: {scoliosis: 'pain_area_second', wholeback: 'pain_area_second', topback: 'pain_area_second', herniaback: 'pain_area_second', bottomback: 'pain_area_second', block_id: 'pain_area', stage_name: 'Область спины'},
		pain_area_second: {nopain: 'weight_pain_duo', scoliosis: 'weight_pain_duo', wholeback: 'weight_pain_duo', topback: 'weight_pain_duo', herniaback: 'weight_pain_duo', bottomback: 'weight_pain_duo', block_id: 'pain_area_second', stage_name: 'Область спины партнера'},
	}
	
	var current_stage = 'for_who';
	var current_stack = stacks[current_stage];
	var data = [];
	$('#'+current_stack.block_id).addClass('active');
	
	if (current_stage != 'finish'){
		$('#conf_menu').append('<li id="stage_menu_for_who" data-stage="for_who" class="active-stage">'+current_stack.stage_name+'</li>');
	}
	
	$('body').on('click', '.active .item', function(event) {
		var id = $(this).attr('id');
		var stage_name = $('.active').attr('id');
		
		StageChange(id);
		
		if(stage_name == 'weight' || stage_name == 'weight_second') {
			var weight_id = $(this).attr('data-id');
			data.push({question:stage_name, answer:weight_id});
		} else {
			data.push({question:stage_name, answer:id});
		}
		console.log(stacks)
	});

	$('body').on('click', '.again', function(event) {
		event.preventDefault();
		current_stage = 'for_who';
		current_stack = stacks[current_stage];
		data = [];
		$('#finish').removeClass('active');
		$('#'+current_stack.block_id).addClass('active');
		$('#conf_menu').html('<li id="stage_menu_for_who" data-stage="for_who" class="active-stage">'+current_stack.stage_name+'</li>').show();
		$('#mattress_items').html("");
	});
	
	$('body').on('click', '#conf_menu li', function(event) {
		var item_id = $(this).attr('data-stage');
		
		$('#stage_menu_'+current_stage).removeClass('active-stage');
		$(this).addClass('active-stage');
		
		while(!$('#conf_menu li:last-child').hasClass('active-stage')) {
			$('#conf_menu li:last-child').remove();
			data.pop();
		}
		
		$('#'+current_stack.block_id).removeClass('active');
		$('#'+item_id).addClass('active');
		
		current_stage = item_id;
		current_stack = stacks[current_stage];
	});
	
	$('body').on('click', '.active .skip-question', function(event) {
		var id = $('.active .item').attr('id');
		var stage_name = $('.active').attr('id');
		
		StageChange(id);
		data.push({question:stage_name, answer:'none'});
	});
	
	function StageChange(id){
		$('#'+current_stack.block_id).removeClass('active');			
		current_stage = current_stack[id];
		
		if (current_stage == 'finish') {
			data.push({question:current_stack.block_id, answer:id});
			$('#'+current_stage).addClass('active');
			$('#conf_menu').hide();
			
			console.log(data);
			
			$.ajax({
				url: 'ajax',
				type: 'GET',
				data: {'data' : JSON.stringify(data)},
				cache: false,
				beforeSend: function(){
					$('#finish').append('<img id="ajax_uploader" src="ajax/loader.gif">');
				},
				success: function(response){
					$('#ajax_uploader').remove();
					$('#mattress_items').html('').prepend(response);
					
				}
			});
		} else {
			current_stack = stacks[current_stage];
			$('#'+current_stack.block_id).addClass('active');
			
			$('#conf_menu li').removeClass('active-stage');
			$('#conf_menu').append('<li id="stage_menu_'+current_stage+'" data-stage="'+current_stage+'">'+current_stack.stage_name+'</li>');
			$('#stage_menu_'+current_stage).addClass('active-stage');
		}
	}
});