<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Купить мебель в рассрочку без банка от Формула мебели. Большой выбор корпусной и мягкой мебели.");
$APPLICATION->SetPageProperty("title", "Купить мебель в рассрочку на 6 месяцев в магазине формула мебели");
$APPLICATION->SetTitle("Рассрочка");
?><div class="container static-page">
	<p>

	</p>
	<p>
 <b>Вы можете оформить рассрочку если:</b>
	</p>
	<ul>
		<li>Вам от 18 лет</li>
		<li>Ваш стаж&nbsp;на последнем месте работы не менее 3-х месяцев</li>
		<li>Вы прописаны в Пермском крае</li>
	</ul>
	<p>
 <b>Для оформления вам нужно:</b>
	</p>
	<ul>
		<li>Паспорт</li>
		<li>Второй документ (водительское, СНИЛС, военный билет)</li>
	</ul>
	<p>
 <b>Условия оформления рассрочки:</b>
	</p>
	<ul>
		<li>от 5000 до 30000 рублей — первоначальный взнос 50%, остальная сумма делится на 3 месяца</li>
		<li>от 30000 рублей и больше — первоначальный взнос 50%, остальная сумма делится на 5 месяцев</li>
	</ul>
	<blockquote class="blockquote">
 <b>Например:</b> <br>
 <big><a href="/catalog/pryamye_divany/divan_pryamoy_feniks/"><i><u>Диван Феникс</u></i></a><i><u> за 21 990 рублей в рассрочку на 3 месяца</u></i></big><i><u> </u></i><br>
 <i>Первоначальный взнос 50% это </i><b><i>11 145</i></b><i> рублей от общей суммы 21990+300(стоимость доставки)=22 290 руб. </i><br>
 <i>Оставшуюся сумму 11 145 руб делим на три равные части и получаем <b>3 715</b> рублей ежемесячный платеж. </i><br>
 <i>Просто, удобно и без переплат.</i>
		<p>
		</p>
	</blockquote>
	<p>
 <span style="font-size: 14pt; color: #393185;"><b>При покупке в рассрочку доставка 300 рублей.</b></span>
	</p>
 <section id="rassrochka-form">
	<?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"inline",
	Array(
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_TIME" => "3600000",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "?send=ok",
		"USE_EXTENDED_ERRORS" => "N",
		"VARIABLE_ALIASES" => Array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID"),
		"WEB_FORM_ID" => "9"
	)
);?> </section>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>