<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/workflow/prolog.php");
use Bitrix\Main\Config\Option;

$APPLICATION->SetTitle("Установка свойств");
require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
$IBLOCK_ID = 39;
$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$IBLOCK_ID));
?>
<?
if(!empty($_POST['code'])){
    $propertyCodesSet = serialize($_POST['code']);
    Option::set("iblock", "visible_product_property", $propertyCodesSet);
}
?>
<?
$propertyCodesGet = unserialize(Option::get("iblock", "visible_product_property"));
if(empty($propertyCodesGet)){
    $propertyCodesGet = [];
}
?>
<form action="" method="POST">
<?
while ($prop_fields = $properties->Fetch())
{
    $checked = false;
    if(in_array($prop_fields['CODE'],$propertyCodesGet)){
        $checked = true;
    }
    ?><div><?=$prop_fields['ID']?> : <?=$prop_fields["NAME"]?> <input type="checkbox" name="code[]" value="<?=$prop_fields['CODE']?>" <?=$checked === true? "checked" : ""?>></div><br><?php
}
?>
<input type="submit" value="Сохранить">
</form>
<?
require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>
