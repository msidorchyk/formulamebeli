						<?CNext::checkRestartBuffer();?>
						<?IncludeTemplateLangFile(__FILE__);?>
							<?if(!$isIndex):?>
								<?if($isBlog):?>
									</div> <?// class=col-md-9 col-sm-9 col-xs-8 content-md?>
									<div class="col-md-3 col-sm-3 hidden-xs hidden-sm right-menu-md">
										<div class="sidearea">
				
											<?$APPLICATION->ShowViewContent('under_sidebar_content');?>
											<?CNext::get_banners_position('SIDE', 'Y');?>
											<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "sect", "AREA_FILE_SUFFIX" => "sidebar", "AREA_FILE_RECURSIVE" => "Y"), false);?>
										</div>
									</div>
								</div><?endif;?>
								<?if($isHideLeftBlock && !$isWidePage):?>
									</div> <?// .maxwidth-theme?>
								<?endif;?>
								</div> <?// .container?>
							<?else:?>
								<?CNext::ShowPageType('indexblocks');?>
							<?endif;?>
							<?CNext::get_banners_position('CONTENT_BOTTOM');?>
						</div> <?// .middle?>
					<?//if(!$isHideLeftBlock && !$isBlog):?>
					<?if(($isIndex && $isShowIndexLeftBlock) || (!$isIndex && !$isHideLeftBlock) && !$isBlog):?>
						</div> <?// .right_block?>				
						<?if($APPLICATION->GetProperty("HIDE_LEFT_BLOCK") != "Y" && !defined("ERROR_404")):?>
							<div class="left_block">
								<?CNext::ShowPageType('left_block');?>
							</div>
						<?endif;?>
					<?endif;?>
				<?if($isIndex):?>
					</div>
				<?elseif(!$isWidePage):?>
					</div> <?// .wrapper_inner?>				
				<?endif;?>
			</div> <?// #content?>
			<?CNext::get_banners_position('FOOTER');?>
		</div><?// .wrapper?>
		<footer id="footer">
			<?if($APPLICATION->GetProperty("viewed_show") == "Y" || $is404):?>
				<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	"basket", 
	array(
		"COMPONENT_TEMPLATE" => "basket",
		"PATH" => SITE_DIR."include/footer/comp_viewed.php",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "standard.php",
		"PRICE_CODE" => array(
			0 => "Цены для сайта",
		),
		"STORES" => array(
			0 => "44",
			1 => "45",
			2 => "46",
			3 => "47",
			4 => "48",
			5 => "49",
			6 => "50",
			7 => "51",
			8 => "52",
			9 => "53",
			10 => "54",
			11 => "55",
			12 => "56",
			13 => "57",
			14 => "59",
			15 => "60",
			16 => "61",
			17 => "62",
			18 => "63",
			19 => "65",
			20 => "66",
			21 => "67",
			22 => "68",
			23 => "69",
			24 => "70",
			25 => "71",
			26 => "72",
			27 => "73",
			28 => "74",
			29 => "75",
			30 => "76",
			31 => "77",
			32 => "78",
			33 => "79",
			34 => "81",
			35 => "82",
			36 => "83",
			37 => "84",
			38 => "85",
			39 => "86",
			40 => "87",
			41 => "88",
			42 => "89",
			43 => "90",
			44 => "91",
			45 => "92",
			46 => "96",
			47 => "97",
			48 => "43",
			49 => "58",
			50 => "64",
			51 => "80",
			52 => "",
		),
		"BIG_DATA_RCM_TYPE" => "bestsell",
		"STIKERS_PROP" => "HIT",
		"SALE_STIKER" => "SALE_TEXT",
		"SHOW_DISCOUNT_PERCENT_NUMBER" => "Y",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>					
			<?endif;?>
			<?CNext::ShowPageType('footer');?>
		</footer>
		<div class="bx_areas">
			<?CNext::ShowPageType('bottom_counter');?>
		</div>
		<?CNext::ShowPageType('search_title_component');?>
		<?CNext::setFooterTitle();
		CNext::showFooterBasket();?>

		<?require($_SERVER["DOCUMENT_ROOT"]."/include/components/popups/text-director.php");?>
		<?require($_SERVER["DOCUMENT_ROOT"]."/include/components/popups/form-rassrochka.php");?>
		<?require($_SERVER["DOCUMENT_ROOT"]."/include/components/popups/form-rassrochka-basket.php");?>
	</body>
</html>