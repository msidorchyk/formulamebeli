<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?use Bitrix\Currency\CurrencyTable;?>
<?
$arResult["ELEMENTS"] = array();
$arResult["SEARCH"] = array();
if (CModule::IncludeModule("catalog"))
{
    $itemsId = [];
    foreach($arResult['CATEGORIES'] as $key => &$category){
        if($key == 'all') continue;
        foreach($category['ITEMS'] as &$item){
            if(stripos($item['NAME'],'[') !== false){
                $item['NAME'] = substr($item['NAME'],0,stripos($item['NAME'],'['));
            }
            if(stripos($item['NAME'],'((') !== false){
                $item['NAME'] = substr($item['NAME'],0,stripos($item['NAME'],'(('));
            }
            if($item['PARAM2'] == 39){
                $itemsId[39][$key][] = $item['ITEM_ID'];
            }
        }

        $additionalProperties = [];

        $arSelect = Array("*","CATALOG_GROUP_4","CATALOG_GROUP_1");
        $arFilter = Array("IBLOCK_ID"=> 39, "ID" => $itemsId[39][$key]);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
        while($ob = $res->Fetch())
        {
            $picture = "";
            if ($ob["PREVIEW_PICTURE"] > 0)
                $picture = CFile::ResizeImageGet($ob["PREVIEW_PICTURE"], array("width"=>80, "height"=>80), BX_RESIZE_IMAGE_PROPORTIONAL, true);
            elseif ($ob["DETAIL_PICTURE"] > 0)
                $picture = CFile::ResizeImageGet($ob["DETAIL_PICTURE"], array("width"=>80, "height"=>80), BX_RESIZE_IMAGE_PROPORTIONAL, true);

            $additionalProperties[39][$ob['ID']] = [
              "CATALOG_PRICE_4" => $ob['CATALOG_PRICE_4'],
              "CATALOG_PRICE_1" => $ob['CATALOG_PRICE_1'],
              "PICTURE" => $picture,
            ];
        }
    }
}
$arResult['ADDITIONAL_PROPERTIES'] = $additionalProperties;

