<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?if (empty($arResult["CATEGORIES"])) return;?>
<div class="bx_searche scrollbar">
    <?foreach($arResult["CATEGORIES"] as $category_id => $arCategory):?>
        <?if($category_id == 'all') continue;?>
		<?foreach($arCategory["ITEMS"] as $i => $arItem):?>
            <?if(!($arResult['ADDITIONAL_PROPERTIES'][$arItem['PARAM2']][$arItem['ITEM_ID']]['CATALOG_PRICE_1'])
                && !($arResult['ADDITIONAL_PROPERTIES'][$arItem['PARAM2']][$arItem['ITEM_ID']]['CATALOG_PRICE_4'])) continue;?>
			<?//=$arCategory["TITLE"]?>
            <?if($arItem["URL"]){?>
				<a class="bx_item_block" href="<?=$arItem["URL"]?>">
					<div class="maxwidth-theme">
						<div class="bx_img_element">
							<?if($arResult['ADDITIONAL_PROPERTIES'][$arItem['PARAM2']][$arItem['ITEM_ID']]['PICTURE']['src']):?>
								<img src="<?=$arResult['ADDITIONAL_PROPERTIES'][$arItem['PARAM2']][$arItem['ITEM_ID']]['PICTURE']['src']?>">
							<?else:?>
								<img src="<?=SITE_TEMPLATE_PATH?>/images/no_photo_small.png" width="38" height="38">
							<?endif;?>
						</div>
						<div class="bx_item_element">
							<span><?=$arItem["NAME"]?></span>
							<div class="price cost prices">
								<div class="title-search-price">
                                    <?
                                    if($arResult['ADDITIONAL_PROPERTIES'][$arItem['PARAM2']][$arItem['ITEM_ID']]['CATALOG_PRICE_1']){
                                        ?><strike style="font-size:13px"><?=$arResult['ADDITIONAL_PROPERTIES'][$arItem['PARAM2']][$arItem['ITEM_ID']]['CATALOG_PRICE_1']?></strike><?
                                    }
                                    if($arResult['ADDITIONAL_PROPERTIES'][$arItem['PARAM2']][$arItem['ITEM_ID']]['CATALOG_PRICE_4']){
                                        ?><div class="price"><?=$arResult['ADDITIONAL_PROPERTIES'][$arItem['PARAM2']][$arItem['ITEM_ID']]['CATALOG_PRICE_4']?></div><?
                                    }
                                    ?>
								</div>
							</div>
						</div>
						<div style="clear:both;"></div>
					</div>
				</a>
            <?}?>
		<?endforeach;?>
	<?endforeach;?>
</div>