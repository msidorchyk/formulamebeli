<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true ); ?>
<?
	function cl_print_r ($var, $label = '') {
        $str = json_encode(print_r ($var, true));
        echo "<script>console.group('".$label."');console.log('".$str."');console.groupEnd();</script>";
    }
?>
<?if($arResult["ITEMS"]):?>
	<div class="blog_wrapper banners-small blog">
		<?if($arParams["TITLE_BLOCK"] || $arParams["TITLE_BLOCK_ALL"]):?>
			<div class="top_block">
				<h3 class="title_block"><?=$arParams["TITLE_BLOCK"];?></h3>
				<a href="<?=SITE_DIR.$arParams["ALL_URL"];?>"><?=$arParams["TITLE_BLOCK_ALL"] ;?></a>
			</div>
		<?endif;?>
		<div class="items">
			<div class="row">
				<?foreach($arResult["ITEMS"] as $key => $arItem)
				{
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));			
					?>
					<div class="col-m-<?=(!$key ? '40 first-item' : '20');?> col-md-4 col-sm-6 col-xs-6">
						<?
							
							if($arItem["PREVIEW_PICTURE"]):
								$firstPicture = CFile::ResizeImageGet(
									$arItem["PREVIEW_PICTURE"], 
									array(
										"width" => 500, 
										"height" => 350
									), 
									BX_RESIZE_IMAGE_PROPORTIONAL, 
									true
								);
								$picture = CFile::ResizeImageGet(
									$arItem["PREVIEW_PICTURE"], 
									array(
										"width" => 300, 
										"height" => 150
									), 
									BX_RESIZE_IMAGE_PROPORTIONAL, 
									true
								);
							else:
								$firstPicture = CFile::ResizeImageGet(
									$arItem["DETAIL_PICTURE"], 
									array(
										"width" => 500, 
										"height" => 350
									), 
									BX_RESIZE_IMAGE_PROPORTIONAL, 
									true
								);
								$picture = CFile::ResizeImageGet(
									$arItem["DETAIL_PICTURE"], 
									array(
										"width" => 200, 
										"height" => 120
									), 
									BX_RESIZE_IMAGE_PROPORTIONAL, 
									true
								);
							endif;
							
						?>
						<div class="item shadow animation-boxs <?=($picture && !$key ? 'shine' : '');?>" <?=($picture && !$key ? "style='background-image:url(".$firstPicture["src"].")'" : "");?> id="<?=$this->GetEditAreaId($arItem['ID']);?>">
							<div class="inner-item">
								<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="gradient_block"></a>
								<?if($picture):?>
									<div class="image shine">
										<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
											<img class="image__lazy-load" src="<?=$picture["src"]?>" alt="<?=($arItem["PREVIEW_PICTURE"]["ALT"]?$arItem["PREVIEW_PICTURE"]["ALT"]:$arItem["NAME"]);?>" title="<?=($arItem["PREVIEW_PICTURE"]["TITLE"]?$arItem["PREVIEW_PICTURE"]["TITLE"]:$arItem["NAME"]);?>" />
										</a>
									</div>
								<?endif;?>
								<div class="title">
									<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><span><?=$arItem["NAME"]?></span></a>
									<?if($arItem['DISPLAY_ACTIVE_FROM']):?>
										<div class="date-block"><?=$arItem['DISPLAY_ACTIVE_FROM'];?></div>
									<?endif;?>
								</div>
							</div>
						</div>
					</div>
				<?}?>
			</div>
		</div>
	</div>
<?endif;?>