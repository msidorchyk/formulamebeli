<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true ); ?>
<?if($arResult["ITEMS"]){?>
	<div class="news_blocks front news_blocks-head">
	<h3>Новости</h3>
    <?foreach($arResult["ITEMS"] as $arItem){
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
        <div class='top_block' id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class='top_block-wrapper'>
                <div class='top_block-img'>
                    <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="">
                </div>
                <?if(strlen($arItem['NAME']) < 30):?>
                    <h4><?=$arItem["NAME"]?></h4>
                <?else:?>
                    <h4><?=substr($arItem['NAME'],0,30)?>...</h4>
                <?endif;?>
            </div>
            <div class='top_block-content'>
                <?if(strlen($arItem['PREVIEW_TEXT']) < 150):?>
                    <p><?=$arItem['PREVIEW_TEXT']?></p>
                <?else:?>
                    <p><?=substr($arItem['PREVIEW_TEXT'],0,150)?>...</p>
                <?endif;?>
            </div>
            <div class='top_block-footer'>
                <p><?=substr($arItem["ACTIVE_FROM"],0,10)?></p>
                <p><a href="<?=$arItem["DETAIL_PAGE_URL"]?>">Подробнее >> </a></p>
            </div>
        </div>
    <?}?>
</div>
<?}?>