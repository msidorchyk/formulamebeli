<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<?$this->setFrameMode(true);?>
<link rel="stylesheet" type="text/css" href="/bitrix/templates/aspro_next/css/slick.css">
<link rel="stylesheet" type="text/css" href="/bitrix/templates/aspro_next/css/slick-theme.css">
<script type="text/javascript" src="/bitrix/templates/aspro_next/js/slick.min.js"></script>

<?if($arResult['ITEMS']):?>
	<script>
		$(document).ready(function() {
			$('.slider').slick({
				dots: true,
				autoplay: true,
				autoplaySpeed: 5500,
				lazyLoad: 'progressive',
			});
		});
	</script>
	<div class="maxwidth-theme">
		<div class="row">
			<div class="slider col-m-60 col-md-6 col-m-push-20" id="main-slider">
				<?foreach($arResult['ITEMS'] as $i => $arItem):?>
					<div class="slide">
						<?if($arItem["PROPERTIES"]["LINK"]["VALUE"]):?>
							<a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>">
						<?endif;?>
							<picture>
								<source srcset="<?=CFile::GetFileArray($arItem['PROPERTIES']['TABLET']['VALUE'])['SRC']?>" media="(min-width: 768px) and (max-width: 1023px)">
								<source srcset="<?=CFile::GetFileArray($arItem['PROPERTIES']['DESKTOP']['VALUE'])['SRC']?>" media="(min-width: 1024px)">
								<img src="<?=CFile::GetFileArray($arItem['PROPERTIES']['PHONE']['VALUE'])['SRC']?>" alt="MDN">
							</picture>
						<?if($arItem["PROPERTIES"]["LINK"]["VALUE"]):?>
							</a>
						<?endif;?>
					</div>
				<?endforeach;?>	
			</div>
			<?
				$APPLICATION->IncludeFile(
					"/include/mainpage/comp_pictures_slider.php",
					Array(),
					Array("MODE"=>"php")
				);
			?>
		</div>
	</div>
<?endif;?>