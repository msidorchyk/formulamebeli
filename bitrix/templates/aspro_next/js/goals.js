jQuery(document).ready(function($) {
    
    // Добавить в корзину (В разделе)
    $('.footer_button .to-cart').on('click', function() {
        dataLayer.push({
            event: 'TargetEvent',
            eventCategory: 'other',
            eventAction: 'add_basket'
        });
        ym(27398093, 'reachGoal', 'add_basket');
        console.log('Goal Reached!');
    });

    // Добавить в корзину (Карточка товара)
    $('.goal__add-ro-cart').on('click', function() {
        dataLayer.push({
            event: 'TargetEvent',
            eventCategory: 'other',
            eventAction: 'add_basket'
        });
        ym(27398093, 'reachGoal', 'add_basket');
        console.log('Goal Reached!');
    });

    // Добавить в корзину (Карточка товара)
    $('#fast_view_item .button_block span').on('click', function() {
        dataLayer.push({
            event: 'TargetEvent',
            eventCategory: 'other',
            eventAction: 'add_basket'
        });
        ym(27398093, 'reachGoal', 'add_basket');
        console.log('Goal Reached!');
    });

    // Купить в 1 клик (Карточка товара)
    $('#one_click_buy_form').on('submit', function() {
        dataLayer.push({
            event: 'TargetEvent',
            eventCategory: 'action',
            eventAction: '1click'
        });
        ym(27398093, 'reachGoal', '1click');
        ym(27398093, 'reachGoal', 'all_request');
        fbq('track', 'Lead');
        console.log('Goal Reached!');
    });

    // Купить в 1 клик (Быстрый просмотр)
    $('.fast_view_popup #one_click_buy_form').on('submit', function() {
        dataLayer.push({
            event: 'TargetEvent',
            eventCategory: 'action',
            eventAction: '1click'
        });
        ym(27398093, 'reachGoal', '1click');
        ym(27398093, 'reachGoal', 'all_request');
        fbq('track', 'Lead');
        console.log('Goal Reached!');
    });

    // Купить в расрочку без банка (Карточка товара)
    $('[name="SIMPLE_FORM_9"]').on('submit', function() {
        dataLayer.push({
            event: 'TargetEvent',
            eventCategory: 'action',
            eventAction: 'rassrochka'
        });
        ym(27398093, 'reachGoal', 'rassrochka');
        ym(27398093, 'reachGoal', 'all_request');
        fbq('track', 'Lead');
        console.log('Goal Reached!');
    });

    // Нашли дешевле (Карточка товара)
    $('[name="SIMPLE_FORM_14"]').on('submit', function() {
        dataLayer.push({
            event: 'TargetEvent',
            eventCategory: 'action',
            eventAction: 'cheap'
        });
        ym(27398093, 'reachGoal', 'cheap');
        ym(27398093, 'reachGoal', 'all_request');
        fbq('track', 'Lead');
        console.log('Goal Reached!');
    });

    // Нашли дешевле (Быстрый просмотр)
    $('.fast_view_popup #one_click_buy_form').on('submit', function() {
        dataLayer.push({
            event: 'TargetEvent',
            eventCategory: 'action',
            eventAction: 'cheap'
        });
        ym(27398093, 'reachGoal', 'cheap');
        ym(27398093, 'reachGoal', 'all_request');
        fbq('track', 'Lead');
        console.log('Goal Reached!');
    });

    // Нашли дешевле (Отдельная страница)
    $('form').on('submit', function() {
        if(document.location.href == 'https://formulamebeli.com/nashli-deshevle/') {
            dataLayer.push({
                event: 'TargetEvent',
                eventCategory: 'action',
                eventAction: 'cheap'
            });
            ym(27398093, 'reachGoal', 'cheap');
            ym(27398093, 'reachGoal', 'all_request');
            fbq('track', 'Lead');
            console.log('Goal Reached!');
        }
    });

    // Заказать звонок (Подвал, сквозная)
    $('[name="CALLBACK"]').on('submit', function() {
        dataLayer.push({
            event: 'TargetEvent',
            eventCategory: 'action',
            eventAction: 'callback'
        });
        ym(27398093, 'reachGoal', 'callback');
        ym(27398093, 'reachGoal', 'all_request');
        console.log('Goal Reached!');
    });

    // Заказать звонок (Нашли дешевле)
    $('#contactForm').on('submit', function() {
        dataLayer.push({
            event: 'TargetEvent',
            eventCategory: 'action',
            eventAction: 'cheap_call'
        });
        ym(27398093, 'reachGoal', 'cheap_call');
        ym(27398093, 'reachGoal', 'all_request');
        console.log('Goal Reached!');
    });

    // Купить в расрочку без банка (Страница рассрочки)
    $('#rassrochka-form form').on('submit', function() {
        dataLayer.push({
            event: 'TargetEvent',
            eventCategory: 'action',
            eventAction: 'rassrochka'
        });
        ym(27398093, 'reachGoal', 'rassrochka');
        console.log('Goal Reached!');
    });

    // Форма "Опт"
    $('form').on('submit', function() {
        if(document.location.href == 'https://formulamebeli.com/opt/') {
            dataLayer.push({
                event: 'TargetEvent',
                eventCategory: 'opt',
                eventAction: 'form'
            });
            ym(27398093, 'reachGoal', 'form');
            console.log('Goal Reached!');
        }
    });

    // Форма "Написать директору"
    $('[name="SIMPLE_FORM_12"]').on('submit', function() {
        dataLayer.push({
            event: 'TargetEvent',
            eventCategory: 'other',
            eventAction: 'dir'
        });
        ym(27398093, 'reachGoal', 'dir');
        console.log('Goal Reached!');
    });

    // Подписка на рассылку
    $('.subscribe-form').on('submit', function() {
        dataLayer.push({
            event: 'TargetEvent',
            eventCategory: 'other',
            eventAction: 'rassilka'
        });
        ym(27398093, 'reachGoal', 'rassilka');
        console.log('Goal Reached!');
    });

    // Оформить заказ в корзине
    $('[data-entity="basket-checkout-button"]').on('click', function() {
        dataLayer.push({
            event: 'TargetEvent',
            eventCategory: 'action',
            eventAction: 'buy'
        });
        ym(27398093, 'reachGoal', 'buy');
        ym(27398093, 'reachGoal', 'all_request');
        fbq('track', 'Lead');
        console.log('Goal Reached!');
    });

    // Купить в 1 клик в корзине
    $('.basket-checkout-section-inner .fastorder .oneclickbuy').on('click', function() {
        dataLayer.push({
            event: 'TargetEvent',
            eventCategory: 'action',
            eventAction: '1click'
        });
        ym(27398093, 'reachGoal', '1click');
        ym(27398093, 'reachGoal', 'all_request');
        fbq('track', 'Lead');
        console.log('Goal Reached!');
    });
});
