<?
$MESS["AUTH_PLEASE_AUTH"] = "Por favor autorice:";
$MESS["AUTH_LOGIN"] = "Login:";
$MESS["AUTH_PASSWORD"] = "Contraseña:";
$MESS["AUTH_REMEMBER_ME"] = "Recuerdame en esta computadora";
$MESS["AUTH_AUTHORIZE"] = "Autorizarse";
$MESS["AUTH_REGISTER"] = "Registrarse";
$MESS["AUTH_FIRST_ONE"] = "Si es la primera vez que visita el sitio, complete el formulario de registro.";
$MESS["AUTH_FORGOT_PASSWORD_2"] = "¿Olvidaste tu contraseña?";
$MESS["AUTH_CAPTCHA_PROMT"] = "Escribe texto de la imagen";
$MESS["AUTH_TITLE"] = "Iniciar sesión";
$MESS["AUTH_SECURE_NOTE"] = "La contraseña será encriptada antes de ser enviada. Esto evitará que la contraseña aparezca en forma abierta sobre los canales de transmisión de datos.";
$MESS["AUTH_NONSECURE_NOTE"] = "La contraseña será enviada en forma abierta. Habilite JavaScript en su navegador web para habilitar el cifrado de contraseña.";
?>