<?
// use Bitrix\Sale\Services\Base;
// use Bitrix\Sale\Internals\Entity;
// use Bitrix\Sale\Payment;

// class PayRestrictionByUserGroup extends Base\Restriction {
//     const ONLY_FOR_USER_GROUPS_STRING_IDENTIFIER = 'ONLY_FOR_USER_GROUPS';

//     public static $easeSort = 100;

//     /**
//      * Заголовок ограничения
//      * @return string
//      */
//     public static function getClassTitle() {
//         return 'promedia: По группе пользователя';
//     }

//     /**
//      * Описание ограничения
//      * @return string
//      */
//     public static function getClassDescription() {
//         return 'Для определённых групп пользователей.';
//     }

//     /**
//      * Параметры ограничения
//      * @param int $entityId
//      * @return array
//      */
//     public static function getParamsStructure($entityId = 0) {
//         $allSiteUserGroups = array();
//         $db = \CGroup::GetList($by = 'c_sort', $order = 'asc');
//         while ($userGroupData = $db->Fetch()):
//             $allSiteUserGroups[$userGroupData['ID']] = $userGroupData['REFERENCE'];
//         endwhile;

//         return array(
//             self::ONLY_FOR_USER_GROUPS_STRING_IDENTIFIER => array(
//                 'TYPE'     => 'ENUM',
//                 'MULTIPLE' => 'Y',
//                 'LABEL'    => 'Показывать только этим группам пользователей',
//                 'OPTIONS'  => $allSiteUserGroups,
//             ),
//         );
//     }

//     /**
//      * Получение дополнительных параметров для ограничения при его применении
//      * @param Entity $entity
//      * @return mixed
//      */
//      protected static function extractParams(Entity $entity) {
//         // $orderPrice = null;
//         // $paymentPrice = null;
//         $orderUserId = null; // ID пользователя из заказа.

//         if ($entity instanceof Payment) {
//             /** @var \Bitrix\Sale\PaymentCollection $collection */
//             $collection = $entity->getCollection();
//             /** @var \Bitrix\Sale\Order $order */
//             $order = $collection->getOrder();

//             //$orderPrice = $order->getPrice();
//             //$paymentPrice = $entity->getField('SUM');
//             $orderUserId = $order->getUserId();
//         }

//         return array(
//             // 'PRICE_PAYMENT' => $paymentPrice,
//             // 'PRICE_ORDER'   => $orderPrice,
//             'ORDER_USER_ID' => $orderUserId,
//         );
//     }

//     /**
//      * Проверка при применении ограничения
//      * @param array $params            Данные из self::extractParams()
//      * @param array $restrictionParams Данные из настроек ограничения
//      * @param int   $serviceId         ID платёжной системы
//      * @return bool true - показывать платежную систему, false - нет
//      */
//     public static function check($params, array $restrictionParams, $serviceId = 0) {
//         global $USER;

//         $userId = $params['ORDER_USER_ID'];
//         if ($userId):
//             if ((count(array_intersect(CUser::GetUserGroup($userId), $restrictionParams[self::ONLY_FOR_USER_GROUPS_STRING_IDENTIFIER])) > 0)):
//                 return true;
//             else:
//                 return false;
//             endif;
//         endif;
//         return false;
//     }
// }