<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader;

Bitrix\Main\Loader::registerAutoLoadClasses(null, array(
    '\Solar\Agent' => '/bitrix/classes/Agent.php',
));

define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log_user.txt");
AddEventHandler("main", "OnAfterUserAdd", Array("UserAddCoupon", "OnAfterUserAddHandler"));
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "AddElementOrSectionCode"); 
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "AddElementOrSectionCode");
AddEventHandler("iblock", "OnBeforeIBlockSectionAdd", "AddElementOrSectionCode"); 
AddEventHandler("iblock", "OnBeforeIBlockSectionUpdate", "AddElementOrSectionCode"); 

function AddElementOrSectionCode(&$arFields) { 
    $params = array(
        "max_len" => "100", 
        "change_case" => "L", 
        "replace_space" => "_", 
        "replace_space" => "_",
        "replace_other" => "_",
        "delete_repeat_replace" => "true", 
        "use_google" => "false", 
      );
                                             
    if (strlen($arFields["NAME"])>0 && strlen($arFields["CODE"])<=0 && ($arFields["IBLOCK_ID"] == 39 || $arFields["IBLOCK_ID"] == 40)) {
        $arFields['CODE'] = CUtil::translit($arFields["NAME"], "ru", $params);    
    }
}
AddEventHandler("main", "OnAfterUserRegister", Array("MyClass", "OnAfterUserRegisterHandler"));
AddEventHandler("main", "OnAfterUserLogin", Array("MyClass", "OnAfterUserLoginHandler"));
AddEventHandler("main", "OnAfterUserUpdate", Array("MyClass", "OnAfterUserUpdateHandler"));
AddEventHandler("sale", "OnBasketUpdate", Array("MyClass", "OnBasketAddHandler"));
AddEventHandler("sale", "OnBeforeBasketDelete", Array("MyClass", "OnBasketDeleteHandler"));
AddEventHandler("sale", "OnOrderAdd", Array("MyClass", "OnOrderAddHandler"));
class MyClass
{
   function OnAfterUserRegisterHandler(&$arFields)
    {
       try {
        	if($arFields['USER_ID'] > 0){
                // The data to send to the API
                $postData = array(
                    'customer' => array(
                        'ids' => array(
                            'clientWebsiteId' => $arFields['ID']
                        ),
                        'fullName' => $arFields['NAME'].' '.$arFields['SECOND_NAME'].' '.$arFields['LAST_NAME'],
                        'email' => $arFields['EMAIL'],
                        'mobilePhone' => $arFields['PERSONAL_PHONE'],
                        'subscriptions' => array(
                            array(
                                'brand'=> "formulamebeli",
                                'pointOfContact'=> "Email",
                                'isSubscribed'=> "true",
                                'valueByDefault' => true
                            )
                        )
                    )
                    
                );
    
                // Setup cURL
                $ch = curl_init("https://api.mindbox.ru/v3/operations/async?endpointId=online&operation=Registration&deviceUUID=$_COOKIE[mindboxDeviceUUID]");
                curl_setopt_array($ch, array(
                    CURLOPT_POST => TRUE,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HTTPHEADER => array(
                        'Authorization: Mindbox secretKey="1ol5EaXi84HQnUh1eLOh"',
                        'Accept: application/json',
                        'X-Customer-IP: '.$_SERVER['REMOTE_ADDR'],
                        'Content-Type: application/json'
                    ),
                    CURLOPT_POSTFIELDS => json_encode($postData)
                ));
    
                // Send the request
                $response = curl_exec($ch);
    
                $responseData = json_decode($response, TRUE);
                AddMessage2Log($responseData, "");
            }else{
                AddMessage2Log("Неудачная регистрация ".json_encode($arFields));
            }
        }catch (Exception $e){
        	var_dump($e->getMessage());
        }catch (Error $e) {
        	var_dump($e->getMessage());
        } 
    }
    function OnAfterUserLoginHandler(&$arFields)
    {
    	try {
	        if($arFields['USER_ID'] > 0){
	            // The data to send to the API
	            $postData = array(
	                'customer' => array(
	                    'ids' => array(
	                        'clientWebsiteId' => $arFields['USER_ID']
	                    )
	                )
	                
	            );

	            // Setup cURL
	            $ch = curl_init("https://api.mindbox.ru/v3/operations/async?endpointId=online&operation=Authorization&deviceUUID=$_COOKIE[mindboxDeviceUUID]");
	            curl_setopt_array($ch, array(
	                CURLOPT_POST => TRUE,
	                CURLOPT_RETURNTRANSFER => TRUE,
	                CURLOPT_HTTPHEADER => array(
	                    'Authorization: Mindbox secretKey="1ol5EaXi84HQnUh1eLOh"',
	                    'Accept: application/json',
	                    'X-Customer-IP: '.$_SERVER['REMOTE_ADDR'],
	                    'Content-Type: application/json'
	                ),
	                CURLOPT_POSTFIELDS => json_encode($postData)
	            ));

	            // Send the request
	            $response = curl_exec($ch);

	            $responseData = json_decode($response, TRUE);
	            AddMessage2Log($responseData, "");
	        }
	    }catch (Exception $e){
        	var_dump($e->getMessage());
        }catch (Error $e) {
        	var_dump($e->getMessage());
        } 
    }
    function OnAfterUserUpdateHandler(&$arFields)
    {
    	try{
	        if($arFields['RESULT'] == true){
	            // The data to send to the API
	            $secretKey = '1ol5EaXi84HQnUh1eLOh';

	            $myWebSiteId = $arFields['ID'];
	            $dateTimeString = gmdate('Y-m-d H:i:s');

	            $message = 'ExternalIdentityAuthentication|MyWebSite|'.$myWebSiteId.'|'.$dateTimeString;
	            $hash = hash_hmac('sha512', $message, $secretKey);

	            $authenticationTicket = bin2hex($message).'|'.$hash;
	            $postData = array(
	                'customer' => array(
	                    'ids' => array(
	                        'clientWebsiteId' => $arFields['ID']
	                    ),
	                    'authenticationTicket' => $authenticationTicket,
	                    'sex' => $arFields['PERSONAL_GENDER'],
	                    'birthDate' => $arFields['PERSONAL_BIRTHDAY'],
	                    'fullName' => $arFields['NAME']." ".$arFields['SECOND_NAME']." ".$arFields['LAST_NAME'],
	                    'email' => $arFields['EMAIL'],
	                    'mobilePhone' => $arFields['PERSONAL_PHONE'],
	                )
	                
	            );

	            // Setup cURL
	            $ch = curl_init("https://api.mindbox.ru/v3/operations/async?endpointId=online&operation=Data&deviceUUID=$_COOKIE[mindboxDeviceUUID]");
	            curl_setopt_array($ch, array(
	                CURLOPT_POST => TRUE,
	                CURLOPT_RETURNTRANSFER => TRUE,
	                CURLOPT_HTTPHEADER => array(
	                    'Authorization: Mindbox secretKey="1ol5EaXi84HQnUh1eLOh"',
	                    'Accept: application/json',
	                    'X-Customer-IP: '.$_SERVER['REMOTE_ADDR'],
	                    'Content-Type: application/json'
	                ),
	                CURLOPT_POSTFIELDS => json_encode($postData)
	            ));

	            // Send the request
	            $response = curl_exec($ch);

	            $responseData = json_decode($response, TRUE);
	            AddMessage2Log($responseData, "");
	        }
        }catch (Exception $e){
        	var_dump($e->getMessage());
        }catch (Error $e) {
        	var_dump($e->getMessage());
        } 
    }
    function OnBasketAddHandler($ID, $arFields){
        try{
	        $postData = array(
	            'productList' => array(

	            )
	        );
	        $arItems = CSaleBasket::GetByID($ID);

	        $delay = false;

	        if ($arItems['DELAY'] == "Y") {
	            $operation = "SetIzbrannoeItemList";
	        }else{
	            $operation = "SetKorzinaItemList";
	        }

	        $dbFindInBasket=CSaleBasket::GetList(array("NAME" => "ASC","ID" => "ASC"),
	            Array(
	                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
	                "DELAY" => $arItems['DELAY']
	            )
	        );
	        while ($arItem = $dbFindInBasket->Fetch())
	        {
	            // Получаем данные о торговом предложении, делаем explode(), чтобы разбить внешние коды товара и торгового предложения
	            $external = $arItem['PRODUCT_XML_ID'];	
	            if (strpos($external, '#')):
	                $external = explode('#', $external);
	                $sku_id = $external[1];
	            endif;
	            $sku_price = $arItem['BASE_PRICE'];
	            $count = $arItem['QUANTITY'];	

	            $id = $external[0];
	            $productInfo = CCatalogSku::GetProductInfo($arItem['PRODUCT_ID']);
	            if(is_array($productInfo)):
	                $productPrice = CPrice::GetBasePrice($productInfo['ID']);
	                $price = $productPrice['PRICE'];
	            else:
	                $id = $external[1];
	                $price = $sku_price;
	            endif;

	            $postData['productList'][] = array(
	                'product' => array(
	                    'sku' => array(
	                        'ids' => array(
	                            'website' => $sku_id
	                        ),
	                        'price' => $sku_price
	                    ),
	                    'ids' => array(
	                        'website' => $id
	                    ),
	                    'price' => $price
	                ),
	                'count' => $count
	            );
	        }
	         // Setup cURL
	        $ch = curl_init("https://api.mindbox.ru/v3/operations/async?endpointId=online&operation=$operation&deviceUUID=$_COOKIE[mindboxDeviceUUID]");
	        curl_setopt_array($ch, array(
	            CURLOPT_POST => TRUE,
	            CURLOPT_RETURNTRANSFER => TRUE,
	            CURLOPT_HTTPHEADER => array(
	                'Authorization: Mindbox secretKey="1ol5EaXi84HQnUh1eLOh"',
	                'Accept: application/json',
	                'X-Customer-IP: '.$_SERVER['REMOTE_ADDR'],
	                'Content-Type: application/json'
	            ),
	            CURLOPT_POSTFIELDS => json_encode($postData)
	        ));

	        // Send the request
	        $response = curl_exec($ch);

	        $responseData = json_decode($response, TRUE);
	        AddMessage2Log($responseData, "");
	    }catch (Exception $e){
        	var_dump($e->getMessage());
        }catch (Error $e) {
        	var_dump($e->getMessage());
        } 
    }
    function OnBasketDeleteHandler($ID){
        try{
	        $postData = array(
	            'productList' => array(

	            )
	        );
	        $arItems = CSaleBasket::GetByID($ID);

	        if (CModule::IncludeModule("catalog"))
	        {
	            $delay = false;

	            if ($arItems['DELAY'] == "Y") {
	                $operation = "SetIzbrannoeItemList";
	            }else{
	                $operation = "SetKorzinaItemList";
	            }

	            $dbFindInBasket=CSaleBasket::GetList(
	                Array("NAME" => "ASC","ID" => "ASC"),
	                Array(
	                    "FUSER_ID" => CSaleBasket::GetBasketUserID(),
	                    "DELAY" => $arItems['DELAY'],
	                    "!ID" => $ID
	                )
	            );
	            while ($arItem = $dbFindInBasket->Fetch())
	            {
	                // Получаем данные о торговом предложении, делаем explode(), чтобы разбить внешние коды товара и торгового предложения
	                $external = $arItem['PRODUCT_XML_ID'];	
	                if (strpos($external, '#')):
	                    $external = explode('#', $external);
	                    $sku_id = $external[1];
	                endif;
	                $sku_price = $arItem['BASE_PRICE'];
	                $count = $arItem['QUANTITY'];	
	                $id = $external[0];
	                $productInfo = CCatalogSku::GetProductInfo($arItem['PRODUCT_ID']);
	                if(is_array($productInfo)):
	                    $productPrice = CPrice::GetBasePrice($productInfo['ID']);
	                    $price = $productPrice['PRICE'];
	                else:
	                    $id = $external[1];
	                    $price = $sku_price;
	                endif;
	                $postData['productList'][] = array(
	                    'product' => array(
	                        'sku' => array(
	                            'ids' => array(
	                                'website' => $sku_id
	                            ),
	                            'price' => $sku_price
	                        ),
	                        'ids' => array(
	                            'website' => $id
	                        ),
	                        'price' => $price
	                    ),
	                    'count' => $count
	                );
	            }
	             // Setup cURL
	            $ch = curl_init("https://api.mindbox.ru/v3/operations/async?endpointId=online&operation=$operation&deviceUUID=$_COOKIE[mindboxDeviceUUID]");
	            curl_setopt_array($ch, array(
	                CURLOPT_POST => TRUE,
	                CURLOPT_RETURNTRANSFER => TRUE,
	                CURLOPT_HTTPHEADER => array(
	                    'Authorization: Mindbox secretKey="1ol5EaXi84HQnUh1eLOh"',
	                    'Accept: application/json',
	                    'X-Customer-IP: '.$_SERVER['REMOTE_ADDR'],
	                    'Content-Type: application/json'
	                ),
	                CURLOPT_POSTFIELDS => json_encode($postData)
	            ));

	            // Send the request
	            $response = curl_exec($ch);

	            $responseData = json_decode($response, TRUE);
	            AddMessage2Log(json_encode($_COOKIE), "");
	        }
	    }catch (Exception $e){
        	var_dump($e->getMessage());
        }catch (Error $e) {
        	var_dump($e->getMessage());
        } 
    }
    function OnOrderAddHandler($ID, $arFields){
    	try{
	        $postData = array(
	            "executionDateTimeUtc" => $arFields['DATE_STATUS'],
	            'customer' => array(
	                "ids" => array(
	                    "webSiteUserId" => $arFields['USER_ID']
	                ),
	                "email" => $arFields['ORDER_PROP'][2],
	                "mobilePhone" => $arFields['ORDER_PROP'][3]
	            ),
	            "order" => array(
	                "deliveryCost"=> $arFields['PRICE_DELIVERY'],
	                "lines" => array(
	                    
	                ),
	                "payments"=> array(
	                    array(
	                        "type"=> $arFields['PAY_SYSTEM_ID'],
	                        "amount"=> $arFields['PRICE']
	                    )
	                )
	            )
	        );
	        foreach ($arFields['BASKET_ITEMS'] as $key => $line) {
	            $postData["order"]['lines'][] = array(
	                "lineId" => $line["ID"],
	                "product"=> array(
	                    "ids"=> array(
	                        "websiteId"=> $line["PRODUCT_ID"]
	                    )
	                ),
	                "basePricePerItem"=> $line["BASE_PRICE"],
	                "quantity"=> $line["QUANTITY"],
	            );
	        }
	         // Setup cURL
	        $ch = curl_init("https://api.mindbox.ru/v3/operations/async?endpointId=online&operation=CreateOrder&deviceUUID=$_COOKIE[mindboxDeviceUUID]");
	        curl_setopt_array($ch, array(
	            CURLOPT_POST => TRUE,
	            CURLOPT_RETURNTRANSFER => TRUE,
	            CURLOPT_HTTPHEADER => array(
	                'Authorization: Mindbox secretKey="1ol5EaXi84HQnUh1eLOh"',
	                'Accept: application/json',
	                'X-Customer-IP: '.$_SERVER['REMOTE_ADDR'],
	                'Content-Type: application/json'
	            ),
	            CURLOPT_POSTFIELDS => json_encode($postData)
	        ));

	        // Send the request
	        $response = curl_exec($ch);

	        $responseData = json_decode($response, TRUE);
	        AddMessage2Log($responseData, "");
	    }catch (Exception $e){
        	var_dump($e->getMessage());
        }catch (Error $e) {
        	var_dump($e->getMessage());
        } 
	}
}

function _f($obj, $filename = null, $print_r = true)
{
    if (!$filename) {
        $filename = $_SERVER['DOCUMENT_ROOT']."/log.".time().".txt";
    }
    $fp = fopen($filename, "a+");
    if ($print_r) {
        fwrite($fp, print_r($obj, true));
    } else {
        ob_start();
        var_dump($obj);
        fwrite($fp, ob_get_clean());
    }

    fclose($fp);
}

function get_offer_min_price($item_id,$intmax = false){
    $ret = 0;

    CModule::IncludeModule("iblock");
    CModule::IncludeModule("catalog");
    // ID инфоблока товаров
    $IBLOCK_ID = 39;
    $ID = $item_id;
    $arInfo = CCatalogSKU::GetInfoByProductIBlock($IBLOCK_ID);

    if (is_array($arInfo)) {
        $offersExist = CCatalogSKU::getExistOffers([$item_id]);
        if(!empty($offersExist[$item_id])){
            $min = [];
            $res = CIBlockElement::GetList(Array("PRICE"=>"ASC"),
                array('IBLOCK_ID'=>$arInfo['IBLOCK_ID'], 'ACTIVE'=>'Y', 'PROPERTY_'.$arInfo['SKU_PROPERTY_ID'] => $ID),
                false,
                false,
                array('ID', 'NAME',"CATALOG_QUANTITY"));
            while($arRes = $res->fetch()){
                if(GetCatalogProductPrice($arRes["ID"], 4)['PRICE']){
                    $min[] = GetCatalogProductPrice($arRes["ID"], 4)['PRICE'];
                }
            }
            $result = min($min);
        }else{
            if(GetCatalogProductPrice($item_id, 4)['PRICE']) {
                $result = GetCatalogProductPrice($item_id, 4)['PRICE'];
            }
        }
    }

    if($intmax){
        if($result == 0){
            $result = PHP_INT_MAX;
        }
    }

    return $result;
}

AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("PropertySetter", "OnAfterIBlockElementUpdateHandler"));
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("PropertySetter", "OnAfterIBlockElementAddHandler"));
class PropertySetter
{
    function OnAfterIBlockElementUpdateHandler(&$arFields)
    {
        if($arFields['IBLOCK_ID'] == 39){
            CModule::IncludeModule("iblock");
            CModule::IncludeModule("catalog");
            $result = get_offer_min_price($arFields['ID']);
            \CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array("MINIMUM_PRICE" => $result));

            if($result == 0){
                $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=> 39, "CODE"=>"MINIMUM_PRICE_NULL"));
                while($enum_fields = $property_enums->GetNext())
                {
                    if($enum_fields['VALUE'] == "Да"){
                        $optionId = $enum_fields['ID'];
                    }
                }
                \CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array("MINIMUM_PRICE_NULL" => ["VALUE" => $optionId]));
            }

            $result = get_offer_min_price($arFields['ID'],true);
            \CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array("DEFAULT_SORT_PRICE" => $result));

            $arSelect = Array("*","CATALOG_QUANTITY");
            $arFilter = Array("IBLOCK_ID"=> 39, "ID" => $arFields['ID']);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
            while($ob = $res->Fetch())
            {
                $offersExist = CCatalogSKU::getExistOffers([$ob['ID']]);
                if(!empty($offersExist[$ob['ID']])){
                    $arInfo = CCatalogSKU::GetInfoByProductIBlock(39);
                    if (is_array($arInfo)) {
                        $rsOffers = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $arInfo['IBLOCK_ID'], 'PROPERTY_' . $arInfo['SKU_PROPERTY_ID'] => $ob['ID']), false,false, ['CATALOG_QUANTITY',"ID","NAME"]);
                        $offersAmount = 0;
                        while ($arOffer = $rsOffers->Fetch()) {
                            $offersAmount += $arOffer['CATALOG_QUANTITY'];
                        }
                        if($offersAmount > 0){
                            $status = 'В наличии на складе';
                        }else{
                            $status = 'Под заказ';
                        }
                        \CIBlockElement::SetPropertyValuesEx($ob['ID'], false, array("AVAILABLE_STATUS" => $status));
                    }
                }else{
                    if($ob['CATALOG_QUANTITY'] > 0){
                        $status = 'В наличии на складе';
                    }else{
                        $status = 'Под заказ';
                    }
                    \CIBlockElement::SetPropertyValuesEx($ob['ID'], false, array("AVAILABLE_STATUS" => $status));
                }
            }

            $filter1 = array('IBLOCK_ID' => 39);
            $arSelect1 = array('*','PROPERTY_*');
            $res1 = CIBlockElement::GetList(false, $filter1, false, false, $arSelect1);
            while ($db_res1 = $res1->Fetch()){
                $filter = array('IBLOCK_ID' => 39,"ID" => $db_res1['ID']);
                $arSelect = array('PROPERTY_HIT', 'ID', 'IBLOCK_ID');
                $arRes = [];
                $res = CIBlockElement::GetList(false, $filter, false, false, $arSelect);
                while ($db_res = $res->GetNextElement()):
                    $arRes[] = $db_res->GetProperties();
                endwhile;

                if(in_array("Хит",$arRes[0]['HIT']['VALUE'])){
                    \CIBlockElement::SetPropertyValuesEx($db_res1['ID'], false, array("OFFER" => 0));
                }
                if(in_array("Акция",$arRes[0]['HIT']['VALUE'])){
                    \CIBlockElement::SetPropertyValuesEx($db_res1['ID'], false, array("OFFER" => 1));
                }
            }
        }
    }

    function OnAfterIBlockElementAddHandler(&$arFields)
    {
        if($arFields['IBLOCK_ID'] == 39){
            CModule::IncludeModule("iblock");
            CModule::IncludeModule("catalog");
            $result = get_offer_min_price($arFields['ID']);
            \CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array("MINIMUM_PRICE" => $result));

            if($result == 0){
                $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=> 39, "CODE"=>"MINIMUM_PRICE_NULL"));
                while($enum_fields = $property_enums->GetNext())
                {
                    if($enum_fields['VALUE'] == "Да"){
                        $optionId = $enum_fields['ID'];
                    }
                }
                \CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array("MINIMUM_PRICE_NULL" => ["VALUE" => $optionId]));
            }

            $result = get_offer_min_price($arFields['ID'],true);
            \CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, array("DEFAULT_SORT_PRICE" => $result));
            
            $arSelect = Array("*","CATALOG_QUANTITY");
            $arFilter = Array("IBLOCK_ID"=> 39, "ID" => $arFields['ID']);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
            while($ob = $res->Fetch())
            {
                $offersExist = CCatalogSKU::getExistOffers([$ob['ID']]);
                if(!empty($offersExist[$ob['ID']])){
                    $arInfo = CCatalogSKU::GetInfoByProductIBlock(39);
                    if (is_array($arInfo)) {
                        $rsOffers = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $arInfo['IBLOCK_ID'], 'PROPERTY_' . $arInfo['SKU_PROPERTY_ID'] => $ob['ID']), false,false, ['CATALOG_QUANTITY',"ID","NAME"]);
                        $offersAmount = 0;
                        while ($arOffer = $rsOffers->Fetch()) {
                            $offersAmount += $arOffer['CATALOG_QUANTITY'];
                        }
                        if($offersAmount > 0){
                            $status = 'В наличии на складе';
                        }else{
                            $status = 'Под заказ';
                        }
                        \CIBlockElement::SetPropertyValuesEx($ob['ID'], false, array("AVAILABLE_STATUS" => $status));
                    }
                }else{
                    if($ob['CATALOG_QUANTITY'] > 0){
                        $status = 'В наличии на складе';
                    }else{
                        $status = 'Под заказ';
                    }
                    \CIBlockElement::SetPropertyValuesEx($ob['ID'], false, array("AVAILABLE_STATUS" => $status));
                }
            }

            $filter1 = array('IBLOCK_ID' => 39);
            $arSelect1 = array('*','PROPERTY_*');
            $res1 = CIBlockElement::GetList(false, $filter1, false, false, $arSelect1);
            while ($db_res1 = $res1->Fetch()){
                $filter = array('IBLOCK_ID' => 39,"ID" => $db_res1['ID']);
                $arSelect = array('PROPERTY_HIT', 'ID', 'IBLOCK_ID');
                $arRes = [];
                $res = CIBlockElement::GetList(false, $filter, false, false, $arSelect);
                while ($db_res = $res->GetNextElement()):
                    $arRes[] = $db_res->GetProperties();
                endwhile;

                if(in_array("Хит",$arRes[0]['HIT']['VALUE'])){
                    \CIBlockElement::SetPropertyValuesEx($db_res1['ID'], false, array("OFFER" => 0));
                }
                if(in_array("Акция",$arRes[0]['HIT']['VALUE'])){
                    \CIBlockElement::SetPropertyValuesEx($db_res1['ID'], false, array("OFFER" => 1));
                }
            }
        }
    }
}

AddEventHandler("search", "BeforeIndex", "BeforeIndexHandler");
function BeforeIndexHandler($arFields){
    if($arFields["PARAM1"] == "1c_catalog" && $arFields['PARAM2'] == 39){
        $arFields['TITLE'] .= "[".$arFields['TAGS']."]";
        return $arFields;
    }
    return $arFields;
}

AddEventHandler("main", "OnBuildGlobalMenu", "MyOnBuildGlobalMenu");
function MyOnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
{
    $aModuleMenu[] = [
      "parent_menu" => "global_menu_content",
      "section" => "product_property",
      "sort" => 5000,
      "text" => "Установка выводимых свойств товара",
      "title" => "Установка выводимых свойств товара",
      "icon" => "workflow_menu_icon",
      "page_icon" => "workflow_page_icon",
      "item_id" => "menu_product_property",
      "items" => [
         0 => [
           "text" => "Установка свойств",
           "title" => "Установка свойств",
           "url" => "product_property.php",
         ],
      ],
    ];
}

function offers_compare($a, $b)
{
    if(isset($a['PRICE']['VALUE'])){
        if(isset($b['PRICE']['VALUE'])){
            $test1 = (int) $a['PRICE']['VALUE'];
            $test2 = (int) $b['PRICE']['VALUE'];
            if ($test1 == $test2) {
                return 0;
            }
            return ($test1 < $test2) ? -1 : 1;
        }else{
            return -1;
        }
    }else{
        return 1;
    }

}

// Bitrix\Main\EventManager::getInstance()->addEventHandler(
//     'sale',
//     'onSalePaySystemRestrictionsClassNamesBuildList',
//     function() {
//         return new \Bitrix\Main\EventResult(
//             \Bitrix\Main\EventResult::SUCCESS,
//             array(
//                 '\PayRestrictionByUserGroup' => '/bitrix/php_interface/include/pay_restriction_by_user_group.php',
//             )
//         );
//     }
// );

/*class UserAddCoupon{
    function OnAfterUserAddHandler(&$arFields) {
        AddMessage2Log($arFields, "test");
        CModule::IncludeModule('sale');
        $coupon = \Bitrix\Sale\Internals\DiscountCouponTable::generateCoupon(true);
        try {
            $couponsResult = \Bitrix\Sale\Internals\DiscountCouponTable::add(
                array(
                    'COUPON' => $coupon,
                    'DISCOUNT_ID' => 2,
                    'USER_ID' => $arFields['ID'],
                    'ACTIVE_FROM' => null,
                    'ACTIVE_TO' => null,
                    'TYPE' => \Bitrix\Sale\Internals\DiscountCouponTable::TYPE_MULTI_ORDER
                )
            );
            if (!$couponsResult->isSuccess()){
                $errors = $couponsResult->getErrorMessages();
                AddMessage2Log($errors, "test");
            }else{
                AddMessage2Log($couponsResult, "test");
            }
        } catch (Exception $e) {
            AddMessage2Log($e, "error");
        }

    }
// }*/
// //пример использования события OnSaleOrderSaved

// use Bitrix\Main; 
// Main\EventManager::getInstance()->addEventHandler(
//     'sale',
//     'OnSaleBasketItemRefreshData',
//     'OnSaleBasketItemRefreshDataHandler'
// );


// function OnSaleBasketItemRefreshDataHandler(Main\Event $event)
// {
//     /** @var Order $order */
//     $order = $event->getParameter("ENTITY");
//     $values = $event->getParameter("VALUES");
//     AddMessage2Log(json_encode($values), "");
    
// }
// 