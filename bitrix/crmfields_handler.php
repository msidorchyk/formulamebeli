<?
/*
 * Prepare check
 */

use SProduction\CrmFields\ExtEventsQueue;

$log_label = 'lbl_' . str_replace('.', '', strval(microtime(true)));

include $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/sproduction.crmfields/lib/exteventsqueue.php';
include $_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/dbconn.php';

$deals_ids = [];

$queue = new ExtEventsQueue($DBHost, $DBName, $DBLogin, $DBPassword);
$deal_id = (int)$_REQUEST['data']['FIELDS']['ID'];
if ($deal_id) {
	$queue->add($deal_id);
	sleep(3);
	$deals_ids = $queue->getAndClear(50);
}

if (empty($deals_ids)) {
	return;
}


/*
 * Main part
 */

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$MODULE_ID = 'sproduction.crmfields';

$incl_res = CModule::IncludeModuleEx($MODULE_ID);
switch ($incl_res) {
    case MODULE_NOT_FOUND:
        echo 'Module '.$MODULE_ID.' not found.';
        die();
        break;
    case MODULE_DEMO_EXPIRED:
        echo 'Module '.$MODULE_ID.' demo expired.';
        die();
        break;
    default: // MODULE_INSTALLED
}

use Bitrix\Main\Config\Option,
    Bitrix\Sale,
    SProduction\CrmFields\CrmFields;

$obRest = SProdCRMFieldsGetRestObj();

$sync_active = Option::get($MODULE_ID, "active");
if ($sync_active != 'Y') {
	return;
}

if (!in_array($_REQUEST['event'], array('ONCRMDEALUPDATE', 'ONCRMDEALADD'))) {
    return;
}

SProdCRMFieldsLog('(crmfields_handler '.$log_label.') event: '.$_REQUEST['event']);

$arCred = $obRest->getCred();
if (!$arCred) {
    return;
}

// Check source of event
if ($_REQUEST['auth']['member_id'] != $arCred['member_id']) {
    return;
}

SProdCRMFieldsLog('deals list: ' . print_r($deals_ids, true));

// Deal data
$deals = [];
if (is_array($deals_ids) && !empty($deals_ids)) {
	$req_list = [];
	foreach ($deals_ids as $i => $deals_id) {
		$req_list[$i] = 'crm.deal.get' . '?' . http_build_query([
				'id' => $deals_id,
			]);
	}
	$resp = $obRest->restCommand('batch', [
		"halt"  => false,
		"cmd" => $req_list,
	], $arCred);
	if ($resp['result']['result']) {
		$deals = $resp['result']['result'];
	}
}

foreach ($deals as $arDeal) {
	$order_id = (int)$arDeal['ORIGIN_ID'];
	SProdCRMFieldsLog('(crmfields_handler ' . $log_label . ') deal:' . print_r($arDeal, true));

	if ( ! $order_id) {
		continue;
	}

	// If changing is blocked
	$ts = CrmFields::isOrderLocked($order_id);
	SProdCRMFieldsLog('(crmfields_handler ' . $log_label . ') isOrderLocked: ' . $ts);
	// Nothing to do
	if ($ts) {
		// Clear block
		CrmFields::delOrderLock($order_id);
	} else {
		// Order data
		if ($obOrder = Sale\Order::load($order_id)) {
			$arFieldsMap = CrmFields::getFieldsMap();
			// If this Deal is new
			if ($_REQUEST['event'] == 'ONCRMDEALADD') {
				// Change fields from CRM to Store
				$arOrder = CrmFields::getOrderArray($obOrder);
				CrmFields::syncOrderToDeal($arOrder, $arFieldsMap, true);
			} // If fields get from CRM
			else {
				// Change fields in Store
				$opt_direction = Option::get($MODULE_ID, "direction");
				if ( ! $opt_direction || $opt_direction == 'full' || $opt_direction == 'ctos') {
					// Additional check
					if ( ! isset($arFieldsMap['ORDER_ID']) || $arDeal[$arFieldsMap['ORDER_ID']] == $order_id) {
						CrmFields::syncDealToOrder($arDeal, $arFieldsMap, true);
					}
				}
			}
		}
	}
}