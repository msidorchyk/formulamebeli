<?
/*
 * Prepare check
 */

use SProduction\CrmStatus\ExtEventsQueue;

$log_label = 'lbl_' . str_replace('.', '', strval(microtime(true)));

include $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/sproduction.crmstatus/lib/exteventsqueue.php';
include $_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/dbconn.php';

$deals_ids = [];

$queue = new ExtEventsQueue($DBHost, $DBName, $DBLogin, $DBPassword);
$deal_id = (int)$_REQUEST['data']['FIELDS']['ID'];
if ($deal_id) {
	$queue->add($deal_id);
	sleep(3);
	$deals_ids = $queue->getAndClear(50);
}

if (empty($deals_ids)) {
	return;
}


/*
 * Main part
 */

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$incl_res = CModule::IncludeModuleEx("sproduction.crmstatus");
switch ($incl_res) {
    case MODULE_NOT_FOUND:
        echo 'Module sproduction.crmstatus not found.';
        die();
        break;
    case MODULE_DEMO_EXPIRED:
        echo 'Module sproduction.crmstatus demo expired.';
        die();
        break;
    default: // MODULE_INSTALLED
}

use Bitrix\Main\Config\Option;
use SProduction\CrmStatus\CrmStatus;

$obRest = getRestObj();

if (!in_array($_REQUEST['event'], array('ONCRMDEALUPDATE', 'ONCRMDEALADD'))) {
    return;
}

$arCred = $obRest->getFileCred();
if (!$arCred) {
    return;
}

// Check source of event
if ($_REQUEST['auth']['member_id'] != $arCred['member_id']) {
    return;
}

SProdCRMStatusLog('deals list: ' . print_r($deals_ids, true));

// Deal data
$deals = [];
if (is_array($deals_ids) && !empty($deals_ids)) {
	$req_list = [];
	foreach ($deals_ids as $i => $deals_id) {
		$req_list[$i] = 'crm.deal.get' . '?' . http_build_query([
			'id' => $deals_id,
		]);
	}
	$resp = $obRest->restCommand('batch', [
		"halt"  => false,
		"cmd" => $req_list,
	], $arCred);
	if ($resp['result']['result']) {
		$deals = $resp['result']['result'];
	}
}

foreach ($deals as $arDeal) {
	$order_id = (int)$arDeal['ORIGIN_ID'];
	SProdCRMStatusLog('(crmstatus_handler '.$log_label.') deal: '.print_r([
		'ID' => $arDeal['ID'],
		'TITLE' => $arDeal['TITLE'],
		'STAGE_ID' => $arDeal['STAGE_ID'],
		'ORIGIN_ID' => $arDeal['ORIGIN_ID'],
	], true));
	if ( ! $order_id) {
		continue;
	}

	// If status changing is blocked
	$ts = CrmStatus::isOrderBlocked($order_id);
	// Nothing to do
	if ($ts) {
		// Clear block
		CrmStatus::delOrderBlock($order_id);
	} else {
		// Order data
		$arFilter = array('ID' => $order_id);
		$arSelect = array('ID', 'ACCOUNT_NUMBER', 'STATUS_ID', 'DATE_INSERT', 'DATE_STATUS', 'PAYED');
		$db       = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter, false, false, $arSelect);
		if ($arOrder = $db->Fetch()) {
			SProdCRMStatusLog('(crmstatus_handler ' . $log_label . ') order: ' . print_r($arOrder, true));
			$arStatTbl         = CrmStatus::getStatusTable();
			$opt_payment_block = Option::get("sproduction.crmstatus", "payment_block");
			$opt_direction     = Option::get("sproduction.crmstatus", "direction");
			// If this Deal is new
			if ($_REQUEST['event'] == 'ONCRMDEALADD') {
				SProdCRMStatusLog('(crmstatus_handler ' . $log_label . ') New deal');
				// Change CRM status to the Store status
				CrmStatus::syncOrderToDeal($arOrder, $arStatTbl, true);
			} // If CRM status is changed after payment
			elseif ($arOrder['PAYED'] == 'Y' && $arDeal['STAGE_ID'] == 'WON'
			        && ($opt_payment_block == 'hard' || CrmStatus::isOrderPayed($order_id))) {
				SProdCRMStatusLog('(crmstatus_handler ' . $log_label . ') Status needs to be reset');
				CrmStatus::delOrderPayed($order_id);
				// Change CRM status back to the Store status
				CrmStatus::syncOrderToDeal($arOrder, $arStatTbl, true);
				// Save a record that the status has already been changed
				CrmStatus::addOrderBlock($order_id);
			} // If status get from CRM
			else {
				SProdCRMStatusLog('(crmstatus_handler ' . $log_label . ') Status doesn\'t needed to be reset');
				// Change status in Store
				if ( ! $opt_direction || $opt_direction == 'full' || $opt_direction == 'ctos') {
					CrmStatus::syncDealToOrder($arDeal, $arStatTbl, true);
				}
			}
		}
	}
}
