
/**
 *
 * MIXINS
 *
 */

var utilFuncs = {
    methods: {
        getReqPath: function (action) {
            return '/bitrix/admin/sprod_integr_ajax.php?action=' + action;
        },
    },
};

var mainFuncs = {
    data: function () {
        return {
            loader_counter: 0,
            errors: [],
            warnings: [],
        }
    },
    methods: {
        getReqPath: function (action) {
            return '/bitrix/admin/sprod_integr_ajax.php?action=' + action;
        },
        startLoadingInfo: function () {
            this.loader_counter++;
        },
        stopLoadingInfo: function () {
            this.loader_counter--;
            if (this.loader_counter < 0) {
                this.loader_counter = 0;
            }
        },
    },
    mounted() {
        // Check module state
        axios
            .get(this.getReqPath('main_check'))
            .then(response => {
                if (response.data.errors.length) {
                    this.errors = response.data.errors;
                    this.warnings = response.data.warnings;
                }
            })
            .catch(error => {
                console.log(error);
            });
    },
};


/**
 *
 * VUE COMPONENTS
 *
 */

// Error
Vue.component('main-errors', {
    props: ['errors', 'warnings'],
    template: `
    <div class="main-errors">
        <b-alert show variant="danger" v-for="text in errors" v-html="text"></b-alert>
        <b-alert show variant="warning" v-for="text in warnings" v-html="text"></b-alert>
    </div>
`,
});

// Loader
Vue.component('loader', {
    props: ['counter'],
    template: `
    <div class="loader float-right" v-if="counter">
        <div class="spinner-border text-info m-2" role="status"></div>
    </div>
`,
});
