
/**
 *
 * SETTINGS AND EXTERNAL VALUES
 *
 */

Vue.prototype.$profile_id = profile_id;


/**
 *
 * MIXINS
 *
 */

var componentsFuncs = {
    methods: {
        blockSaveData: function (code, callback) {
            this.$emit('load_start');
            axios
                .post(this.getReqPath('profile_save'), {
                    id: this.$profile_id,
                    block: code,
                    fields: this.fields,
                })
                .then((response) => {
                    if (response.data.status == 'ok') {
                        this.$emit('block_update', code);
                    }
                    // Callback success
                    if (typeof callback === 'function') {
                        callback(response);
                    }
                    this.$emit('load_stop');
                }, (error)  =>  {
                });
        },
        profileDelete: function () {
            if (confirm(this.$t("page.SP_CI_PROFILE_EDIT_DELETE_WARNING"))) {
                this.$emit('load_start');
                axios
                    .post(this.getReqPath('profile_del'), {
                        id: this.$profile_id,
                    })
                    .then((response) => {
                        if (response.data.status == 'ok') {
                            window.parent.location = '/bitrix/admin/sprod_integr_profiles.php?lang=ru';
                        }
                        this.$emit('load_stop');
                    }, (error)  =>  {
                    });
            }
        },
    },
    mounted() {
        // Blocks update (data received)
        this.$root.$on('blocks_update', (data, calling_block) => {
            for (item in this.fields) {
                if (data.blocks[this.code][item] !== undefined) {
                    this.fields[item] = data.blocks[this.code][item];
                }
            }
        });
    },
};


/**
 *
 * COMPONENTS
 *
 */

// Main settings
Vue.component('profile-main', {
    props: ['categ_list','users_list'],
    mixins: [utilFuncs, componentsFuncs],
    data: function () {
        return {
            code: 'main',
            state: {
                display: true,
                active: false,
            },
            fields: {
                active: '',
                name: '',
                prefix: '',
                deal_category: '',
                deal_respons_def: '',
            },
            profile_active: false,
        }
    },
    watch: {
        'users_list': function (new_val) {
            if (this.fields.deal_respons_def = '') {
                for (let id in this.users_list) {
                    this.fields.deal_respons_def = id;
                    break;
                }
            }
        },
        profile_active: function (new_value) {
            new_active_value = new_value ? 'Y' : 'N';
            if (this.fields.active != new_active_value) {
                this.fields.active = new_active_value;
            }
        },
        'fields.active': function (new_value) {
            new_profile_active_value = (this.fields.active == 'Y');
            if (this.profile_active != new_profile_active_value) {
                this.profile_active = new_profile_active_value;
            }
        },
    },
    template: `
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-body">

                <div class="row mb-2">
                    <div class="col-sm-6">
                        <div class="alert alert-info">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="main_active" v-model="profile_active" value="Y">
                                <label class="custom-control-label" for="main_active">{{ $t("page.SP_CI_PROFILE_EDIT_MAIN_PROFILE_ACTIVE") }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <button class="btn btn-sm btn-danger float-right" @click="profileDelete">{{ $t("page.SP_CI_PROFILE_EDIT_MAIN_PROFILE_DELETE") }}</button>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group mb-3">
                            <label for="main_name">{{ $t("page.SP_CI_PROFILE_EDIT_MAIN_NAME") }}</label>
                            <input type="text" id="main_name" class="form-control" v-model="fields.name">
                        </div>
                        <div class="form-group mb-3">
                            <label for="main_prefix">{{ $t("page.SP_CI_PROFILE_EDIT_MAIN_PREFIX") }}</label>
                            <input type="text" id="main_prefix" class="form-control" v-model="fields.prefix" v-b-tooltip.hover :title="$t('page.SP_CI_PROFILE_EDIT_MAIN_PREFIX_TOOLTIP')">
                        </div>
                        <div class="form-group mb-3">
                            <label for="main_deal_category">{{ $t("page.SP_CI_PROFILE_EDIT_MAIN_DEAL_CATEGORY") }}</label>
                            <b-form-select v-model="fields.deal_category" id="main_deal_category">
                                <option v-for="(c_name,c_id) in categ_list" :value="c_id">{{c_name}}</option>
                            </b-form-select>
                        </div>
                        <div class="form-group mb-3">
                            <label for="main_deal_respons_def">{{ $t("page.SP_CI_PROFILE_EDIT_MAIN_DEAL_RESPONS_DEF") }}</label>
                            <b-form-select v-model="fields.deal_respons_def" :options="users_list" id="main_deal_respons_def"></b-form-select>
                        </div>
                    </div>
                </div>

            </div> <!-- end card-body -->
            <div class="card-footer">
                <button class="btn btn-success" @click="blockSaveData(code)">{{ $t("page.SP_CI_PROFILE_EDIT_SAVE") }}</button>
                <!--<vue-ladda button-class="btn btn-success" data-style="expand-left" loading="true">Сохранить</vue-ladda>-->
            </div>
        </div> <!-- end card -->

    </div>
</div>
`,
});

// Filter
Vue.component('profile-filter', {
    props: ['condition_list'],
    mixins: [utilFuncs, componentsFuncs],
    data: function () {
        return {
            code: 'filter',
            state: {
                display: true,
                active: false,
            },
            fields: {
                filter: [],
            },
        }
    },
    methods: {
        addCondition: function () {
            this.fields.filter.push({
                condition: '',
                operation: '',
                value: [''],
            });
        },
        delCondition: function (index) {
            this.fields.filter.splice(index, 1)
        },
    },
    watch: {
        fields: {
            handler: function () {
                let item_i, item, last_value;
                for (item_i in this.fields.filter) {
                    item = this.fields.filter[item_i];
                    last_value = item.value.length - 1;
                    if (item.value[last_value] == '' && item.value[last_value - 1] == '') {
                        item.value.splice(last_value - 1, 1);
                    }
                    else if (item.value[last_value] != '') {
                        item.value.push('');
                    }
                }
            },
            deep: true
        },
    },
    template: `
<div class="card">
    <div class="card-body">
        <h4 class="header-title mb-2">{{ $t("page.SP_CI_PROFILE_EDIT_FILTER_TITLE") }}</h4>
        <p class="sub-header">{{ $t("page.SP_CI_PROFILE_EDIT_FILTER_SUBTITLE") }}</p>
        <div class="row">
            <div class="col-md-12">
                <div class="row mb-2" v-for="(item,index) in fields.filter">
                    <div class="col-4">
                        <b-form-select v-model="item.field">
                            <option v-for="(group,group_id) in condition_list" v-if="group.items.length == 0" :value="group_id">{{group.title}}</option>
                            <optgroup v-for="(group,group_id) in condition_list" v-if="group.items.length != 0" :label="group.title">
                                <option v-for="(field_name,field_id) in group.items" :value="group_id+'_'+field_id">{{field_name}} ({{field_id}})</option>
                            </optgroup>
                        </b-form-select>
                    </div>
                    <div class="col-2">
                        <b-form-select v-model="item.operation">
                            <option value="equal">{{ $t("page.SP_CI_PROFILE_EDIT_FILTER_OPERATION_EQUAL") }}</option>
                            <option value="not_equal">{{ $t("page.SP_CI_PROFILE_EDIT_FILTER_OPERATION_NOT_EQUAL") }}</option>
                            <option value="more">{{ $t("page.SP_CI_PROFILE_EDIT_FILTER_OPERATION_MORE") }}</option>
                            <option value="less">{{ $t("page.SP_CI_PROFILE_EDIT_FILTER_OPERATION_LESS") }}</option>
                        </b-form-select>
                    </div>
                    <div class="col-4">
                        <input type="text" v-for="(item_v,item_i) in item.value" v-model="fields.filter[index].value[item_i]" class="form-control mb-1" />
                    </div>
                    <div class="col-2">
                        <button class="btn btn-danger" @click="delCondition(index)">{{ $t("page.SP_CI_PROFILE_EDIT_FILTER_COND_DEL") }}</button>
                    </div>
                </div>
                <a href="#" @click="addCondition" class="btn btn-info waves-effect waves-light mt-2" data-animation="fadein" data-overlaycolor="#38414a"><i class="mdi mdi-plus-circle mr-1"></i> {{ $t("page.SP_CI_PROFILE_EDIT_FILTER_COND_ADD") }}</a>
            </div>
        </div>
    </div> <!-- end card-body -->
    <div class="card-footer">
        <button class="btn btn-success" @click="blockSaveData(code)">{{ $t("page.SP_CI_PROFILE_EDIT_SAVE") }}</button>
    </div>
</div> <!-- end card -->
`,
});

// Statuses and stages
Vue.component('profile-statuses', {
    props: ['stage_list','status_list'],
    mixins: [utilFuncs, componentsFuncs],
    data: function () {
        return {
            code: 'statuses',
            state: {
                display: true,
                active: false,
            },
            fields: {
                comp_table: {},
                cancel_stages: {},
            },
        }
    },
    watch: {
        status_list: function () {
            for (let status_id in this.status_list) {
                if (this.fields.comp_table[status_id] === undefined) {
                    this.fields.comp_table[status_id] = {
                        direction: 'all',
                        stages: [''],
                    };
                }
            }
        },
        // fields: {
        //     handler: function () {
        //         let item, last_value;
        //         for (let status_id in this.status_list) {
        //             item = this.fields.comp_table[status_id];
        //             console.log(item);
        //             // last_value = item.value.length - 1;
        //             // if (item.value[last_value] == '' && item.value[last_value - 1] == '') {
        //             //     item.value.splice(last_value - 1, 1);
        //             // }
        //             // else if (item.value[last_value] != '') {
        //             //     item.value.push('');
        //             // }
        //         }
        //     },
        //     deep: true
        // },
    },
    template: `
<div class="card">
    <div class="card-body">
        <h4 class="header-title mb-3">{{ $t("page.SP_CI_PROFILE_EDIT_STATUSES_TITLE") }}</h4>
        <div class="row">
            <div class="col-md-6">
                <label class="mt-3">{{ $t("page.SP_CI_PROFILE_EDIT_STATUSES_COMP_TABLE") }}</label>
                <table class="table mb-0 table-params table-params-status table-bordered">
                    <thead>
                    <tr>
                        <th class="param"><i class="icon icon-bitrix"></i> {{ $t("page.SP_CI_PROFILE_EDIT_STATUSES_COMP_TABLE_HEAD_ORDER") }}</th>
                        <th class="value"><i class="icon icon-bitrix24"></i> {{ $t("page.SP_CI_PROFILE_EDIT_STATUSES_COMP_TABLE_HEAD_DEAL") }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(status_name, status_id) in status_list">
                        <td>{{status_name}} ({{status_id}})</td>
                        <td>
                            <b-form-select v-for="(item_v,item_i) in fields.comp_table[status_id].stages" v-model="fields.comp_table[status_id].stages[item_i]">
                                <option value="">{{ $t("page.SP_CI_PROFILE_EDIT_STATUSES_COMP_TABLE_NOT_SYNC") }}</option>
                                <option v-for="(stage_name, stage_id) in stage_list" :value="stage_id">{{stage_name}} ({{stage_id}})</option>
                            </b-form-select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <label class="mt-3">{{ $t("page.SP_CI_PROFILE_EDIT_STATUSES_CANCEL_STAGES") }}</label>
                <div class="stages-cancel p-3 alert alert-light">
                    <div v-for="(name, id) in stage_list" class="checkbox checkbox-info mb-2">
                        <input type="checkbox" :id="'checkbox_'+id.replace(':','_')" v-model="fields.cancel_stages" :value="id">
                        <label :for="'checkbox_'+id.replace(':','_')">
                            {{name}} ({{id}})
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end card-body -->
    <div class="card-footer">
        <button class="btn btn-success" @click="blockSaveData(code)">{{ $t("page.SP_CI_PROFILE_EDIT_SAVE") }}</button>
    </div>
</div> <!-- end card -->
`,
});

// Order properties
Vue.component('profile-props', {
    props: ['person_type_list','prop_list','prop_other_list','field_list'],
    mixins: [utilFuncs, componentsFuncs],
    data: function () {
        return {
            code: 'props',
            state: {
                display: true,
                active: false,
            },
            fields: {
                comp_table: {},
            },
        }
    },
    watch: {
        'fields.comp_table': function(new_val, old_val) {
            let pt_i, pt, prop_i, prop;
            for (pt_i in this.person_type_list) {
                for (prop_i in this.prop_list[pt_i]) {
                    prop = this.prop_list[pt_i][prop_i];
                    if (new_val[prop.ID] === undefined) {
                        this.fields.comp_table[prop.ID] = {
                            direction: 'all',
                            value: '',
                        };
                    }
                }
            }
        },
        field_list: function(new_val, old_val) {
            let pt_i, pt, prop_i, prop;
            for (pt_i in this.person_type_list) {
                for (prop_i in this.prop_list[pt_i]) {
                    prop = this.prop_list[pt_i][prop_i];
                    if (this.fields.comp_table[prop.ID] === undefined) {
                        this.fields.comp_table[prop.ID] = {
                            direction: 'all',
                            value: '',
                        };
                    }
                }
            }
        },
    },
    template: `
<div class="card">
    <div class="card-body">
        <h4 class="header-title mb-2">{{ $t("page.SP_CI_PROFILE_EDIT_PROPS_TITLE") }}</h4>
        <p class="sub-header">{{ $t("page.SP_CI_PROFILE_EDIT_PROPS_SUBTITLE") }}</p>
        <div class="row">
            <div v-for="(pt_name, pt_id) in person_type_list" class="col-md-6">
                <label v-html="$t('page.SP_CI_PROFILE_EDIT_PROPS_COMP_TABLE_PT_LABEL', [pt_name, pt_id])"></label>
                <table class="table mb-2 table-params table-params-props table-bordered">
                    <thead>
                    <tr>
                        <th class="param"><i class="icon icon-bitrix"></i> {{ $t("page.SP_CI_PROFILE_EDIT_PROPS_COMP_TABLE_HEAD_ORDER") }}</th>
                        <!-- <th class="direct"></th> -->
                        <th class="value"><i class="icon icon-bitrix24"></i> {{ $t("page.SP_CI_PROFILE_EDIT_PROPS_COMP_TABLE_HEAD_DEAL") }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="prop in prop_list[pt_id]">
                        <td class="param">{{prop.NAME}} ({{prop.ID}})</td>
                        <!-- <td class="direct">
                            <a href="#" class="params-change-direct to-crm active"><i class="fa fa-arrow-alt-circle-right"></i></a>
                            <a href="#" class="params-change-direct to-order"><i class="fa fa-arrow-alt-circle-left"></i></a>
                        </td> -->
                        <td class="value">
                            <b-form-select v-model="fields.comp_table[prop.ID].value">
                                <option value="">{{ $t("page.SP_CI_PROFILE_EDIT_PROPS_COMP_TABLE_NOT_SYNC") }}</option>
                                <option v-for="(field_name, field_id) in field_list" :value="field_id">{{field_name}} ({{field_id}})</option>
                            </b-form-select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end card-body -->
    <div class="card-footer">
        <button class="btn btn-success" @click="blockSaveData(code)">{{ $t("page.SP_CI_PROFILE_EDIT_SAVE") }}</button>
    </div>
</div> <!-- end card -->
`,
});

// Contact info
Vue.component('profile-contact', {
    props: ['person_type_list','site_field_list','crm_field_list','ugroup_list'],
    mixins: [utilFuncs, componentsFuncs],
    data: function () {
        return {
            code: 'contact',
            state: {
                display: true,
                active: false,
            },
            fields: {
                sync_new_type: '',
                comp_table: {},
            },
        }
    },
    watch: {
        'fields.comp_table': function(new_val, old_val) {
            let crm_field_id;
            for (let pt_id in this.person_type_list) {
                if (this.fields.comp_table[pt_id] === undefined) {
                    this.fields.comp_table[pt_id] = {};
                }
                for (crm_field_id in this.crm_field_list) {
                    if (this.fields.comp_table[pt_id][crm_field_id] === undefined) {
                        this.fields.comp_table[pt_id][crm_field_id] = {
                            direction: 'all',
                            value: '',
                        };
                    }
                }
            }
        },
        'crm_field_list': function(new_val, old_val) {
            let crm_field_id;
            for (let pt_id in this.person_type_list) {
                if (this.fields.comp_table[pt_id] === undefined) {
                    this.fields.comp_table[pt_id] = {};
                }
                for (crm_field_id in this.crm_field_list) {
                    if (this.fields.comp_table[pt_id][crm_field_id] === undefined) {
                        this.fields.comp_table[pt_id][crm_field_id] = {
                            direction: 'all',
                            value: '',
                        };
                    }
                }
            }
        },
    },
    template: `
<div class="card">
    <div class="card-body">
        <h4 class="header-title mb-2">{{ $t("page.SP_CI_PROFILE_EDIT_CONTACT_TITLE") }}</h4>
        <p class="sub-header">{{ $t("page.SP_CI_PROFILE_EDIT_CONTACT_SUBTITLE") }}</p>
        <div class="row">
            <div class="col-md-6 mb-3">
                <label>{{ $t("page.SP_CI_PROFILE_EDIT_CONTACT_SYNC_NEW_TYPE_LABEL") }}</label>
                <b-form-select v-model="fields.sync_new_type">
                    <option value="">{{ $t("page.SP_CI_PROFILE_EDIT_CONTACT_SYNC_NEW_TYPE_0") }}</option>
                    <option value="1">{{ $t("page.SP_CI_PROFILE_EDIT_CONTACT_SYNC_NEW_TYPE_1") }}</option>
                    <!--<option value="2">{{ $t("page.SP_CI_PROFILE_EDIT_CONTACT_SYNC_NEW_TYPE_2") }}</option>-->
                </b-form-select>
            </div>
        </div>
        <div class="row">
            <div v-for="(pt_name, pt_id) in person_type_list" class="col-md-6">
                <label v-html="$t('page.SP_CI_PROFILE_EDIT_CONTACT_PT_LABEL', [pt_name, pt_id])"></label>
        
                <table class="table mb-3 table-params table-params-props table-bordered">
                    <thead>
                    <tr>
                        <th class="param"><i class="icon icon-bitrix24"></i> {{ $t("page.SP_CI_PROFILE_EDIT_CONTACT_COMP_TABLE_HEAD_B24") }}</th>
                        <!-- <th class="direct"></th> -->
                        <th class="value"><i class="icon icon-bitrix"></i> {{ $t("page.SP_CI_PROFILE_EDIT_CONTACT_COMP_TABLE_HEAD_STORE") }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(crm_field_name,crm_field_id) in crm_field_list">
                        <td class="param">{{crm_field_name}}</td>
                        <!-- <td class="direct">
                            <a href="#" class="params-change-direct to-crm"><i class="fa fa-arrow-alt-circle-right"></i></a>
                            <a href="#" class="params-change-direct to-order active"><i class="fa fa-arrow-alt-circle-left"></i></a>
                        </td> -->
                        <td class="value">
                            <b-form-select v-model="fields.comp_table[pt_id][crm_field_id].value">
                                <option value="">{{ $t('page.SP_CI_PROFILE_EDIT_CONTACT_COMP_TABLE_NOT_SYNC') }}</option>
                                <optgroup :label="$t('page.SP_CI_PROFILE_EDIT_CONTACT_COMP_TABLE_USER')">
                                    <option v-for="(field_name, field_id) in site_field_list.user.items" :value="field_id">{{field_name}} ({{field_id}})</option>
                                </optgroup>
                                <optgroup :label="$t('page.SP_CI_PROFILE_EDIT_CONTACT_COMP_TABLE_PROPS', [pt_id])">
                                    <option v-for="(field_name, field_id) in site_field_list.props.items[pt_id]" :value="field_id">{{field_name}} ({{field_id}})</option>
                                </optgroup>
                            </b-form-select>
                        </td>
                    </tr>
                    </tbody>
                </table>
        
            </div>
            <!--
            <div class="col-md-6">
                <label>Группы пользователей, которые не должны обновляться из CRM</label>
                <div class="stages-cancel pl-2">
                    <div class="checkbox checkbox-info mb-2">
                        <input id="checkbox0" type="checkbox" checked>
                        <label for="checkbox0">
                            Администраторы
                        </label>
                    </div>
                </div>
            </div>
            -->
        </div>
    </div> <!-- end card-body -->
    <div class="card-footer">
        <button class="btn btn-success" @click="blockSaveData(code)">{{ $t("page.SP_CI_PROFILE_EDIT_SAVE") }}</button>
    </div>
</div> <!-- end card -->
`,
});

// Other properties
Vue.component('profile-other', {
    props: ['prop_other_list','field_list'],
    mixins: [utilFuncs, componentsFuncs],
    data: function () {
        return {
            code: 'other',
            state: {
                display: true,
                active: false,
            },
            fields: {
                comp_table: {},
            },
        }
    },
    watch: {
        'fields.comp_table': function(new_val, old_val) {
            let prop_i, prop;
            for (prop_i in this.prop_other_list) {
                prop = this.prop_other_list[prop_i];
                if (new_val[prop.ID] === undefined) {
                    this.fields.comp_table[prop.ID] = {
                        direction: 'all',
                        value: '',
                    };
                }
            }
        },
        'field_list': function(new_val, old_val) {
            let prop_i, prop;
            for (prop_i in this.prop_other_list) {
                prop = this.prop_other_list[prop_i];
                if (this.fields.comp_table[prop.ID] === undefined) {
                    this.fields.comp_table[prop.ID] = {
                        direction: 'all',
                        value: '',
                    };
                }
            }
        },
    },
    template: `
<div class="card">
    <div class="card-body">
        <h4 class="header-title mb-2">{{ $t("page.SP_CI_PROFILE_EDIT_OTHER_TITLE") }}</h4>
        <p class="sub-header">{{ $t("page.SP_CI_PROFILE_EDIT_OTHER_SUBTITLE") }}</p>
        <div class="row">
            <div class="col-md-6">
                <label>{{ $t("page.SP_CI_PROFILE_EDIT_OTHER_LABEL") }}</label>
                <table class="table mb-2 table-params table-params-props table-bordered">
                    <thead>
                    <tr>
                        <th class="param"><i class="icon icon-bitrix"></i> {{ $t("page.SP_CI_PROFILE_EDIT_OTHER_HEAD_ORDER") }}</th>
                        <!-- <th class="direct"></th> -->
                        <th class="value"><i class="icon icon-bitrix24"></i> {{ $t("page.SP_CI_PROFILE_EDIT_OTHER_HEAD_DEAL") }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="p_other in prop_other_list">
                        <td class="param">{{p_other.NAME}}</td>
                        <!-- <td class="direct">
                            <a href="#" class="params-change-direct to-crm active"><i class="fa fa-arrow-alt-circle-right"></i></a>
                            <a href="#" class="params-change-direct to-order"><i class="fa fa-arrow-alt-circle-left"></i></a>
                        </td> -->
                        <td class="value">
                            <b-form-select v-model="fields.comp_table[p_other.ID].value">
                                <option value="">{{ $t("page.SP_CI_PROFILE_EDIT_OTHER_NOT_SYNC") }}</option>
                                <option v-for="(field_name, field_id) in field_list" :value="field_id">{{field_name}} ({{field_id}})</option>
                            </b-form-select>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end card-body -->
    <div class="card-footer">
        <button class="btn btn-success" @click="blockSaveData(code)">{{ $t("page.SP_CI_PROFILE_EDIT_SAVE") }}</button>
    </div>
</div> <!-- end card -->
`,
});

// Info
Vue.component('profile-info', {
    props: [],
    template: `
<b-alert show variant="warning" v-html="$t('page.SP_CI_PROFILE_EDIT_INFO_SAVE_CHANGES')"></b-alert>
`,
});


/**
 *
 * VUE APP
 *
 */

const i18n = new VueI18n({
    locale: 'ru',
    messages,
});

var app = new Vue({
    el: '#app',
    i18n,
    mixins: [utilFuncs, mainFuncs],
    data: {
        main_error: '',
        info: {
            crm: {
                users: [],
                directions: [],
                stages: [],
                fields: [],
            },
            site: {
                user_groups: [],
                statuses: [],
                person_types: [],
                props: [],
                other_props: [],
                contact_fields: [],
                conditions: [],
            },
        },
    },
    methods: {
        // Blocks update
        updateBlocks: function (calling_block) {
            this.startLoadingInfo();
            this.$emit('blocks_update_before', calling_block);
            axios
                .post(this.getReqPath('profile_info'), {
                    id: this.$profile_id,
                })
                .then(response => {
                    if (response.data.status == 'ok') {
                        this.info = response.data.info;
                        axios
                            .post(this.getReqPath('profile_get'), {
                                id: this.$profile_id,
                            })
                            .then(response => {
                                if (response.data.status == 'ok') {
                                    this.$emit('blocks_update', response.data, calling_block);
                                }
                                // Callback success
                                if (typeof callback === 'function') {
                                    callback(response);
                                }
                                this.stopLoadingInfo();
                            })
                            .catch(error => {
                                console.log(error);
                            });
                    }
                })
                .catch(error => {
                    console.log(error);
                });
        },
    },
    mounted() {
        this.updateBlocks();
    },
});
