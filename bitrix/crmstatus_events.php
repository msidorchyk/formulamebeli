<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main;
use Bitrix\Main\Type;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use Bitrix\Main\SiteTable;
use Bitrix\Sale;
use Bitrix\Main\Context;
use SProduction\CrmStatus\CrmStatus;

\Bitrix\Main\Loader::includeModule('sproduction.crmstatus');

SProdCRMStatusLog('(background) run');

$order_id = $_REQUEST['order_id'];
$arOrder = unserialize($_REQUEST['order']);
$arFieldsMap = unserialize($_REQUEST['status_table']);
//SProdCRMStatusLog('(crmstatus_events) $arOrder ' . print_r($arOrder,1));
if ($order_id && !empty($arOrder) && !empty($arFieldsMap)) {
	// Sync
	CrmStatus::syncOrderToDeal($arOrder, $arFieldsMap, true);
}
