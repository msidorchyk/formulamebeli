<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Документы при оформлении покупки в Формула мебели");
$APPLICATION->SetPageProperty("description", "Документы которые мы вручаем при заключении договора. Информация по телефону 88002226845");
$APPLICATION->SetTitle("Документы");
?><div class="container static-page">
	<h4>
	Вместе с каждым товаром мы вручаем покупателю: </h4>
	<ul>
		<li>Договор на мебель</li>
		<li>Договор на услуги</li>
		<li>Приходный кассовый ордер</li>
	</ul>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>