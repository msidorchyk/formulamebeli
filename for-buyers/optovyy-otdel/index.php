<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оптовый отдел");
?>
<div class="container static-page">
    <p>
        Компания "Формула Мебели" предлагает выгодное сотрудничество оптовым покупателям по корпусной и мягкой мебели.
    </p>
    <p>
        У нас низкие цены, так как мы являемся партнерами многих мебельных фабрик, а также действуют акции и спецпредложения, обращайтесь!
    </p>
    <p>
        Контакты сотрудников оптового отдела:
    </p>
    <b>По вопросам сотрудничества:</b><br>
    <p>
        Муштупенко Александр
    </p>
    <p>
        Телефон: +7 (912) 880-56-93
    </p>
    <p>
        Электронная почта: <a href="mailto:mushtupenko_as@formulamebeli.com">mushtupenko_as@formulamebeli.com</a>
    </p>
    <p>
        <br>
    </p>
    <strong>Менеджер&nbsp;отдела оптовых продаж:</strong>
    <p>
        Иочис Наталья Александровна
    </p>
    <p>
        тел.: 8-922-24-201-63&nbsp;
    </p>
    <p>
        почта: <a href="mailto:iochis.fm@yandex.ru">iochis.fm@yandex.ru</a>
    </p>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>