<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Сертификаты Формула мебели от наших поставщиков");
$APPLICATION->SetPageProperty("description", "Сертификаты качества от наших поставщиков");
$APPLICATION->SetTitle("Наши сертификаты");
CModule::IncludeModule("fileman");
CMedialib::Init();
$certificates_collection = CMedialibItem::GetList(array('arCollections' => array("0" => 2)));
?>
<div class="container static-page js-cert" style="opacity: 0;">
    <div class="certificates-slider" >
        <ul class="slides">
            <?foreach ($certificates_collection as $certificate):?>
                <li>
                    <img
                         data-image="<?=$certificate['PATH']?>"
                         src="<?=$certificate['PATH']?>"
                         alt="<?=$certificate['PATH']?>">
                </li>
            <?endforeach;?>
        </ul>
    </div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>