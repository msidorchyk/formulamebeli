'use strict';

import gulp from 'gulp';
import rename from 'gulp-rename';
import sass from 'gulp-sass';
import postcss from 'gulp-postcss';
import autoprefixer from 'autoprefixer';
import uglify from 'gulp-uglify';
import browserify from 'browserify';
import babelify from 'babelify';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import csso from 'gulp-csso';

var paths = {
    style: 'src/scss/main.scss',
    theme: 'src/scss/theme.scss',
    watchSCSS: 'src/scss/**/*',
    mainScripts: 'src/js/main.js',
    watchScripts: 'src/js/**/*',
    optimizedImagesDist: './src/dist/upload/',
    optimizedTemplateImagesDist: './src/dist/images/',
    images: 'upload/**/*',
    templateImages: 'bitrix/templates/sales-generator/images/**/*'
};


gulp.task('compress', () => {
    browserify(paths.mainScripts)
.transform('babelify', {
    global: true,
    only: /^(?:.*\/node_modules\/(?:a|b|c|d)\/|(?!.*\/node_modules\/)).*$/,
    presets: ["es2015"]
})
    .bundle()
    .pipe(source('custom.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest('./bitrix/templates/aspro_next/js/'));
});


gulp.task('sass', function() {
    var plugins = [
        autoprefixer({browsers: ['last 4 version']})
    ];
    return gulp.src(paths.style)
        .pipe(sass())
        .pipe(postcss(plugins))
        .pipe(csso({
            restructure: true,
            sourceMap: false,
            debug: true
        }))
        .pipe(rename('custom.css'))
        .pipe(gulp.dest('./bitrix/templates/aspro_next/css/'));
});

gulp.task('theme', function() {
  var plugins = [
    autoprefixer({browsers: ['last 4 version']})
  ];
  return gulp.src(paths.theme)
    .pipe(sass())
    .pipe(postcss(plugins))
    .pipe(csso({
      restructure: true,
      sourceMap: false,
      debug: true
    }))
    .pipe(rename('theme.min.css'))
    .pipe(gulp.dest('./bitrix/templates/aspro_next/themes/8/'));
});

gulp.task('watch', () => {
    gulp.watch(paths.watchSCSS, ['sass']);
});

gulp.task('watch-compress', () => {
    gulp.watch(paths.watchScripts, ['compress']);
});

gulp.task('script_compress', ['compress', 'watch-compress']);
gulp.task('sass_to_css', ['sass', 'watch']);
gulp.task('sass_theme', ['theme', 'watch']);

